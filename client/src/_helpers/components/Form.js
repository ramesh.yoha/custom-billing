import React from 'react'
import styled from 'styled-components'

const Form = styled.form`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const FormBody = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const FormActions = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  grid-gap: 20px;
  justify-content: center;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

export default ({ onSubmit, children, actions, style }) => (
  <Form onSubmit={onSubmit}>
    <FormBody style={{ ...style }}>
      {children}
    </FormBody>
    <FormActions>
      {actions}
    </FormActions>
  </Form>
)
