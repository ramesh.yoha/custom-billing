import React from 'react'
import styled from 'styled-components'
import {
  Paper,
  Typography
} from '@material-ui/core'
import { green } from '@material-ui/core/colors'

const Card = styled(Paper)`
  display: grid;
  margin: 20px 20px;
`

export default ({ title, children, color, style }) => (
  <Card style={{ margin: 0, ...style }}>
    <Typography
      style={{
        textAlign: 'left',
        padding: 10,
        fontSize: '18px',
        color: 'white',
        letterSpacing: 1,
        height: '70px',
        background: color || green[900]
      }}
      variant='overline'
    >
      {title}
    </Typography>
    <div style={{ padding: 20 }}>
      {children}
    </div>
  </Card>
)
