export const isValidPhone = phone => {
  return phone.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
}

export const isValidEmail = email => {
  return email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
}

export const isValidMAC = mac => {
  return mac.match(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/)
}

export const validateAddNewUserCredits = (credits, authCreditsAvailable, minimumTransferrableCredits) => {
  let isValid = true
  let errorMessage = ''
  if (credits < minimumTransferrableCredits) {
    isValid = false
    errorMessage = `Minimum Transferrable credits is ${minimumTransferrableCredits}`
  } else if (authCreditsAvailable < credits) {
    isValid = false
    errorMessage = "You don't have enough credits to transfer"
  }
  return { isValid, errorMessage }
}

export const validateUserCreditsTransfer = (credits, creditsAvailable, action = 'add', authCreditsAvailable, authUserType, minimumTransferrableCredits, nonRefundableCredits) => {
  let isValid = true
  let errorMessage = ''
  if (action === 'add') {
    if (credits < minimumTransferrableCredits) {
      isValid = false
      errorMessage = `Minimum Transferrable credits is ${minimumTransferrableCredits}`
    } else if (authCreditsAvailable < credits) {
      isValid = false
      errorMessage = 'You don\'t have enough credits to transfer'
    }
  } else { // action = 'recover'
    if (creditsAvailable - credits < 0) {
      isValid = false
      errorMessage = 'User does not have enough credits to recover'
    } else if (authUserType === 'superReseller' && (creditsAvailable - credits < nonRefundableCredits)) {
      isValid = false
      errorMessage = `Cannot recover past minimum non refundable credits ${nonRefundableCredits}`
    }
  }
  return { isValid, errorMessage }
}

export const validateAddNewClientCredits = (credits, authCreditsAvailable) => {
  let isValid = true
  let errorMessage = ''
  if (authCreditsAvailable < credits) {
    isValid = false
    errorMessage = "You don't have enough credits to transfer"
  } else if (credits > 12 || credits < 0) {
    isValid = false
    errorMessage = 'Credits can range from 0 to 12'
  }
  return { isValid, errorMessage }
}

export const validateClientCreditsTransfer = (credits, accountBalance, action = 'add', authCreditsAvailable) => {
  let isValid = true
  let errorMessage = ''
  if (credits < 1) {
    isValid = false
    errorMessage = 'Minimum credits must be 1'
  } else if (action === 'add') {
    if (authCreditsAvailable <= 0 && accountBalance === 0) {
      isValid = false
      errorMessage = 'You don\'t have enough credits to transfer'
    }
  } else { // action = 'recover'
    if (accountBalance - credits < 0) {
      isValid = false
      errorMessage = 'Client has not enough credits to recover'
    }
  }
  return { isValid, errorMessage }
}
