import { store } from 'config/store'

export const getChildUserType = (authUserType) => {
  switch (authUserType) {
    case 'superAdmin':
      return 'admin'
    case 'admin':
      return 'superReseller'
    case 'superReseller':
      return 'reseller'
    default:
      return null
  }
}

export const findInternalUserFromUsername = (username) => {
  const state = store.getState().users
  let user = state.admins.find(admin => admin.username === username)
  if (user) return user
  user = state.superResellers.find(superReseller => superReseller.username === username)
  if (user) return user
  user = state.resellers.find(reseller => reseller.username === username)
  if (user) return user
  user = state.staffs.find(staff => staff.username === username)
  if (user) return user
  user = state.specialDeleters.find(specialDeleter => specialDeleter.username === username)
  if (user) return user
  return state.clients.find(client => client.login === username)
}
