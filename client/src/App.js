import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Login, Wrapper, Logout } from 'containers'

export default () =>
  <Switch>
    <Route exact path='/login' component={Login} />
    <Route exact path='/logout' component={Logout} />
    <Route component={Wrapper} />
  </Switch>
