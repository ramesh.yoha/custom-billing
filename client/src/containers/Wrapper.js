import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  Notification,
  Loading,
  InactivityPopup,
  OfflinePopup,
  FourOhFour,
  Header
} from 'components'
import {
  Sidebar,
  Dashboard,
  UserList,
  UserEdit,
  UserAdd,
  StaffList,
  StaffEdit,
  StaffAdd,
  ClientList,
  ClientEdit,
  ClientAdd,
  SpecialDeleterList,
  SpecialDeleterAdd,
  SpecialDeleterEdit,
  Transaction,
  Events,
  SuperAdminConfig,
  SuperAdminLogs,
  Activities,
  Profile,
  SpecialDeleterHome
} from 'containers'
import socketIOClient from 'socket.io-client'
import { defaultColors } from '_helpers/colors'

import { refreshToken, logout } from 'actions/auth'
import { setMobileView, toggleMobileSideBar } from 'actions/general'
import { sync } from 'actions/sync'

const routes = [
  { path: '/', Component: Dashboard },
  { path: '/(admins|superResellers|resellers)', Component: UserList },
  { path: '/specialDeleters', Component: SpecialDeleterList },
  { path: '/staffs', Component: StaffList },
  { path: '/clients', Component: ClientList },
  { path: '/transactions', Component: Transaction },
  { path: '/events', Component: Events },
  { path: '/config', Component: SuperAdminConfig },
  { path: '/logs', Component: SuperAdminLogs },
  { path: '/activities', Component: Activities },
  { path: '/profile', Component: Profile },
  { path: '/users/new', Component: UserAdd },
  { path: '/users/:id', Component: UserEdit },
  { path: '/staffs/new', Component: StaffAdd },
  { path: '/staffs/:id', Component: StaffEdit },
  { path: '/clients/new', Component: ClientAdd },
  { path: '/clients/:id', Component: ClientEdit },
  { path: '/specialDeleters/new', Component: SpecialDeleterAdd },
  { path: '/specialDeleters/:id', Component: SpecialDeleterEdit }
]
class Wrapper extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      client: null,
      syncing: true
    }
  }

  componentWillMount = () => {
    if (!this.props.token) this.props.history.push('/login')
    else {
      this.socket = socketIOClient('/', { transports: ['websocket'] })
      this.updateDimensions()
    }
  }

  componentDidMount = () => {
    const { token } = this.props

    if (!token) this.props.history.push('/login')
    else {
      this.setState({ syncing: true }, () => {
        this.props.sync(true)
          .then(() => this.setState({ syncing: false }))
      })
      let mobileView = window.innerWidth <= 768
      if (this.props.mobileView !== mobileView) this.props.setMobileView(mobileView)
      window.addEventListener('resize', this.updateDimensions)
      this.socket.on('message', ({ authUsername, type }) => {
        this.props.sync(type === 'UPDATE_CONFIG_SUCCESS')
      })
    }
  }

  componentDidUpdate = () => {
    if (!this.props.token) this.props.history.push('/login')
  }

  componentWillUnmount = () => {
    this.socket && this.socket.disconnect()
    window.removeEventListener('resize', this.updateDimensions)
  }

  updateDimensions = () => {
    let mobileView = window.innerWidth <= 768
    if (this.props.mobileView !== mobileView) this.props.setMobileView(mobileView)
  }

  gotoLink = link => this.props.history.push(link)

  render () {
    const { syncing } = this.state
    const { token, username, userType, mobileView, location, colors = defaultColors } = this.props

    const RootDiv = styled.div`
      flex-grow: 1;
      z-index: 1;
      overflow: hidden;
      position: relative;
      display: flex;
    `

    const ContentDiv = styled.div`
      flex-grow: 1;
      min-width: 0px;
      padding: 16px;
      margin-left: ${mobileView ? 'inherit' : '250px'}
      padding-top: ${mobileView ? '70px' : '10px'}
    `

    if (syncing) return <Loading forced />
    if (!token) return <Loading forced />

    if (userType === 'specialDeleter') {
      return (
        <div>
          <SpecialDeleterHome />
          <Loading />
          <Notification />
          <OfflinePopup />
          <InactivityPopup logout={this.props.logout} refreshToken={this.props.refreshToken} />
        </div>
      )
    }

    return (
      <RootDiv>
        {mobileView &&
          <Header
            gotoLink={this.gotoLink}
            toggleMobileSideBar={this.props.toggleMobileSideBar}
            username={username}
            background={colors.sidebar_background}
          />
        }

        <Sidebar gotoLink={this.gotoLink} activePage={location.pathname} sync={this.props.sync} />

        <ContentDiv>
          <Switch>
            {routes.map(({ path, Component }) => <Route key={path} exact path={path} render={props => <Component sync={this.props.sync} {...props} />} />)}
            <Route component={FourOhFour} />
          </Switch>
        </ContentDiv>

        <Loading />
        <Notification />
        <OfflinePopup />
        <InactivityPopup logout={this.props.logout} refreshToken={this.props.refreshToken} />
      </RootDiv>
    )
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  username: state.auth.username,
  userType: state.auth.userType,
  storeAdmin: state.auth.storeAdmin,
  mobileView: state.general.mobileView,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  sync: configUpdated => dispatch(sync(configUpdated)),
  setMobileView: (mobileView) => dispatch(setMobileView(mobileView)),
  toggleMobileSideBar: (open) => dispatch(toggleMobileSideBar(open)),
  refreshToken: () => dispatch(refreshToken()),
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper)
