import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual } from 'lodash'
import { format } from 'date-fns'
import InputMask from 'react-input-mask'
import styled from 'styled-components'
import {
  Typography,
  TextField,
  Button
} from '@material-ui/core'
import {
  Save as SaveIcon
} from '@material-ui/icons'
import {
  Confirmation,
  Table
} from 'components'
import {
  isValidEmail,
  isValidPhone
} from '_helpers/validations'
import {
  Card,
  Form
} from '_helpers/components'
import { updateProfile } from 'actions/users'
import { defaultColors } from '_helpers/colors'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const TopWrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-gap: 20px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const ProfileDetails = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

const rows = [
  { field: 'loginDate', label: 'Login Date', type: 'date' },
  { field: 'loginIp', label: 'Login IP', type: 'string' },
  { field: 'loginUserAgent', label: 'User Agent', type: 'string' }
]

class Profile extends Component {
  constructor (props) {
    super(props)
    const { username, email, firstName, lastName, phoneNo } = props.auth
    this.state = {
      authUser: { username, email, firstName, lastName, phoneNo },
      newPassword: '',
      confirmation: false
    }
  }

  handleTextChange = (field, value) => {
    this.setState({ authUser: { ...this.state.authUser, [field]: value } })
  }

  updateProfile = (event) => {
    event.preventDefault()
    const { username, email, firstName, lastName, phoneNo } = this.state.authUser
    if (this.state.newPassword) {
      this.confirmationProceed = () => {
        this.props.updateProfile(username, { email, password: this.state.newPassword, firstName, lastName, phoneNo })
          .then(() => this.props.sync())
          .then(() => this.setState({ newPassword: '', confirmation: false }))
      }
      this.setState({ confirmation: true })
    } else {
      this.props.updateProfile(username, { email, firstName, lastName, phoneNo })
        .then(() => this.props.sync())
    }
  }

  confirmationProceed = () => this.setState({ confirmation: false })
  confirmationCancel = () => this.setState({ confirmation: false })

  getTableData = () => {
    let displayData = []
    for (let activity of this.props.auth.loginActivities) {
      let activityData = {}
      for (let row of rows) {
        activityData[row.field] = activity[row.field]
      }
      displayData.push({ ...activityData })
    }
    return displayData
  }

  render () {
    const { username, email, firstName, lastName, phoneNo } = this.props.auth
    const { userType, creditsAvailable, creditsOwed, createdAt, updatedAt, lastLogin } = this.props.auth
    const { mobileView, loading = true, colors = defaultColors } = this.props

    const isEmailEmpty = this.state.authUser.email === ''
    const isInvalidEmail = !isValidEmail(this.state.authUser.email)
    const firstNameEmpty = this.state.authUser.firstName === ''
    const lastNameEmpty = this.state.authUser.lastName === ''
    const phoneNoEmpty = this.state.authUser.phoneNo === '' || this.state.authUser.phoneNo === '___-___-____'
    const phoneNoInvalid = !isValidPhone(this.state.authUser.phoneNo)
    const noChanges = isEqual({ username, email, firstName, lastName, phoneNo }, this.state.authUser)

    const invalidForm = isEmailEmpty || isInvalidEmail || firstNameEmpty || lastNameEmpty || phoneNoEmpty || phoneNoInvalid || noChanges

    return (
      <Wrapper>
        <TopWrapper>
          <Card title='Edit Profile' color={colors.own_profileUpdate}>
            <Form
              onSubmit={this.updateProfile}
              style={{ gridTemplateColumns: '1fr 1fr' }}
              actions={
                [
                  <Button
                    key='update'
                    variant='contained'
                    type='submit'
                    color='primary'
                    disabled={(!this.state.newPassword) && (loading || invalidForm)}
                  >
                    Update
                    <SaveIcon style={{ marginLeft: 5 }} />
                  </Button>
                ]
              }
            >
              <TextField
                label='Email'
                type='email'
                value={this.state.authUser.email}
                onChange={(e) => this.handleTextChange('email', e.target.value)}
                fullWidth
                disabled={loading}
                error={isEmailEmpty || isInvalidEmail}
                helperText={isEmailEmpty ? 'Required' : isInvalidEmail ? 'Invalid email' : ''}
              />
              <InputMask mask='999-999-9999'
                value={this.state.authUser.phoneNo}
                onChange={(e) => this.handleTextChange('phoneNo', e.target.value)}
                disabled={loading}
              >
                {(inputProps) => (
                  <TextField
                    {...inputProps}
                    label='Phone'
                    fullWidth
                    error={phoneNoEmpty || phoneNoInvalid}
                    helperText={phoneNoEmpty ? 'Required' : phoneNoInvalid ? 'Invalid Phone' : null}
                  />
                )}
              </InputMask>
              <TextField
                label='First Name'
                value={this.state.authUser.firstName}
                onChange={(e) => this.handleTextChange('firstName', e.target.value)}
                fullWidth
                disabled={loading}
                error={firstNameEmpty}
                helperText={firstNameEmpty ? 'Required' : null}
              />
              <TextField
                label='Last Name'
                value={this.state.authUser.lastName}
                onChange={(e) => this.handleTextChange('lastName', e.target.value)}
                fullWidth
                disabled={loading}
                error={lastNameEmpty}
                helperText={lastNameEmpty ? 'Required' : null}
              />
              <TextField
                label='Change Password'
                type='password'
                placeholder='Leave empty to keep the current password unchanged'
                value={this.state.newPassword}
                onChange={(e) => this.setState({ newPassword: e.target.value })}
                fullWidth
                disabled={loading}
              />
            </Form>
          </Card>

          <Card title='Account Details' color={colors.own_accountDetails}>
            <ProfileDetails>
              <Typography variant='overline' style={{ fontSize: '12px' }}>Account Type</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{userType}</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>Credits Available</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{creditsAvailable}</Typography>
              {userType === 'reseller' &&
                <>
                  <Typography variant='overline' style={{ fontSize: '12px' }}>Credits on Hold</Typography>
                  <Typography variant='overline' style={{ fontSize: '12px' }}>{creditsOwed}</Typography>
                </>
              }
              <Typography variant='overline' style={{ fontSize: '12px' }}>Date Joined</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{format(Date.parse(createdAt), 'D MMM YYYY @ HH:mm:ss')}</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>Last Updated</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{format(Date.parse(updatedAt), 'D MMM YYYY @ HH:mm:ss')}</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>Last Login Date</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{format(Date.parse(lastLogin.loginDate), 'D MMM YYYY @ HH:mm:ss')}</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>Last Login IP</Typography>
              <Typography variant='overline' style={{ fontSize: '12px' }}>{lastLogin.loginIp}</Typography>
            </ProfileDetails>
          </Card>
        </TopWrapper>

        <Table
          title='Login Activities'
          rows={rows}
          data={this.getTableData()}
          orderBy='loginDate'
          orderByDirection='desc'
          mobileView={mobileView}
          viewOnly
          tableHeight={mobileView ? '75vh' : '85vh'}
          backgroundColor={colors.own_loginActivities}
          headingColor='white'
        />

        <Confirmation
          open={this.state.confirmation}
          message='You are changing your password. Are you sure you want to continue ?'
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
          disabled={!this.state.newPassword}
        />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  mobileView: state.general.mobileView,
  loading: state.general.loading,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateProfile: (username, user) => dispatch(updateProfile(username, user))
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
