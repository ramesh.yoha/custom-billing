import React from 'react'
import { connect } from 'react-redux'
import {
  ListItem,
  ListItemText,
  ListItemIcon,
  SwipeableDrawer,
  Typography,
  Icon
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { LogoImage } from 'assets'
import { defaultColors } from '_helpers/colors'

import { toggleMobileSideBar, clearNotification } from 'actions/general'
import { logout } from 'actions/auth'

const menus = [
  { label: 'Dashboard', link: '/', icon: 'dashboard' },
  { label: 'Admins', link: '/admins', icon: 'local_library' },
  { label: 'Super Resellers', link: '/superResellers', icon: 'group' },
  { label: 'Resellers', link: '/resellers', icon: 'person' },
  { label: 'Staffs', link: '/staffs', icon: 'airline_seat_recline_normal' },
  { label: 'Clients', link: '/clients', icon: 'live_tv' },
  { label: 'Special Deleters', link: '/specialDeleters', icon: 'voice_over_off' },
  { label: 'Transactions', link: '/transactions', icon: 'attach_money' },
  { label: 'Activities', link: '/activities', icon: 'reorder' },
  { label: 'Send Events', link: '/events', icon: 'near_me' },
  { label: 'Configuration', link: '/config', icon: 'info' },
  { label: 'My Account', link: '/profile', icon: 'settings' },
  { label: 'Cron Logs', link: '/logs', icon: 'timeline' }
]

const Sidebar = (props) => {
  const { mobileView, mobileMenu, activePage, enableSendEventsFor, gotoLink, clearNotification, logout, notificationShow, colors = defaultColors, sync } = props
  const { userType, username, creditsAvailable, creditsOwed, lastLogin, storeAdmin } = props.auth

  const onLinkClick = (link) => {
    if (mobileView) props.toggleMobileSideBar(false)
    if (activePage !== link) {
      gotoLink(link)
      if (notificationShow) clearNotification()
    }
  }

  const logo = (
    <div style={{ textAlign: 'center' }} onClick={() => sync()}>
      <img src={LogoImage} alt='Logo' style={{ maxWidth: 100, padding: '10px 10px' }} />
      <Typography variant='h5' color='inherit' noWrap style={{ color: 'white' }}>
        G1HD Billing
      </Typography>
      <br />
    </div>
  )

  const authInfo = (
    <div style={{ textAlign: 'center', marginBottom: 20 }}>
      <Typography variant='button' color='inherit' noWrap style={{ color: 'white' }}>
        Welcome {username} <br />
      </Typography>
      {lastLogin &&
      <Typography variant='button' color='inherit' noWrap style={{ color: 'white' }}>
        Last Login IP: {lastLogin.loginIp} <br />
      </Typography>
      }
      <Typography variant='subtitle2' color='inherit' noWrap style={{ color: 'white' }}>
        Credits Available <strong style={{ fontSize: 25 }}>{creditsAvailable}</strong>
      </Typography>
      {userType === 'reseller' &&
        <Typography variant='subtitle2' color='inherit' noWrap style={{ color: 'white' }}>
          Credits Owed <strong style={{ fontSize: 25 }}>{creditsOwed}</strong>
        </Typography>
      }
      {creditsOwed > creditsAvailable &&
        <Typography variant='subtitle2' color='secondary' style={{ color: 'yellow' }}>
          WARNING ! <br />You owe more credits than available
        </Typography>
      }
    </div>
  )

  let permissionsDenied = {
    'superAdmin': ['/staffs'],
    'admin': ['/admins', '/config', '/logs', '/staffs'],
    'superReseller': ['/admins', '/superResellers', '/config', '/logs', '/staffs', '/specialDeleters'],
    'reseller': ['/admins', '/superResellers', '/resellers', '/config', '/logs', '/specialDeleters']
  }
  if (userType === 'reseller' && storeAdmin !== 'me') permissionsDenied.reseller.push('/staffs')

  if (enableSendEventsFor) {
    if (!enableSendEventsFor['admin']) permissionsDenied['admin'].push('/events')
    if (!enableSendEventsFor['superReseller']) permissionsDenied['superReseller'].push('/events')
    if (!enableSendEventsFor['reseller']) permissionsDenied['reseller'].push('/events')
  }

  const StyledListItem = withStyles({
    root: {
      minHeight: 60,
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: colors.sidebar_itemHover
      }
    }
  })(ListItem)

  const renderLinks = menus.map(({ label, link, icon }, idx) =>
    !permissionsDenied[userType].includes(link) &&
    <StyledListItem
      key={idx}
      button
      style={{ background: activePage === link ? colors.sidebar_itemActive : '' }}
      onClick={() => onLinkClick(link)}
      disabled={props.loading}
    >
      <ListItemIcon style={{ color: activePage === link ? 'white' : colors.sidebar_itemInactive }}>
        <Icon>{icon}</Icon>
      </ListItemIcon>
      <ListItemText disableTypography primary={<Typography type='subheading' style={{ color: activePage === link ? 'white' : colors.sidebar_itemInactive }}>{label}</Typography>} />
    </StyledListItem>
  )

  const footer = (
    <StyledListItem
      onClick={logout}
      button
      style={{ marginTop: 5 }}
    >
      <ListItemIcon style={{ color: colors.sidebar_itemInactive }}><Icon>logout</Icon></ListItemIcon>
      <ListItemText disableTypography primary={<Typography type='subheading' style={{ color: colors.sidebar_itemInactive }}>Logout</Typography>} />
    </StyledListItem>
  )

  const StyledSwipeableDrawer = withStyles({
    paper: {
      width: 250,
      border: 'none',
      background: colors.sidebar_background
    },
    root: {
      minHeight: 60,
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: colors.sidebar_itemHover
      }
    }
  })(SwipeableDrawer)

  return (
    <StyledSwipeableDrawer
      open={!mobileView || mobileMenu}
      variant={!mobileView ? 'permanent' : 'temporary'}
      onClose={() => props.toggleMobileSideBar(false)}
      onOpen={() => props.toggleMobileSideBar(true)}
    >
      {logo}
      {authInfo}
      {renderLinks}
      {footer}
    </StyledSwipeableDrawer>
  )
}

const mapStateToProps = state => ({
  auth: state.auth,
  mobileMenu: state.general.mobileMenu,
  mobileView: state.general.mobileView,
  notificationShow: state.general.notificationShow,
  enableSendEventsFor: state.config.enableSendEventsFor,
  loading: state.general.loading,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  toggleMobileSideBar: (open) => dispatch(toggleMobileSideBar(open)),
  clearNotification: () => dispatch(clearNotification()),
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
