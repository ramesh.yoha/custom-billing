import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subDays } from 'date-fns'
import { Table } from 'components'

import { getTransactions } from 'actions/transactions'

const rows = [
  { field: 'createdAt', label: 'Date', type: 'date' },
  { field: 'transactionFrom', label: 'From', type: 'string' },
  { field: 'transactionTo', label: 'To', type: 'string' },
  { field: 'credits', label: 'Credits', type: 'integer' },
  { field: 'description', label: 'Description', type: 'string' }
]

const getTableData = (transactions, username) => {
  let displayData = []
  for (let transaction of transactions) {
    if (transaction.transactionFrom === username || transaction.transactionTo === username) {
      let transactionData = {}
      for (const row of rows) {
        transactionData[row.field] = transaction[row.field]
      }
      displayData.push({ ...transactionData })
    }
  }
  return displayData
}

class Transaction extends Component {
  constructor (props) {
    super(props)
    this.state = {
      daysBehind: 30
    }
  }

  handleFilter = (event, daysBehind) => {
    event.preventDefault()
    this.setState({ daysBehind }, () => {
      const from = subDays(new Date(), this.state.daysBehind)
      const to = new Date()
      this.props.getTransactions({ from, to })
    })
  }

  render () {
    const { daysBehind } = this.state
    const {
      mobileView,
      title,
      transactions,
      authUserType,
      embedded,
      color,
      limit,
      noPagination = false
    } = this.props

    const username = this.props.username || this.props.authUsername

    return (
      <Table
        title={title || 'Transactions'}
        rows={rows}
        data={getTableData(transactions, username)}
        orderBy='createdAt'
        orderByDirection='asc'
        mobileView={mobileView}
        viewOnly
        canDownload={authUserType === 'superAdmin'}
        tableHeight={embedded ? '300px' : mobileView ? '75vh' : '85vh'}
        limit={embedded ? limit || 5 : 99999}
        noPagination={noPagination}
        backgroundColor={embedded ? color : ''}
        headingColor={embedded ? 'white' : ''}
        handleFilter={this.handleFilter}
        daysBehind={daysBehind}
      />
    )
  }
}

const mapStateToProps = state => ({
  transactions: state.users.transactions,
  authUsername: state.auth.username,
  authUserType: state.auth.userType,
  mobileView: state.general.mobileView
})

const mapDispatchToProps = dispatch => ({
  getTransactions: args => dispatch(getTransactions(args))
})

export default connect(mapStateToProps, mapDispatchToProps)(Transaction)
