import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Tabs,
  Tab,
  Icon
} from '@material-ui/core'
import { Table } from 'components'

let rows = [
  { field: 'username', label: 'Username', type: 'string' },
  { field: 'email', label: 'Email', type: 'string' },
  { field: 'phoneNo', label: 'Telephone', type: 'string' },
  { field: 'createdAt', label: 'Created At', type: 'date' }
]

const getTableData = (users, filter) => {
  let displayData = []
  for (let user of users) {
    let canPush = false
    switch (filter) {
      case 'active':
        canPush = user.accountStatus
        break
      case 'inactive':
        canPush = !user.accountStatus
        break
      default:
        break
    }
    if (canPush) {
      let userData = {}
      for (const row of rows) {
        userData[row.field] = user[row.field]
      }
      displayData.push({ ...userData })
    }
  }
  return displayData
}

class StaffList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      filter: 'active'
    }
  }

  componentDidMount = () => {
    if (this.props.authUserType !== 'reseller') this.props.history.push('/')
  }

  render () {
    const {
      title,
      authUserType,
      staffs,
      orderBy,
      orderByDirection,
      mobileView,
      embedded,
      color,
      noPagination = false
    } = this.props
    const { filter } = this.state

    return (
      <>
        {!embedded &&
          <Tabs value={filter} onChange={(e, filter) => this.setState({ filter })} >
            <Tab label='Active' value='active' icon={<Icon style={{ color: 'green' }}>thumb_up</Icon>} />
            <Tab label='Inactive' value='inactive' icon={<Icon style={{ color: 'red' }}>thumb_down</Icon>} />
          </Tabs>
        }

        <Table
          title={title || 'Staffs'}
          rows={rows}
          data={getTableData(staffs, filter)}
          orderBy={orderBy || 'username'}
          orderByDirection={orderByDirection || 'desc'}
          mobileView={mobileView}
          gotoLink={(user) => this.props.history.push(`/staffs/${user.username}`)}
          canAdd
          addNew={() => this.props.history.push('/staffs/new')}
          canDownload={authUserType === 'superAdmin'}
          tableHeight={embedded ? '100%' : mobileView ? '75vh' : '85vh'}
          limit={embedded ? 5 : 99999}
          noPagination={noPagination}
          backgroundColor={embedded ? color : ''}
          headingColor={embedded ? 'white' : ''}
        />
      </>
    )
  }
}

const mapStateToProps = state => ({
  staffs: state.users.staffs,
  mobileView: state.general.mobileView,
  authUserType: state.auth.userType
})

export default connect(mapStateToProps)(StaffList)
