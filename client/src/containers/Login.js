import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  TextField,
  Button,
  Typography,
  Paper
} from '@material-ui/core'
import {
  Notification,
  Loading,
  OfflinePopup
} from 'components'
import {
  LogoImage
} from 'assets'

import { login, getLoginActivities } from 'actions/auth'
import { getUsers, getConfig } from 'actions/users'
import { getStaffs } from 'actions/staffs'
import { getTransactions } from 'actions/transactions'
import {
  getActivities,
  getStats,
  getTariffPlans
} from 'actions/general'

const Wrapper = styled(Paper)`
  max-width: 400px;
  max-height: 600px;
  margin: 10vh auto;
  text-align: center;
  background: #fff !important;
  padding: 10px;
`

const LoginButton = styled(Button)`
  margin-top: 20px !important;
  max-width: 70%;
`

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }
  }

  componentDidMount = () => {
    if (this.props.token) this.props.history.push('/')
  }

  componentDidUpdate = () => {
    if (this.props.token) this.props.history.push('/')
  }

  handleTextChange = (event) => {
    this.setState({ [event.target.name]: event.target.value.trim() })
  }

  login = (event) => {
    event.preventDefault()
    const { username, password } = this.state
    this.props.login({ username, password })
      .then(({ type, payload }) => {
        if (type === 'LOGIN_SUCCESS') {
          if (payload.data.user.userType !== 'specialDeleter') {
            Promise.all([
              this.props.getLoginActivities(),
              this.props.getConfig(),
              this.props.getUsers(),
              this.props.getStats(),
              payload.data.user.userType === 'reseller' && payload.data.user.storeAdmin === 'me' && this.props.getStaffs(),
              this.props.getTransactions(),
              this.props.getActivities(),
              this.props.getTariffPlans()
            ])
          }
        }
      })
  }

  render () {
    return (
      <Wrapper elevation={24}>

        <Loading />
        <Notification />
        <OfflinePopup />

        <Typography variant='h4'>G1HD Billing</Typography>

        <img src={LogoImage} alt='Logo' style={{ maxWidth: 200, padding: 20 }} />

        <form onSubmit={this.login} style={{ padding: 10, display: 'grid', justifyItems: 'center' }}>
          <TextField
            name='username'
            label='Username'
            type='username'
            autoComplete='username'
            fullWidth
            autoFocus
            onChange={this.handleTextChange}
            value={this.state.username}
            required
            margin='normal'
          />
          <TextField
            name='password'
            label='Password'
            type='password'
            autoComplete='password'
            fullWidth
            onChange={this.handleTextChange}
            value={this.state.password}
            required
            margin='normal'
          />
          <LoginButton variant='contained' type='submit' color='primary' fullWidth>
            Login
          </LoginButton>
        </form>
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  token: state.auth.token
})

const mapDispatchToProps = dispatch => ({
  login: credentials => dispatch(login(credentials)),
  getConfig: () => dispatch(getConfig()),
  getUsers: () => dispatch(getUsers()),
  getStaffs: () => dispatch(getStaffs()),
  getTransactions: () => dispatch(getTransactions()),
  getActivities: () => dispatch(getActivities()),
  getStats: () => dispatch(getStats()),
  getTariffPlans: () => dispatch(getTariffPlans()),
  getLoginActivities: () => dispatch(getLoginActivities())
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
