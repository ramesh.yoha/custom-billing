import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual } from 'lodash'
import styled from 'styled-components'
import InputMask from 'react-input-mask'
import {
  Typography,
  TextField,
  Button,
  Switch,
  FormControlLabel,
  Icon
} from '@material-ui/core'
import {
  Confirmation
} from 'components'
import { Activities } from 'containers'
import {
  Card,
  Form
} from '_helpers/components'
import {
  isValidPhone
} from '_helpers/validations'
import { defaultColors } from '_helpers/colors'

import {
  updateUser,
  deleteUser
} from 'actions/users'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`
class SpecialDeleterEdit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: null,
      editingUser: null,
      confirmation: false,
      confirmationMessage: '',
      newPassword: ''
    }
  }

  componentDidMount = () => {
    if (this.props.authUserType !== 'superAdmin' && this.props.authUserType !== 'admin') this.props.history.push('/')
    const username = this.props.match.params.id
    const user = this.findSpecialDeleterFromUsername(username)
    if (user) this.setState({ user, editingUser: { ...user } })
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    const username = this.props.match.params.id
    const user = this.findSpecialDeleterFromUsername(username)
    if (!isEqual(prevState.user, user)) {
      this.setState({ user, editingUser: { ...user } })
    }
  }

  findSpecialDeleterFromUsername = (username) => {
    return this.props.specialDeleters.find(specialDeleter => specialDeleter.username === username)
  }

  handleTextChange = (field, value) => {
    this.setState({ editingUser: { ...this.state.editingUser, [field]: value } })
  }

  confirmationProceed = () => this.setState({ confirmation: false })
  confirmationCancel = () => this.setState({ confirmation: false })

  updateUser = (event) => {
    event.preventDefault()
    const { username } = this.state.editingUser
    const { email, firstName, lastName, phoneNo, accountStatus } = this.state.editingUser
    if (this.state.newPassword) {
      this.confirmationProceed = () => {
        this.setState({ confirmation: false }, () => {
          this.props.updateUser(username, { email, password: this.state.newPassword, firstName, lastName, phoneNo, accountStatus })
            .then(() => this.props.sync())
            .then(() => this.setState({ newPassword: '' }))
        })
      }
      this.setState({ confirmation: true, confirmationMessage: 'You are changing the user\'s password. Are you sure you want to continue ?' })
    } else {
      this.props.updateUser(username, { email, firstName, lastName, phoneNo, accountStatus })
        .then(() => this.props.sync())
    }
  }

  deleteUser = (event) => {
    event.preventDefault()
    let { username } = this.state.user
    this.confirmationProceed = () => {
      this.setState({ confirmation: false }, () => {
        this.props.deleteUser(username)
          .then(() => this.props.history.push('/specialDeleters'))
          .then(() => this.props.sync())
      })
    }
    this.setState({ confirmation: true, confirmationMessage: 'Are you sure you want to delete this specialDeleter ?', newPassword: '' })
  }

  render () {
    if (!this.state.user) {
      return (
        <Wrapper>
          <Typography variant='h4' noWrap>
              Staff with username {this.props.match.params.id} was not found
          </Typography>
        </Wrapper>
      )
    }

    const {
      loading,
      mobileView,
      colors = defaultColors
    } = this.props

    const firstNameEmpty = this.state.editingUser.firstName === ''
    const lastNameEmpty = this.state.editingUser.lastName === ''
    const phoneNoInvalid = this.state.editingUser.phoneNo === '' || !isValidPhone(this.state.editingUser.phoneNo)
    const noChanges = isEqual(this.state.editingUser, this.state.user)

    const invalidForm = firstNameEmpty || lastNameEmpty || phoneNoInvalid || noChanges

    return (
      <Wrapper>
        <Card title={`Edit Special Deleter:  ${this.state.user.username}`} color={colors.user_profileUpdate}>
          {this.state.editingUser &&
            <Form
              onSubmit={this.updateUser}
              style={{ gridTemplateColumns: mobileView ? '1fr' : '1fr 1fr' }}
              actions={[
                <Button
                  key='update'
                  variant='contained'
                  type='submit'
                  color='primary'
                  disabled={
                    (!this.state.newPassword) &&
                    (loading || invalidForm || noChanges)
                  }
                >
                  Update <Icon style={{ marginLeft: 5 }} > save </Icon>
                </Button>,
                <Button
                  key='delete'
                  variant='contained'
                  type='submit'
                  color='secondary'
                  disabled={loading}
                  onClick={this.deleteUser}
                >
                  Delete <Icon style={{ marginLeft: 5 }} > delete </Icon>
                </Button>
              ]}
            >
              <TextField
                label='Email'
                type='email'
                value={this.state.editingUser.email}
                onChange={(e) => this.handleTextChange('email', e.target.value)}
                fullWidth
                disabled={loading}
              />
              <InputMask mask='999-999-9999'
                value={this.state.editingUser.phoneNo}
                onChange={(e) => this.handleTextChange('phoneNo', e.target.value)}
              >
                {(inputProps) => (
                  <TextField
                    {...inputProps}
                    label='Phone'
                    required
                    fullWidth
                    error={phoneNoInvalid}
                    helperText={phoneNoInvalid ? 'Invalid Phone' : null}
                  />
                )}
              </InputMask>
              <TextField
                label='First Name'
                type='firstName'
                required
                value={this.state.editingUser.firstName}
                onChange={(e) => this.handleTextChange('firstName', e.target.value)}
                fullWidth
                disabled={loading}
                error={firstNameEmpty}
                helperText={firstNameEmpty ? 'Required' : null}
              />
              <TextField
                label='Last Name'
                type='lastName'
                required
                value={this.state.editingUser.lastName}
                onChange={(e) => this.handleTextChange('lastName', e.target.value)}
                fullWidth
                disabled={loading}
                error={lastNameEmpty}
                helperText={lastNameEmpty ? 'Required' : null}
              />
              <TextField
                label='Change Password'
                type='password'
                placeholder='Leave empty to keep the current password unchanged'
                value={this.state.newPassword}
                onChange={(e) => this.setState({ newPassword: e.target.value })}
                fullWidth
                disabled={loading}
              />

              <FormControlLabel
                label={`Account Status (${this.state.editingUser.accountStatus ? 'Active' : 'Inactive'})`}
                control={
                  <Switch
                    checked={this.state.editingUser.accountStatus}
                    onChange={(e) => this.handleTextChange('accountStatus', e.target.checked)}
                    value={this.state.editingUser.accountStatus}
                    color='primary'
                    disabled={loading}
                    style={{ color: this.state.editingUser.accountStatus ? 'green' : 'red' }}
                  />
                }
              />
            </Form>
          }
        </Card>

        <Activities
          username={this.state.user.username}
          type='user'
          embedded
          title='Recent Activities'
          limit={9999}
          color={colors.user_activities}
        />

        <Confirmation
          open={this.state.confirmation}
          message={
            this.state.newPassword ? this.state.confirmationMessage
              : this.state.user.childUsernames.length > 0 ? 'User has active children. Please remove/move all children before deleting.'
                : (this.state.user.creditsAvailable + this.state.user.creditsOwed > 0) ? 'User has active credits. Please recover them before deleting.'
                  : this.state.confirmationMessage
          }
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
          disabled={!this.state.newPassword && (this.state.user.childUsernames.length > 0 || (this.state.user.creditsAvailable + this.state.user.creditsOwed > 0))}
        />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  specialDeleters: state.users.specialDeleters,
  mobileView: state.general.mobileView,
  loading: state.general.loading,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateUser: (username, user) => dispatch(updateUser(username, user)),
  deleteUser: (username) => dispatch(deleteUser(username))
})

export default connect(mapStateToProps, mapDispatchToProps)(SpecialDeleterEdit)
