import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual, startCase } from 'lodash'
import styled from 'styled-components'
import InputMask from 'react-input-mask'
import {
  Typography,
  TextField,
  Button,
  Switch,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Icon
} from '@material-ui/core'
import {
  Confirmation
} from 'components'
import {
  Transaction,
  Activities
} from 'containers'
import {
  Card,
  Form
} from '_helpers/components'
import {
  isValidPhone,
  validateUserCreditsTransfer
} from '_helpers/validations'
import Loading from 'components/Loading'
import { defaultColors } from '_helpers/colors'

import {
  updateUser,
  deleteUser,
  upgradeUser
} from 'actions/users'
import { updateCredits } from 'actions/transactions'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const TopRowWrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-gap: 20px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

class UserEdit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: null,
      editingUser: null,
      confirmation: false,
      confirmationMessage: '',
      credits: {
        value: props.minimumTransferrableCredits,
        action: 'add'
      },
      newPassword: '',
      upgradingUser: false,
      upgradingNewUsername: '',
      upgradingNewPassword: '',
      upgradingNewPasswordConfirmation: ''
    }
  }

  componentDidMount = () => {
    const username = this.props.match.params.id
    const user = this.findInternalUserFromUsername(username)
    if (user) this.setState({ user, editingUser: { ...user } })
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    const username = this.props.match.params.id
    const user = this.findInternalUserFromUsername(username)
    if (user) {
      if (!isEqual(prevState.user, user)) {
        this.setState({ user, editingUser: { ...user } })
      }
    }
  }

  findInternalUserFromUsername = (username) => {
    let user
    user = this.props.admins.find(admin => admin.username === username)
    if (!user) user = this.props.superResellers.find(superReseller => superReseller.username === username)
    if (!user) user = this.props.resellers.find(reseller => reseller.username === username)
    return user
  }

  handleTextChange = (field, value) => {
    this.setState({ editingUser: { ...this.state.editingUser, [field]: value } })
  }

  confirmationProceed = () => this.setState({ confirmation: false })
  confirmationCancel = () => this.setState({ confirmation: false })

  updateUser = (event) => {
    event.preventDefault()
    const { username } = this.state.editingUser
    const { email, firstName, lastName, phoneNo, accountStatus } = this.state.editingUser
    if (this.state.newPassword) {
      this.confirmationProceed = () => {
        this.setState({ confirmation: false }, () => {
          this.props.updateUser(username, { email, password: this.state.newPassword, firstName, lastName, phoneNo, accountStatus })
            .then(() => this.props.sync())
            .then(() => this.setState({ newPassword: '' }))
        })
      }
      this.setState({ confirmation: true, confirmationMessage: 'You are changing the user\'s password. Are you sure you want to continue ?' })
    } else {
      this.props.updateUser(username, { email, firstName, lastName, phoneNo, accountStatus })
        .then(() => this.props.sync())
    }
  }

  deleteUser = (event) => {
    event.preventDefault()
    const { username, userType } = this.state.user
    this.confirmationProceed = () => {
      this.setState({ confirmation: false }, () => {
        this.props.deleteUser(username)
          .then(() => this.props.history.push(`/${userType}s`))
          .then(() => this.props.sync())
      })
    }
    this.setState({ confirmation: true, confirmationMessage: 'Are you sure you want to delete this user ?', newPassword: '' })
  }

  updateCredits = (e) => {
    e.preventDefault()
    this.props.updateCredits({
      credits: this.state.credits.action === 'add' ? this.state.credits.value : this.state.credits.value * -1,
      description: `${startCase(this.state.credits.action)}ed ${this.state.credits.value} credits`,
      transactionTo: this.state.user.username
    })
      .then(() => this.props.sync())
  }

  upgradeUser = (e) => {
    e.preventDefault()
    const username = this.state.user.username
    const { upgradingNewUsername, upgradingNewPassword } = this.state
    const upgradingUserType = this.getUpgradingUserType()
    const upgradingUser = {
      username: upgradingNewUsername,
      userType: upgradingUserType,
      password: upgradingNewPassword
    }
    this.props.upgradeUser(username, upgradingUser)
      .then((upgradeResponse) => {
        if (upgradeResponse.type === 'UPGRADE_USER_SUCCESS') {
          this.props.sync()
            .then(() => this.props.history.push(`/${upgradingUserType}s`))
        } else {
          this.setState({ upgradingUser: false })
        }
      })
  }

  getUpgradingUserType = () => {
    switch (this.state.user.userType) {
      case 'reseller':
        return 'superReseller'
      case 'superReseller':
        return 'admin'
      default:
        return false
    }
  }

  userIsUpgradable = () => {
    const alreadyUpgraded = this.state.user.upgradedAccount
    const isUpgradableUserType = ['superReseller', 'reseller'].includes(this.state.user.userType)
    if ((this.state.user.userType === 'superReseller') && (this.props.authUserType !== 'superAdmin')) return false
    else if ((this.state.user.userType === 'reseller') && (this.props.authUserType !== 'admin')) return false
    else return !alreadyUpgraded && isUpgradableUserType
  }

  render () {
    if (!this.state.user) {
      return (
        <Wrapper>
          <Typography variant='h4' noWrap>
              User with username {this.props.match.params.id} was not found
          </Typography>
        </Wrapper>
      )
    }

    const userIsUpgradable = this.userIsUpgradable()

    const {
      authCreditsAvailable,
      authUserType,
      authUsername,
      minimumTransferrableCredits,
      nonRefundableCredits,
      loading,
      mobileView,
      colors = defaultColors
    } = this.props

    const firstNameEmpty = this.state.editingUser.firstName === ''
    const lastNameEmpty = this.state.editingUser.lastName === ''
    const phoneNoInvalid = this.state.editingUser.phoneNo === '' || !isValidPhone(this.state.editingUser.phoneNo)
    const creditsValidation = validateUserCreditsTransfer(this.state.credits.value, this.state.user.creditsAvailable, this.state.credits.action, authCreditsAvailable, authUserType, minimumTransferrableCredits, nonRefundableCredits)
    const noChanges = isEqual(this.state.editingUser, this.state.user)

    const invalidForm = firstNameEmpty || lastNameEmpty || phoneNoInvalid || noChanges

    return (
      <Wrapper>
        <TopRowWrapper>
          <Card title={`Edit User:  ${this.state.user.username}`} color={colors.user_profileUpdate}>
            {this.state.editingUser &&
              <Form
                onSubmit={this.updateUser}
                style={{ gridTemplateColumns: mobileView ? '1fr' : '1fr 1fr' }}
                actions={[
                  <Button
                    key='update'
                    variant='contained'
                    type='submit'
                    color='primary'
                    disabled={
                      (!this.state.newPassword) &&
                      (loading || invalidForm || noChanges)
                    }
                  >
                    Update <Icon style={{ marginLeft: 5 }} > save </Icon>
                  </Button>,
                  userIsUpgradable &&
                  <Button
                    key='upgrade'
                    variant='contained'
                    color='primary'
                    disabled={(loading)}
                    onClick={() => this.getUpgradingUserType() && this.setState({ upgradingUser: true })}
                  >
                    Upgrade <Icon style={{ marginLeft: 5 }} > trending_up </Icon>
                  </Button>,
                  <Button
                    key='delete'
                    variant='contained'
                    type='submit'
                    color='secondary'
                    disabled={loading}
                    onClick={this.deleteUser}
                  >
                    Delete <Icon style={{ marginLeft: 5 }} > delete </Icon>
                  </Button>
                ]}
              >
                <TextField
                  label='Email'
                  type='email'
                  value={this.state.editingUser.email}
                  onChange={(e) => this.handleTextChange('email', e.target.value)}
                  fullWidth
                  disabled={loading}
                />
                <InputMask mask='999-999-9999'
                  value={this.state.editingUser.phoneNo}
                  onChange={(e) => this.handleTextChange('phoneNo', e.target.value)}
                >
                  {(inputProps) => (
                    <TextField
                      {...inputProps}
                      label='Phone'
                      required
                      fullWidth
                      error={phoneNoInvalid}
                      helperText={phoneNoInvalid ? 'Invalid Phone' : null}
                    />
                  )}
                </InputMask>
                <TextField
                  label='First Name'
                  type='firstName'
                  required
                  value={this.state.editingUser.firstName}
                  onChange={(e) => this.handleTextChange('firstName', e.target.value)}
                  fullWidth
                  disabled={loading}
                  error={firstNameEmpty}
                  helperText={firstNameEmpty ? 'Required' : null}
                />
                <TextField
                  label='Last Name'
                  type='lastName'
                  required
                  value={this.state.editingUser.lastName}
                  onChange={(e) => this.handleTextChange('lastName', e.target.value)}
                  fullWidth
                  disabled={loading}
                  error={lastNameEmpty}
                  helperText={lastNameEmpty ? 'Required' : null}
                />
                <TextField
                  label='Change Password'
                  type='password'
                  placeholder='Leave empty to keep the current password unchanged'
                  value={this.state.newPassword}
                  onChange={(e) => this.setState({ newPassword: e.target.value })}
                  fullWidth
                  disabled={loading}
                />

                <FormControlLabel
                  label={`Account Status (${this.state.editingUser.accountStatus ? 'Active' : 'Inactive'})`}
                  control={
                    <Switch
                      checked={this.state.editingUser.accountStatus}
                      onChange={(e) => this.handleTextChange('accountStatus', e.target.checked)}
                      value={this.state.editingUser.accountStatus}
                      color='primary'
                      disabled={loading}
                      style={{ color: this.state.editingUser.accountStatus ? 'green' : 'red' }}
                    />
                  }
                />
              </Form>
            }
          </Card>

          <Card title={'Credits'} color={colors.user_credits}>
            {this.state.user && authUsername === this.state.user.parentUsername &&
              <>
                <Form
                  onSubmit={this.updateCredits}
                  actions={[
                    <Button
                      key='submit'
                      variant='contained'
                      type='submit'
                      color='primary'
                      disabled={loading || !creditsValidation.isValid}
                      onClick={this.updateCredits}
                    >
                      Submit <Icon style={{ marginLeft: 5 }} > save </Icon>
                    </Button>
                  ]}
                >
                  <TextField
                    label='Select Credits'
                    type='number'
                    inputProps={{ min: this.state.credits.action === 'recover' ? (authUserType === 'superReseller' ? nonRefundableCredits : 1) : minimumTransferrableCredits }}
                    value={this.state.credits.value || 0}
                    onChange={(e) => this.setState({ credits: { ...this.state.credits, value: parseInt(e.target.value) } })}
                    fullWidth
                    disabled={loading}
                    error={!creditsValidation.isValid}
                    helperText={creditsValidation.errorMessage}
                  />
                  <FormControl component='fieldset'>
                    <RadioGroup
                      aria-label='Gender'
                      name='gender1'
                      value={this.state.credits.action}
                      style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center' }}
                      onChange={(e) => this.setState({ credits: { ...this.state.credits, action: e.target.value } })}
                    >
                      <FormControlLabel
                        value='add'
                        control={<Radio disabled={loading} />}
                        label='Add'
                      />
                      <FormControlLabel
                        value='recover'
                        control={<Radio disabled={loading} />}
                        label='Recover'
                      />
                    </RadioGroup>
                  </FormControl>
                </Form>
                <br />
              </>
            }
            <div style={{ textAlign: 'center', alignItems: 'center', display: 'grid', gridGap: 10, gridTemplateColumns: this.state.user.userType === 'reseller' ? '1fr 1fr' : '1fr' }}>
              <>
                Credits Available <div style={{ fontSize: 50 }}> {this.state.user.creditsAvailable} </div>
              </>
              {this.state.user.userType === 'reseller' &&
              <>
              Credits Owed<div style={{ fontSize: 50 }}> {this.state.user.creditsOwed} </div>
              </>
              }
            </div>
          </Card>
        </TopRowWrapper>

        <Transaction
          username={this.state.user.username}
          type='user'
          embedded title='Recent Transactions'
          limit={9999}
          color={colors.user_transactions}
        />

        <Activities
          username={this.state.user.username}
          type='user'
          embedded
          title='Recent Activities'
          limit={9999}
          color={colors.user_activities}
        />

        <Confirmation
          open={this.state.confirmation}
          message={
            this.state.newPassword ? this.state.confirmationMessage
              : this.state.user.childUsernames.length > 0 ? 'User has active children. Please remove/move all children before deleting.'
                : (this.state.user.creditsAvailable + this.state.user.creditsOwed > 0) ? 'User has active credits. Please recover them before deleting.'
                  : this.state.confirmationMessage
          }
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
          disabled={!this.state.newPassword && (this.state.user.childUsernames.length > 0 || (this.state.user.creditsAvailable + this.state.user.creditsOwed > 0))}
        />
        <Dialog
          open={this.state.upgradingUser}
          aria-labelledby='form-dialog-title'
          fullWidth
        >
          <form onSubmit={this.upgradeUser}>
            <DialogTitle id='form-dialog-title'>
              Upgrade {this.state.user.username} to {this.getUpgradingUserType()}
            </DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                id='upgradingNewUsername'
                label='Upgrading Username'
                type='text'
                required
                value={this.state.upgradingNewUsername}
                onChange={(e) => this.setState({ upgradingNewUsername: e.target.value.trim() })}
                fullWidth
                error={this.state.upgradingNewUsername === ''}
                helperText={this.state.upgradingNewUsername === '' ? 'Required' : null}
              />
              <TextField
                label='Password'
                type='password'
                required
                value={this.state.upgradingNewPassword}
                onChange={(e) => this.setState({ upgradingNewPassword: e.target.value })}
                fullWidth
                disabled={loading}
                error={this.state.upgradingNewPassword === ''}
                helperText={this.state.upgradingNewPassword && this.state.upgradingNewPassword === '' ? 'Required' : null}
              />
              <TextField
                label='Password Confirmation'
                type='password'
                required
                value={this.state.upgradingNewPasswordConfirmation}
                onChange={(e) => this.setState({ upgradingNewPasswordConfirmation: e.target.value })}
                fullWidth
                disabled={loading}
                error={
                  (this.state.upgradingNewPasswordConfirmation === '') ||
                  (this.state.upgradingNewPasswordConfirmation !== this.state.upgradingNewPassword)
                }
                helperText={
                  this.state.upgradingNewPasswordConfirmation === '' ? 'Required'
                    : (this.state.upgradingNewPasswordConfirmation !== this.state.upgradingNewPassword) ? 'Password & Password Confirmation must match'
                      : null
                }
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={() => this.setState({ upgradingUser: false })} color='secondary'>
                Cancel
              </Button>
              <Button
                variant='contained'
                type='submit'
                color='primary'
                disabled={
                  loading ||
                  this.state.upgradingNewUsername === '' ||
                  (this.state.upgradingNewPassword === '') ||
                  (this.state.upgradingNewPasswordConfirmation !== this.state.upgradingNewPassword)
                }
                onClick={this.upgradeUser}
              >
                Upgrade
              </Button>
            </DialogActions>
          </form>
          <Loading />
        </Dialog>
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  authCreditsAvailable: state.auth.creditsAvailable,
  admins: state.users.admins,
  superResellers: state.users.superResellers,
  resellers: state.users.resellers,
  authUsername: state.auth.username,
  mobileView: state.general.mobileView,
  minimumTransferrableCredits: state.config.minimumTransferrableCredits,
  nonRefundableCredits: state.config.nonRefundableCredits,
  loading: state.general.loading,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateUser: (username, user) => dispatch(updateUser(username, user)),
  updateCredits: (transaction) => dispatch(updateCredits(transaction)),
  deleteUser: (username) => dispatch(deleteUser(username)),
  upgradeUser: (username, upgradingUser) => dispatch(upgradeUser(username, upgradingUser))
})

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)
