import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual, startCase } from 'lodash'
import styled from 'styled-components'
import { SketchPicker } from 'react-color'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'

import {
  TextField,
  Typography,
  Button,
  Checkbox,
  FormControlLabel,
  Icon
} from '@material-ui/core'
import {
  Card,
  Form
} from '_helpers/components'
import { Confirmation } from 'components'
import { updateConfig, getConfig } from 'actions/users'
import { increaseCredits } from 'actions/transactions'

const Wrapper = styled.div`
  display: grid;
  grid-gap: 20px;
`

const Top = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: 1fr 1fr 1fr;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const Bottom = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: 1fr 1fr;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const ThemeColorsWrapper = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const UserTypeTheme = styled.div`
  display: grid;
  grid-gap: 20px;
  justify-items: center;
  grid-template-columns: 1fr;
`

const textEditorModules = {
  toolbar: [
    [{ 'header': [1, 2, false] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
    ['link', 'image'],
    [{
      'color': [
        'rgb(0, 0, 0)',
        'rgb(230, 0, 0)',
        'rgb(255, 153, 0)',
        'rgb(255, 255,   0)',
        'rgb(0, 138, 0)',
        'rgb(0, 102, 204)',
        'rgb(153,  51, 255)',
        'rgb(255, 255, 255)',
        'rgb(250, 204, 204)',
        'rgb(255, 235, 204)', 'rgb(255, 255, 204)', 'rgb(204, 232, 204)',
        'rgb(204, 224, 245)', 'rgb(235, 214, 255)', 'rgb(187, 187, 187)',
        'rgb(240, 102, 102)', 'rgb(255, 194, 102)', 'rgb(255, 255, 102)',
        'rgb(102, 185, 102)', 'rgb(102, 163, 224)', 'rgb(194, 133, 255)',
        'rgb(136, 136, 136)', 'rgb(161, 0, 0)', 'rgb(178, 107, 0)',
        'rgb(178, 178, 0)', 'rgb(0, 97, 0)', 'rgb(0, 71, 178)',
        'rgb(107, 36, 178)', 'rgb( 68, 68, 68)', 'rgb(92, 0, 0)',
        'rgb(102, 61, 0)', 'rgb(102, 102, 0)', 'rgb(0, 55, 0)',
        'rgb(0, 41, 102)', 'rgb( 61, 20, 10)'
      ]
    }],
    ['clean']
  ],
  clipboard: {
    matchVisual: false
  }
}

const textEditorFormats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'color'
]

class SuperAdminConfig extends Component {
  constructor (props) {
    super(props)
    this.state = {
      minimumTransferrableCredits: props.minimumTransferrableCredits,
      nonRefundableCredits: props.nonRefundableCredits,
      increaseCredits: 1,
      UserAnnouncements: props.UserAnnouncements,
      colors: props.colors,
      open: '',
      color: '',
      editorHtml: '',
      userType: props.authUserType,
      allColors: {}
    }
  }

  handleChange = (html) => {
    this.setState({ UserAnnouncements: html })
  }

  componentDidMount = () => {
    if (this.props.authUserType !== 'superAdmin') this.props.history.push('/')
    else {
      this.props.getConfig(true)
        .then(({ type, payload }) => {
          if (type === 'GET_CONFIG_SUCCESS') {
            this.setState({ allColors: payload.data.colors })
          }
        })
    }
  }

  increaseCredits = (e) => {
    e.preventDefault()
    this.props.increaseCredits({
      credits: this.state.increaseCredits,
      description: `Increased own credits by ${this.state.increaseCredits}`,
      transactionTo: this.props.authUsername
    })
      .then(() => this.props.sync(true))
  }

  confirmationProceed = () => {
    const { open, color, userType, allColors } = this.state
    this.setState({ open: '', color: '' }, () => {
      this.props.updateConfig({ configName: 'colors', configValue: { ...allColors, [userType]: { ...allColors[userType], [open]: color.hex } } })
        .then(() => this.props.sync(true))
    })
  }

  confirmationCancel = () => this.setState({ open: '', color: '' })

  render () {
    const { minimumTransferrableCredits, nonRefundableCredits, increaseCredits, UserAnnouncements, colors, open, color, allColors } = this.state
    const { enableSendEventsFor, loading } = this.props

    return (
      <Wrapper>
        <Top>
          <Card title='Minimum Transferrable Credits' color={colors.superAdminConfig_minimumCredits}>
            <Form
              onSubmit={(e) => {
                e.preventDefault()
                this.props.updateConfig({ configName: 'minimumTransferrableCredits', configValue: minimumTransferrableCredits })
                  .then(() => this.props.sync(true))
              }}
              actions={[
                <Button
                  key='update'
                  variant='contained'
                  color='primary'
                  disabled={
                    loading ||
                    minimumTransferrableCredits < 1 ||
                    minimumTransferrableCredits === this.props.minimumTransferrableCredits
                  }
                  onClick={() =>
                    this.props.updateConfig({ configName: 'minimumTransferrableCredits', configValue: minimumTransferrableCredits })
                      .then(() => this.props.sync(true))
                  }
                >
                  Save
                  <Icon style={{ marginLeft: 10 }}> save </Icon>
                </Button>
              ]}
            >
              <TextField
                type='number'
                inputProps={{ min: 1 }}
                value={minimumTransferrableCredits}
                onChange={(e) => this.setState({ minimumTransferrableCredits: e.target.value })}
                disabled={loading}
                error={minimumTransferrableCredits < 1}
                helperText={minimumTransferrableCredits < 1 ? 'Value must be greater 0' : null}
              />
            </Form>
          </Card>

          <Card title='Non Refundable Credits' color={colors.superAdminConfig_nonRefundableCredits}>
            <Form
              onSubmit={(e) => {
                e.preventDefault()
                this.props.updateConfig({ configName: 'nonRefundableCredits', configValue: nonRefundableCredits })
                  .then(() => this.props.sync(true))
              }}
              actions={[
                <Button
                  key='save'
                  variant='contained'
                  color='primary'
                  disabled={
                    loading ||
                    nonRefundableCredits < 1 ||
                    nonRefundableCredits === this.props.nonRefundableCredits
                  }
                  onClick={() =>
                    this.props.updateConfig({ configName: 'nonRefundableCredits', configValue: nonRefundableCredits })
                      .then(() => this.props.sync(true))
                  }
                >
                  Save
                  <Icon style={{ marginLeft: 10 }}> save </Icon>
                </Button>
              ]}
            >
              <TextField
                type='number'
                inputProps={{ min: 1 }}
                value={nonRefundableCredits}
                onChange={(e) => this.setState({ nonRefundableCredits: e.target.value })}
                disabled={loading}
                error={nonRefundableCredits < 1}
                helperText={nonRefundableCredits < 1 ? 'Value must be greater 0' : null}
              />
            </Form>
          </Card>

          <Card title='Increase My Credits' color={colors.superAdminConfig_increaseCredits}>
            <Form
              onSubmit={this.increaseCredits}
              actions={[
                <Button
                  key='save'
                  variant='contained'
                  color='primary'
                  disabled={loading || increaseCredits < 1}
                  onClick={this.increaseCredits}
                >
                  Increase
                  <Icon style={{ marginLeft: 10 }}> arrow_upward </Icon>
                </Button>
              ]}
            >
              <TextField
                type='number'
                inputProps={{ min: 1 }}
                value={increaseCredits}
                onChange={(e) => this.setState({ increaseCredits: e.target.value })}
                disabled={loading}
                error={increaseCredits < 1}
                helperText={increaseCredits < 1 ? 'Value must be greater 0' : null}
              />
            </Form>
          </Card>
        </Top>

        <Bottom>
          <Card title='Announcements' color={colors.superAdminConfig_annoucements}>
            <Form
              onSubmit={(e) => {
                e.preventDefault()
                this.props.updateConfig({ configName: 'UserAnnouncements', configValue: UserAnnouncements.toString('html') })
                  .then(() => this.props.sync(true))
              }}
              actions={[
                <Button
                  key='save'
                  variant='contained'
                  color='primary'
                  disabled={loading || isEqual(UserAnnouncements.toString('html'), this.props.UserAnnouncements)}
                  onClick={() =>
                    this.props.updateConfig({ configName: 'UserAnnouncements', configValue: UserAnnouncements.toString('html') })
                      .then(() => this.props.sync(true))
                  }
                >
                  Save
                  <Icon style={{ marginLeft: 10 }}> save </Icon>
                </Button>
              ]}
            >
              <div style={{ maxHeight: 300, overflowY: 'auto' }}>
                <ReactQuill
                  value={UserAnnouncements}
                  onChange={this.handleChange}
                  modules={textEditorModules}
                  formats={textEditorFormats}
                />
              </div>
            </Form>
          </Card>

          <Card title='Sending Events Capability' color={colors.superAdminConfig_eventsCapability} style={{ height: 'fit-content' }}>
            <Form>
              <Typography>Select types of internal users who are allowed to send events to clients</Typography>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={enableSendEventsFor['admin']}
                    onChange={() =>
                      this.props.updateConfig({
                        configName: 'enableSendEventsFor',
                        configValue: { ...enableSendEventsFor, admin: !enableSendEventsFor['admin'] }
                      })
                        .then(() => this.props.sync(true))
                    }
                  />
                }
                label='Admin'
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={enableSendEventsFor['superReseller']}
                    onChange={() =>
                      this.props.updateConfig({
                        configName: 'enableSendEventsFor',
                        configValue: { ...enableSendEventsFor, superReseller: !enableSendEventsFor['superReseller'] }
                      })
                        .then(() => this.props.sync(true))
                    }
                  />
                }
                label='Super Reseller'
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={enableSendEventsFor['reseller']}
                    onChange={() =>
                      this.props.updateConfig({
                        configName: 'enableSendEventsFor',
                        configValue: { ...enableSendEventsFor, reseller: !enableSendEventsFor['reseller'] }
                      })
                        .then(() => this.props.sync(true))
                    }
                  />
                }
                label='Reseller'
              />
            </Form>
          </Card>
        </Bottom>

        <Card title='Set Theme colors' color={colors.superAdminConfig_themeColors} style={{ height: 'fit-content' }}>
          { Object.keys(allColors).length > 0 &&
            <ThemeColorsWrapper>
              {['superAdmin', 'admin', 'superReseller', 'reseller'].map(userType =>
                <UserTypeTheme key={userType}>
                  <Typography variant='h6'>{startCase(userType)}</Typography>
                  {Object.entries(allColors[userType]).map(([key, value]) =>
                    <Button
                      key={key}
                      fullWidth
                      style={{ background: value, color: 'white' }}
                      onClick={() => this.setState({ open: key, color: value, userType })}
                    >
                      {key}
                    </Button>
                  )}
                </UserTypeTheme>
              )}
            </ThemeColorsWrapper>
          }
        </Card>

        <Confirmation
          open={open !== ''}
          title={`Pick a color to set for ${open}`}
          message={<SketchPicker color={color} width='auto' onChangeComplete={color => this.setState({ color })} />}
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
          disableBackdropClick={false}
          disableEscapeKeyDown={false}
        />

      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  authUsername: state.auth.username,
  minimumTransferrableCredits: state.config.minimumTransferrableCredits,
  UserAnnouncements: state.config.UserAnnouncements,
  enableSendEventsFor: state.config.enableSendEventsFor,
  nonRefundableCredits: state.config.nonRefundableCredits,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateConfig: config => dispatch(updateConfig(config)),
  increaseCredits: (transaction) => dispatch(increaseCredits(transaction)),
  getConfig: (special) => dispatch(getConfig(special))
})

export default connect(mapStateToProps, mapDispatchToProps)(SuperAdminConfig)
