import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table } from 'components'

let rows = [
  { field: 'username', label: 'Username', type: 'string' },
  { field: 'email', label: 'Email', type: 'string' },
  { field: 'phoneNo', label: 'Telephone', type: 'string' },
  { field: 'createdAt', label: 'Created At', type: 'date' }
]

const getTableData = (users) => {
  let displayData = []
  for (let user of users) {
    let userData = {}
    for (const row of rows) {
      userData[row.field] = user[row.field]
    }
    displayData.push({ ...userData })
  }
  return displayData
}

const checkPermissionToAdd = (authUserType) => {
  return authUserType === 'superAdmin' || authUserType === 'admin'
}

class UserList extends Component {
  componentDidMount = () => {
    if (this.props.authUserType !== 'superAdmin' && this.props.authUserType !== 'admin') this.props.history.push('/')
  }

  render () {
    const {
      authUserType,
      orderBy,
      orderByDirection,
      mobileView,
      embedded,
      noPagination = false,
      color,
      specialDeleters
    } = this.props

    return (
      <Table
        title='Special Deleters'
        rows={rows}
        data={getTableData(specialDeleters)}
        orderBy={orderBy || 'username'}
        orderByDirection={orderByDirection || 'desc'}
        mobileView={mobileView}
        gotoLink={(user) => this.props.history.push(`/specialDeleters/${user.username}`)}
        canAdd={!embedded && checkPermissionToAdd(authUserType)}
        addNew={() => this.props.history.push('/specialDeleters/new')}
        canDownload={authUserType === 'superAdmin'}
        tableHeight={embedded ? '100%' : mobileView ? '75vh' : '85vh'}
        limit={embedded ? 5 : 99999}
        noPagination={noPagination}
        backgroundColor={embedded ? color : ''}
        headingColor={embedded ? 'white' : ''}
      />
    )
  }
}

const mapStateToProps = state => ({
  specialDeleters: state.users.specialDeleters,
  mobileView: state.general.mobileView,
  authUserType: state.auth.userType
})

export default connect(mapStateToProps)(UserList)
