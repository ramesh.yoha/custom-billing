import React, { Component } from 'react'
import { connect } from 'react-redux'
import InputMask from 'react-input-mask'
import {
  TextField,
  Button,
  Icon
} from '@material-ui/core'
import {
  Card,
  Form
} from '_helpers/components'
import {
  isValidPhone
} from '_helpers/validations'
import { defaultColors } from '_helpers/colors'

import { getAllUsernames } from 'actions/users'
import { addStaff } from 'actions/staffs'

class UserAdd extends Component {
  constructor (props) {
    super(props)
    this.state = {
      newUser: {
        username: '',
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        phoneNo: '',
        accountStatus: true
      },
      existingUsernames: []
    }
  }

  componentDidMount = () => {
    if (this.props.authUserType !== 'reseller') this.props.history.push('/')
    else {
      // Get all existing usernames
      this.props.getAllUsernames()
        .then(responseJSON => {
          if (responseJSON.type === 'GET_ALL_USERNAMES_SUCCESS') {
            this.setState({ existingUsernames: responseJSON.payload.data })
          }
        })
    }
  }

  handleTextChange = (field, value) => {
    this.setState({ newUser: { ...this.state.newUser, [field]: value } })
  }

  addStaff = (event) => {
    event.preventDefault()
    this.props.addStaff(this.state.newUser)
      .then(userAddResponse => {
        if (userAddResponse.type === 'ADD_STAFF_SUCCESS') {
          this.props.sync()
            .then(() => this.props.history.push(`/staffs/${this.state.newUser.username}`))
        }
      })
  }

  render () {
    const {
      colors = defaultColors,
      loading
    } = this.props

    const usernameEmpty = this.state.newUser.username === ''
    const usernameExist = this.state.existingUsernames.includes(this.state.newUser.username)
    const usernameContainsDeleted = this.state.newUser.username.includes('deleted_')
    const usernameLessThanEightCharacters = !usernameEmpty && this.state.newUser.username.length < 8
    const passwordEmpty = this.state.newUser.password === ''
    const firstNameEmpty = this.state.newUser.firstName === ''
    const lastNameEmpty = this.state.newUser.lastName === ''
    const phoneNoEmpty = this.state.newUser.phoneNo === ''
    const phoneNoInvalid = !isValidPhone(this.state.newUser.phoneNo)

    const invalidForm = usernameEmpty || usernameExist || usernameContainsDeleted || usernameLessThanEightCharacters || passwordEmpty || firstNameEmpty || lastNameEmpty || phoneNoEmpty || phoneNoInvalid

    return (
      <Card title='Create a new Staff' color={colors.user_add}>
        <Form onSubmit={this.addStaff}
          actions={
            [
              <Button
                key='cancel'
                variant='contained'
                color='secondary'
                onClick={() => this.props.history.push('/staffs')}
              >
                Cancel <Icon style={{ marginLeft: 10 }} > cancel </Icon>
              </Button>,
              <Button
                key='submit'
                variant='contained'
                type='submit'
                color='primary'
                disabled={loading || invalidForm}
              >
                Submit <Icon style={{ marginLeft: 10 }} > save </Icon>
              </Button>
            ]
          }
        >
          <TextField
            label='Username'
            type='username'
            required
            value={this.state.newUser.username}
            onChange={(e) => this.handleTextChange('username', e.target.value.trim().toLowerCase())}
            fullWidth
            disabled={loading}
            autoFocus
            error={usernameEmpty || usernameExist || usernameContainsDeleted || usernameLessThanEightCharacters}
            helperText={
              usernameEmpty ? 'Required'
                : usernameExist ? 'Username already exists'
                  : usernameContainsDeleted ? 'Cannot have a username containing with deleted_'
                    : usernameLessThanEightCharacters ? 'Minimum 8 characters needed for username'
                      : null
            }
          />
          <TextField
            label='Email'
            type='email'
            value={this.state.newUser.email}
            onChange={(e) => this.handleTextChange('email', e.target.value)}
            fullWidth
            disabled={loading}
          />
          <TextField
            label='Password'
            type='password'
            required
            value={this.state.newUser.password}
            onChange={(e) => this.handleTextChange('password', e.target.value)}
            fullWidth
            disabled={loading}
            error={passwordEmpty}
            helperText={passwordEmpty ? 'Required' : null}
          />
          <TextField
            label='First Name'
            type='firstName'
            required
            value={this.state.newUser.firstName}
            onChange={(e) => this.handleTextChange('firstName', e.target.value)}
            fullWidth
            disabled={loading}
            error={firstNameEmpty}
            helperText={firstNameEmpty ? 'Required' : null}
          />
          <TextField
            label='Last Name'
            type='lastName'
            required
            value={this.state.newUser.lastName}
            onChange={(e) => this.handleTextChange('lastName', e.target.value)}
            fullWidth
            disabled={loading}
            error={lastNameEmpty}
            helperText={lastNameEmpty ? 'Required' : null}
          />
          <InputMask mask='999-999-9999'
            value={this.state.newUser.phoneNo}
            onChange={(e) => this.handleTextChange('phoneNo', e.target.value)}
          >
            {(inputProps) => (
              <TextField
                {...inputProps}
                label='Phone'
                fullWidth
                required
                disabled={loading}
                error={phoneNoEmpty || phoneNoInvalid}
                helperText={
                  phoneNoEmpty ? 'Required'
                    : phoneNoInvalid ? 'Invalid Phone'
                      : null
                }
              />
            )}
          </InputMask>
        </Form>
      </Card>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  addStaff: (client) => dispatch(addStaff(client)),
  getAllUsernames: () => dispatch(getAllUsernames())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserAdd)
