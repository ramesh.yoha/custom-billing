import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { format, isPast, subDays } from 'date-fns'
import InputMask from 'react-input-mask'
import {
  Paper,
  Typography,
  TextField,
  Icon,
  Tabs,
  Tab
} from '@material-ui/core'
import ReactHtmlParser from 'react-html-parser'
import {
  StatsComponent
} from 'components'
import {
  ClientList,
  UserList,
  Transaction,
  Activities
} from 'containers'
import {
  Card
} from '_helpers/components'
import { defaultColors } from '_helpers/colors'

import { isValidMAC } from '_helpers/validations'
import { checkMAC } from 'actions/clients'
import { updateCredits } from 'actions/transactions'
import { getStats } from 'actions/general'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const Top = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 1fr;
  grid-gap: 20px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`
const Bottom = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const UserStatsRow = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  align-items: center; 
`

const LeftStats = styled(Typography)`
  text-align: left;
  font-size: 14px !important;
  padding-left: 20px;
  color: black;
  letter-spacing: 1;
`

const RightStats = styled(Typography)`
  text-align: right;
  font-size: 14px !important;
  padding-right: 30px;
  color: black;
  letter-spacing: 1;
`

class Dashboard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      checkMAC: '00-1A-79',
      checkMACResults: '',
      daysBehind: 30
    }
  }

  handleFilter = (event, daysBehind) => {
    if (this.state.daysBehind !== daysBehind) {
      this.setState({ daysBehind }, () => {
        const from = subDays(new Date(), this.state.daysBehind)
        const to = new Date()
        this.props.getStats({ from, to })
      })
    }
  }

  checkMac = () => {
    if (isValidMAC(this.state.checkMAC)) {
      this.props.checkMAC(this.state.checkMAC)
        .then(response => {
          if (response.type === 'CHECK_MAC_SUCCESS') {
            this.setState({ checkMACResults: response.payload.data })
          } else {
            this.setState({ checkMACResults: { status: 'Something went wrong while checking MAC availability' } })
          }
        })
    } else {
      if (!this.state.checkMAC.includes('_')) {
        this.setState({ checkMACResults: { status: 'Invalid MAC ID' } })
      } else {
        this.setState({ checkMACResults: '' })
      }
    }
  }

  incrementClientCredit = (client) => {
    this.props.updateCredits({
      credits: 1,
      description: 'Added 1 credit',
      transactionTo: client.stb_mac
    }, client.accountBalance)
      .then(() => this.props.sync())
  }

  render () {
    const {
      authUserType,
      authUsername,
      UserAnnouncements,
      admins,
      superResellers,
      resellers,
      clients,
      stats,
      colors = defaultColors,
      mobileView
    } = this.props
    const { checkMACResults } = this.state

    return (
      <Wrapper>
        <Top>
          <Card title='Announcements' color={colors.dashboard_annoucements}>
            <div style={{ color: 'black', fontSize: '14px', letterSpacing: 1, fontFamily: 'Roboto, Helvetica, Arial, sans-serif', maxHeight: 250, overflowY: 'auto' }}>
              {ReactHtmlParser(UserAnnouncements)}
            </div>
          </Card>

          <Card title='Check MAC' color={colors.dashboard_checkMAC}>
            <InputMask mask='**:**:**:**:**:**'
              value={this.state.checkMAC}
              onChange={(e) => this.setState({ checkMAC: e.target.value.toUpperCase() }, this.checkMac)}
            >
              {(inputProps) => (
                <TextField
                  {...inputProps}
                  label='MAC Address'
                  fullWidth
                />
              )}
            </InputMask>
            {this.state.checkMACResults &&
              <>
                <div style={{ display: 'grid', gridTemplateColumns: '1fr', paddingTop: 10, alignItems: 'center', justifyItems: 'center' }}>
                  <Typography style={{ textAlign: 'left', color: 'black' }} variant='overline'>
                    {checkMACResults.status === 'Available.' ? <Icon fontSize='large'> thumb_up </Icon> : <Icon fontSize='large'> thumb_down </Icon>}
                  </Typography>
                  <Typography style={{ textAlign: 'left', color: 'black' }} variant='overline'> {checkMACResults.status} </Typography>
                  {checkMACResults.expiryDate &&
                    <Typography style={{ textAlign: 'left', padding: 10, color: 'black' }} variant='overline'> Expires on {format(Date.parse(checkMACResults.expiryDate), 'D MMMM YYYY')} </Typography>
                  }
                </div>
              </>
            }
          </Card>

          <Card title='User Stats' color={colors.dashboard_userStats}>
            {authUserType === 'superAdmin' &&
            <UserStatsRow>
              <LeftStats variant='overline'><Icon fontSize='inherit'>local_library</Icon> Admins</LeftStats>
              <RightStats variant='overline'> {admins.length} </RightStats>
            </UserStatsRow>
            }
            {['superAdmin', 'admin'].includes(authUserType) &&
            <UserStatsRow>
              <LeftStats variant='overline'><Icon fontSize='inherit'>group</Icon> Super Resellers </LeftStats>
              <RightStats variant='overline'> {superResellers.length} </RightStats>
            </UserStatsRow>
            }
            {['superAdmin', 'admin', 'superReseller'].includes(authUserType) &&
            <UserStatsRow>
              <LeftStats variant='overline'><Icon fontSize='inherit'>person</Icon> Resellers </LeftStats>
              <RightStats variant='overline'> {resellers.length} </RightStats>
            </UserStatsRow>
            }
            {['superAdmin', 'admin', 'superReseller', 'reseller'].includes(authUserType) &&
            <UserStatsRow>
              <LeftStats variant='overline'><Icon fontSize='inherit'>live_tv</Icon> Active Clients </LeftStats>
              <RightStats variant='overline'> {clients.filter(client => !isPast(client.tariff_expired_date)).length} </RightStats>
            </UserStatsRow>
            }
            {['superAdmin', 'admin', 'superReseller', 'reseller'].includes(authUserType) &&
            <UserStatsRow>
              <LeftStats variant='overline'><Icon fontSize='inherit'>tv_off</Icon> Expired Clients </LeftStats>
              <RightStats variant='overline'> {clients.filter(client => isPast(client.tariff_expired_date)).length} </RightStats>
            </UserStatsRow>
            }
          </Card>
        </Top>

        <Bottom>
          <Paper style={{ minHeight: 500 }}>
            <Tabs value={this.state.daysBehind} onChange={this.handleFilter}>
              <Tab label='Past 7 Days' value={7} icon={<Icon style={{ color: 'green' }}>today</Icon>} />
              <Tab label='Past 30 Days' value={30} icon={<Icon style={{ color: 'orange' }}>today</Icon>} />
              {!mobileView &&
                <Tab label='Past 6 Months' value={180} icon={<Icon style={{ color: 'blue' }}>today</Icon>} />
              }
              <Tab label='Past Year' value={365} icon={<Icon style={{ color: 'maroon' }}>today</Icon>} />
            </Tabs>
            {stats && stats.length > 0 &&
              <div style={{ height: 500 }}>
                <StatsComponent data={stats} userType={authUserType} />
              </div>
            }
          </Paper>

          <ClientList
            embedded
            history={this.props.history}
            title='Clients About To Expire'
            noPagination
            color={colors.dashboard_clients}
          />

          {authUserType !== 'reseller' &&
            <UserList
              embedded
              history={this.props.history}
              title='Users With Low Credits'
              orderBy='creditsAvailable'
              orderByDirection='desc'
              noPagination
              color={colors.dashboard_users}
            />
          }

          <Transaction
            username={authUsername}
            type='user'
            embedded
            title='Recent Transactions'
            limit={9999}
            color={colors.dashboard_transactions}
          />

          <Activities
            username={authUsername}
            type='user'
            embedded
            title='Recent Activities'
            limit={9999}
            color={colors.dashboard_activities}
          />

        </Bottom>
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  authUsername: state.auth.username,
  authCreditsAvailable: state.auth.creditsAvailable,
  authcreditsOwed: state.auth.creditsOwed,
  mobileView: state.general.mobileView,
  admins: state.users.admins,
  superResellers: state.users.superResellers,
  resellers: state.users.resellers,
  clients: state.users.clients,
  transactions: state.users.transactions,
  stats: state.users.stats,
  UserAnnouncements: state.config.UserAnnouncements,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  checkMAC: mac => dispatch(checkMAC(mac)),
  updateCredits: (transaction, currentAccountBalance) => dispatch(updateCredits(transaction, currentAccountBalance)),
  getStats: (from, to) => dispatch(getStats(from, to))
})

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
