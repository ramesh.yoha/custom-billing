import React, { Component } from 'react'
import { connect } from 'react-redux'
import InputMask from 'react-input-mask'
import { flatten } from 'lodash'
import {
  Typography,
  TextField,
  Button,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Icon
} from '@material-ui/core'
import {
  Card,
  Form
} from '_helpers/components'
import {
  isValidPhone,
  isValidMAC,
  validateAddNewClientCredits
} from '_helpers/validations'
import { defaultColors } from '_helpers/colors'

import { addClient, getAllLoginsAndMacs } from 'actions/clients'
import { updateCredits } from 'actions/transactions'

const getRandomInt = max => Math.floor(Math.random() * Math.floor(max))

class ClientAdd extends Component {
  constructor (props) {
    super(props)
    this.state = {
      newClient: {
        login: '',
        stb_mac: '00-1A-79',
        password: '',
        full_name: '',
        phone: '',
        tariff_plan: props.tariffPlans[0].id,
        status: 1
      },
      credits: 0,
      existingLoginsAndMacs: []
    }
  }

  componentDidMount = () => {
    // Get all existing logins and mac addresses
    this.props.getAllLoginsAndMacs()
      .then(responseJSON => {
        if (responseJSON.type === 'GET_ALL_LOGINS_AND_MACS_SUCCESS') {
          this.setState({ existingLoginsAndMacs: flatten(responseJSON.payload.data.map(({ clientMac, login }) => [clientMac, login])) })
        }
      })
  }

  handleTextChange = (field, value) => {
    this.setState({ newClient: { ...this.state.newClient, [field]: value } }, () => {
      if (field === 'full_name') {
        // Auto generate login if its empty only if full name has a space
        if (!this.state.newClient.login && this.state.newClient.full_name.includes(' ')) {
          let login = this.state.newClient.full_name.split(' ')[0]
          while (login.length < 8) {
            login = `${login}${getRandomInt(10)}`
          }
          this.setState({ newClient: { ...this.state.newClient, login: login.toLowerCase() } })
        }
      }
    })
  }

  addClient = (event) => {
    event.preventDefault()
    let { newClient } = this.state
    if (this.isMacEmpty(newClient.stb_mac)) delete newClient.stb_mac
    this.props.addClient(newClient)
      .then(clientAddResponse => {
        if (clientAddResponse.type === 'ADD_CLIENT_SUCCESS') {
          if (this.state.credits > 0) {
            this.props.updateCredits({
              credits: this.state.credits,
              description: 'Add initial credits for new client',
              transactionTo: this.state.newClient.login
            })
          }
          this.props.sync()
            .then(() => this.props.history.push(`/clients/${this.state.newClient.login}`))
        }
      })
  }

  isMacEmpty = mac => !mac || mac === '__:__:__:__:__:__' || mac === '00-1A-79' || mac === '00:1A:79:__:__:__'

  render () {
    const {
      authCreditsAvailable,
      tariffPlans,
      colors = defaultColors,
      loading
    } = this.props

    const loginEmpty = this.state.newClient.login === ''
    const loginExist = this.state.existingLoginsAndMacs.includes(this.state.newClient.login)
    const loginContainsDeleted = this.state.newClient.login.includes('deleted_')
    const loginLessThanEightCharacters = !loginEmpty && this.state.newClient.login.length < 8
    const stb_macExist = this.state.existingLoginsAndMacs.includes(this.state.newClient.stb_mac)
    const stb_macInvalid = !this.isMacEmpty(this.state.newClient.stb_mac) && !isValidMAC(this.state.newClient.stb_mac)
    const passwordEmpty = this.state.newClient.password === ''
    const fullNameEmpty = this.state.newClient.full_name === ''
    const phoneEmpty = this.state.newClient.phone === ''
    const phoneInvalid = !isValidPhone(this.state.newClient.phone)
    const creditsValidation = validateAddNewClientCredits(this.state.credits, authCreditsAvailable)

    const invalidForm = loginEmpty || loginExist || loginContainsDeleted || loginLessThanEightCharacters || stb_macExist || stb_macInvalid || passwordEmpty || fullNameEmpty || phoneEmpty || phoneInvalid || !creditsValidation.isValid

    return (
      <Card title='Create a new client' color={colors.client_add}>
        {authCreditsAvailable <= 0 && <Typography variant='h6' color='secondary'> You don't have enough credits to add a new client. </Typography>}
        <Form onSubmit={this.addClient}
          actions={[
            <Button
              key='cancel'
              variant='contained'
              color='secondary'
              onClick={() => this.props.history.push('/')}
            >
              Cancel <Icon style={{ marginLeft: 10 }}> cancel </Icon>
            </Button>,
            <Button
              key='submit'
              variant='contained'
              type='submit'
              color='primary'
              disabled={loading || invalidForm}
            >
              Submit <Icon style={{ marginLeft: 10 }}> save </Icon>
            </Button>
          ]}
        >
          <TextField
            label='Full Name'
            type='name'
            required
            value={this.state.newClient.full_name}
            onChange={(e) => this.handleTextChange('full_name', e.target.value.replace(/[^a-zA-Z ]/g, ""))}
            fullWidth
            autoFocus
            disabled={loading || authCreditsAvailable <= 0}
            error={fullNameEmpty}
            helperText={fullNameEmpty ? 'Required' : null}
          />
          <TextField
            label='Username'
            type='username'
            required
            value={this.state.newClient.login}
            onChange={(e) => this.handleTextChange('login', e.target.value.toLowerCase().trim().replace(/[^a-zA-Z]/g, ""))}
            fullWidth
            disabled={loading || authCreditsAvailable <= 0}
            error={loginEmpty || loginExist || loginContainsDeleted || loginLessThanEightCharacters}
            helperText={
              loginEmpty ? 'Required'
                : loginExist ? 'Login already exists'
                  : loginContainsDeleted ? 'Cannot have a username containing with deleted_'
                    : loginLessThanEightCharacters ? 'Minimum 8 characters needed for username'
                      : null
            }
          />
          <TextField
            label='Password'
            type='password'
            required
            value={this.state.newClient.password}
            onChange={(e) => this.handleTextChange('password', e.target.value)}
            fullWidth
            disabled={loading}
            error={passwordEmpty}
            helperText={passwordEmpty ? 'Required' : null}
          />
          <InputMask mask='**:**:**:**:**:**'
            value={this.state.newClient.stb_mac}
            onChange={(e) => this.handleTextChange('stb_mac', e.target.value.toUpperCase())}
          >
            {(inputProps) => (
              <TextField
                {...inputProps}
                label='MAC Address'
                fullWidth
                error={stb_macExist || stb_macInvalid}
                helperText={
                  stb_macInvalid ? 'Invalid MAC Given'
                    : stb_macExist ? 'Mac address already exists'
                      : null
                }
              />
            )}
          </InputMask>
          <InputMask mask='999-999-9999'
            value={this.state.newClient.phone}
            onChange={(e) => this.handleTextChange('phone', e.target.value)}
            disabled={loading || authCreditsAvailable <= 0}
          >
            {(inputProps) => (
              <TextField
                {...inputProps}
                label='Phone'
                fullWidth
                required
                error={phoneEmpty || phoneInvalid}
                helperText={
                  phoneEmpty ? 'Required'
                    : phoneInvalid ? 'Invalid Phone'
                      : null
                }
              />
            )}
          </InputMask>
          <FormControl>
            <InputLabel htmlFor='tariff_plan'>Tariff Plan</InputLabel>
            <Select
              label='Tarriff Plan'
              value={this.state.newClient ? this.state.newClient.tariff_plan : tariffPlans[0].id}
              onChange={(e) => this.handleTextChange('tariff_plan', e.target.value)}
              inputProps={{ id: 'tariff' }}
              disabled={loading || authCreditsAvailable <= 0}
            >
              {
                tariffPlans.map(plan => (
                  <MenuItem key={plan.id} value={plan.id}> {plan.name} </MenuItem>
                ))
              }
            </Select>
          </FormControl>
          <TextField
            label='Add Initial Credits'
            value={this.state.credits}
            onChange={(e) => this.setState({ credits: e.target.value ? parseInt(e.target.value) : '' })}
            type='number'
            fullWidth
            inputProps={{ min: 0, max: 12 }}
            disabled={loading || authCreditsAvailable <= 0}
            error={!creditsValidation.isValid}
            helperText={creditsValidation.errorMessage}
          />
        </Form>

      </Card>
    )
  }
}

const mapStateToProps = state => ({
  tariffPlans: state.config.tariffPlans,
  authCreditsAvailable: state.auth.creditsAvailable,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  addClient: (client) => dispatch(addClient(client)),
  getAllLoginsAndMacs: () => dispatch(getAllLoginsAndMacs()),
  updateCredits: (transaction) => dispatch(updateCredits(transaction))
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientAdd)
