import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  TextField,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon
} from '@material-ui/icons'

import { getAllLogs, readLog } from 'actions/users'

class SuperAdminLogs extends Component {
  constructor (props) {
    super(props)
    this.state = {
      allLogs: [],
      expanded: 'null',
      selectedLog: ''
    }
  }

  componentDidMount = () => {
    if (this.props.authUserType !== 'superAdmin') this.props.history.push('/')
    else {
      this.props.getAllLogs()
        .then(({ type, payload }) => {
          if (type === 'GET_ALL_LOGS_SUCCESS') {
            this.setState({ allLogs: payload.data })
          }
        })
    }
  }

  handleChange = panel => (event, expanded) => {
    const expandedNew = expanded ? panel : false
    if (expandedNew) {
      this.props.readLog(panel)
        .then(({ type, payload }) => {
          if (type === 'READ_LOG_SUCCESS') {
            this.setState({ expanded: expandedNew, selectedLog: payload.data })
          }
        })
    } else {
      this.setState({ expanded: expandedNew })
    }
  }

  handleSearch = (e) => {
    const search = e.target.value
    if (search) {
      const filteredLogs = this.state.allLogs.filter(filename => filename.toLowerCase().includes(search.toLowerCase()))
      this.setState({ allLogs: filteredLogs })
    } else {
      this.props.getAllLogs()
        .then(({ type, payload }) => {
          if (type === 'GET_ALL_LOGS_SUCCESS') {
            this.setState({ allLogs: payload.data })
          }
        })
    }
  }

  render () {
    const { expanded, allLogs, selectedLog } = this.state
    const { mobileView } = this.props

    return (
      <div style={{ display: 'grid', gridGap: '10px' }}>
        <TextField label='Search' onChange={this.handleSearch} margin='normal' />
        {allLogs.map(filename =>
          <ExpansionPanel key={filename} expanded={expanded === filename} onChange={this.handleChange(filename)}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} style={{ background: 'linear-gradient(60deg, #66bb6a, #43a047)' }}>
              <Typography>{filename}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style={{ background: '#F1F3F6' }}>
              <pre style={mobileView ? { overflowX: 'scroll', width: '75vw' } : {}}>
                {selectedLog}
              </pre>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  mobileView: state.general.mobileView
})

const mapDispatchToProps = dispatch => ({
  getAllLogs: () => dispatch(getAllLogs()),
  readLog: filename => dispatch(readLog(filename))
})

export default connect(mapStateToProps, mapDispatchToProps)(SuperAdminLogs)
