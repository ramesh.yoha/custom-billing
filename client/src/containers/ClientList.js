import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isPast } from 'date-fns'
import {
  Tabs,
  Tab,
  Icon
} from '@material-ui/core'
import {
  Table,
  Confirmation
} from 'components'

import { updateCredits } from 'actions/transactions'
import { reactivateClient } from 'actions/clients'

const rows = [
  { field: 'login', label: 'Login', type: 'string', link: true },
  { field: 'stb_mac', label: 'MAC Address', type: 'string' },
  { field: 'full_name', label: 'Full Name', type: 'string' },
  { field: 'phone', label: 'Telephone', type: 'string' },
  { field: 'accountBalance', label: 'Credits Available', type: 'integer' },
  { field: 'tariff_expired_date', label: 'Tariff Expiry', type: 'date' },
  { field: 'parentUsername', label: 'Reseller', type: 'string' },
  { field: 'now_playing_content', label: 'Now Playing', type: 'string' }
]

const getTableData = (clients, filter) => {
  let displayData = []
  for (let client of clients) {
    let canPush = false
    switch (filter) {
      case 'active':
        canPush = !isPast(client.tariff_expired_date)
        break
      case 'expired':
        canPush = isPast(client.tariff_expired_date)
        break
      case 'blocked':
        canPush = client.status === 0
        break
      case 'reactivate':
        canPush = client.accountBalance > 0 && client.status === 0
        break
      default:
        break
    }
    if (canPush) {
      let clientData = {}
      for (let row of rows) {
        clientData[row.field] = client[row.field]
      }
      displayData.push({ ...clientData })
    }
  }
  return displayData
}

class ClientList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      confirmation: false,
      plusOneClient: null,
      filter: 'active',
      showReactivateFilter: false
    }
  }

  componentDidMount = () => {
    for (let client of this.props.clients) {
      if (this.props.authUserType === 'reseller' && !this.state.showReactivateFilter && client.accountBalance > 0 && client.status === 0) {
        this.setState({ showReactivateFilter: true })
        break
      }
    }
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (this.state.showReactivateFilter) {
      for (let client of this.props.clients) {
        if (this.props.authUserType === 'reseller' && client.accountBalance > 0 && client.status === 0) {
          return
        }
      }
      this.setState({ showReactivateFilter: false })
    }
  }

  confirmationProceed = () => this.setState({ confirmation: false })
  confirmationCancel = () => this.setState({ confirmation: false })

  incrementClientCredit = (client) => {
    this.confirmationProceed = () => {
      this.setState({ confirmation: false }, () => {
        this.props.updateCredits({
          credits: 1,
          description: 'Added 1 credit',
          transactionTo: client.login
        }, client.accountBalance)
          .then(() => this.props.sync())
      })
    }
    this.setState({ confirmation: true, plusOneClient: client.login })
  }

  render () {
    const { filter, confirmation, plusOneClient, showReactivateFilter } = this.state
    const {
      title,
      clients,
      authUserType,
      mobileView,
      authCreditsAvailable,
      authcreditsOwed,
      embedded,
      noPagination = false,
      color
    } = this.props
    const canShowReactivate = authUserType === 'reseller' && showReactivateFilter

    return (
      <>
        {!embedded &&
          <Tabs value={filter} onChange={(e, filter) => this.setState({ filter })} >
            <Tab label='Tariff Active' value='active' icon={<Icon style={{ color: 'green' }}>live_tv</Icon>} />
            <Tab label='Tariff Expired' value='expired' icon={<Icon style={{ color: 'red' }}>tv_off</Icon>} />
            <Tab label='Account Blocked' value='blocked' icon={<Icon style={{ color: 'black' }}>thumb_down</Icon>} />
            <Tab label='To Be Reactivated' value='reactivate' style={{ display: canShowReactivate ? '' : 'none' }} icon={<Icon style={{ color: 'blue' }}>settings_input_hdmi</Icon>} />
          </Tabs>
        }

        <Table
          title={title || 'Clients'}
          rows={rows}
          data={getTableData(clients, filter)}
          orderBy='tariff_expired_date'
          orderByDirection='desc'
          mobileView={mobileView}
          gotoLink={(client) => this.props.history.push(`/clients/${client.login}`)}
          addNew={() => this.props.history.push('/clients/new')}
          incrementClientCredit={authUserType === 'reseller' ? (client) => this.incrementClientCredit(client) : false}
          reactivateClient={filter === 'reactivate' ? (client) => this.props.reactivateClient(client.login) : false}
          authCreditsAvailable={authCreditsAvailable}
          authcreditsOwed={authcreditsOwed}
          canAdd={!embedded && authUserType === 'reseller'}
          canDownload={authUserType === 'superAdmin'}
          tableHeight={embedded ? '100%' : mobileView ? '75vh' : '85vh'}
          limit={embedded ? 5 : 99999}
          noPagination={noPagination}
          backgroundColor={embedded ? color : ''}
          headingColor={embedded ? 'white' : ''}
        />
        <Confirmation
          open={confirmation}
          message={`Are you sure you want to add +1 credits to ${plusOneClient}`}
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
        />
      </>
    )
  }
}

const mapStateToProps = state => ({
  authUserType: state.auth.userType,
  clients: state.users.clients,
  mobileView: state.general.mobileView,
  authCreditsAvailable: state.auth.creditsAvailable,
  authcreditsOwed: state.auth.creditsOwed
})

const mapDispatchToProps = dispatch => ({
  updateCredits: (transaction, currentAccountBalance) => dispatch(updateCredits(transaction, currentAccountBalance)),
  reactivateClient: mac => dispatch(reactivateClient(mac))
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientList)
