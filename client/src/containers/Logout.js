import React, { Component } from 'react'
import { connect } from 'react-redux'
import { logout } from 'actions/auth'

class Logout extends Component {
  componentDidMount = () => this.logout()

  logout = () => {
    this.props.logout()
    this.props.history.push('/')
  }

  render () {
    return <div>Logging out...</div>
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(Logout)
