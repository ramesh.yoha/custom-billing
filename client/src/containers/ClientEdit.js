import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEqual, startCase } from 'lodash'
import { format, isPast, subDays, isValid } from 'date-fns'
import styled from 'styled-components'
import InputMask from 'react-input-mask'
import {
  Typography,
  TextField,
  Button,
  Switch,
  Select,
  MenuItem,
  FormControl,
  FormControlLabel,
  Checkbox,
  Radio,
  RadioGroup,
  Tooltip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Icon
} from '@material-ui/core'
import {
  Confirmation,
  Loading
} from 'components'
import {
  Transaction,
  Activities
} from 'containers'
import {
  Card,
  Form
} from '_helpers/components'
import {
  isValidPhone,
  isValidMAC,
  validateClientCreditsTransfer
} from '_helpers/validations'
import { defaultColors } from '_helpers/colors'

import { getUsers } from 'actions/users'
import {
  updateClient,
  deleteClient,
  getSubscriptions,
  addSubscription,
  removeSubscription,
  checkMAC
} from 'actions/clients'
import { updateCredits } from 'actions/transactions'
import { getTariffPlans } from 'actions/general'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const TopRowWrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 2fr 3fr;
  grid-gap: 20px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const CreditsDetails = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  align-items: center;
`

const CreditsDetailsRow = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;
  grid-gap: 20px;
  align-items: center;
  justify-items: center;
`

const TariffHeader = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
`

const TariffPackagesHeader = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 1fr;
  grid-gap: 20px;
  padding: 20px 0;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr 1fr 1fr;
    grid-gap: 5px;
  }
  font-weight: bold;
`

const TariffPackages = styled.div`
  display: grid;
  grid-gap: 20px;
  overflow-y: auto;
`

const TariffPackageRow = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 1fr;
  grid-gap: 0px;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
  height: fit-content;
`

const STBDetailsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr 1fr;
  }
  grid-gap: 20px;
`

const STBDetails = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  @media only screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    grid-template-rows: 20px;
    div {
      min-height: 20px;
    }
  }
  grid-gap: 20px;
  justify-items: center;
  align-items: center;

`
class ClientEdit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      client: null,
      editingClient: null,
      newPassword: '',
      confirmationMessage: '',
      confirmation: false,
      credits: {
        value: 1,
        action: 'add'
      },
      subscriptions: [],
      msg: '',
      changeMAC: false,
      newMAC: '00:1A:79',
      checkMACStatus: ''
    }
  }

  componentDidMount = () => {
    const login = this.props.match.params.id
    const client = this.findClientFromLogin(login)
    if (client) {
      this.props.getSubscriptions(client.account_number)
        .then(({ payload, type }) => {
          if (type === 'GET_SUBSCRIPTIONS_SUCCESS') {
            this.setState({
              client,
              editingClient: { ...client },
              subscriptions: payload.data[0].subscribed
            })
          }
        })
    } else {
      this.setState({ client: false })
    }
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    const login = this.props.match.params.id
    const client = this.findClientFromLogin(login)
    if (client) {
      if (!isEqual(prevState.client, client)) {
        this.setState({ client, editingClient: { ...client } })
      }
    }
  }

  findClientFromLogin = login => {
    return this.props.clients.find(client => client.login === login)
  }

  handleTextChange = (field, value) => {
    this.setState({ editingClient: { ...this.state.editingClient, [field]: value } })
  }

  updateClient = (event) => {
    event.preventDefault()
    const { login } = this.state.editingClient
    const { full_name, phone, comment, status } = this.state.editingClient
    if (this.state.newPassword) {
      this.confirmationProceed = () => {
        this.setState({ confirmation: false }, () => {
          this.props.updateClient(login, { full_name, phone, comment, status, password: this.state.newPassword })
            .then(() => this.props.sync())
            .then(() => this.setState({ newPassword: '' }))
        })
      }
      this.setState({ confirmation: true, confirmationMessage: 'You are changing the client\'s password. Are you sure you want to continue ?' })
    } else {
      this.props.updateClient(login, { full_name, phone, comment, status })
        .then(() => this.props.sync())
    }
  }

  confirmationProceed = () => this.setState({ confirmation: false })
  confirmationCancel = () => this.setState({ confirmation: false })

  deleteClient = (event) => {
    event.preventDefault()
    this.confirmationProceed = () => {
      this.setState({ confirmation: false }, () => {
        this.props.deleteClient(this.state.editingClient.login)
          .then(() => this.props.history.push('/clients'))
          .then(() => this.props.sync())
      })
    }
    this.setState({ confirmation: true })
  }

  updateCredits = (event) => {
    event.preventDefault()
    this.props.updateCredits({
      credits: this.state.credits.action === 'add' ? this.state.credits.value : this.state.credits.value * -1,
      description: `${startCase(this.state.credits.action)}ed ${this.state.credits.value} credits`,
      transactionTo: this.state.editingClient.login
    }, this.state.client.accountBalance)
      .then(() => this.props.sync())
  }

  setSubscription = (subscribed, checked) => {
    if (checked) {
      this.props.addSubscription(this.state.editingClient.login, subscribed)
        .then(() => this.setState({ subscriptions: [...this.state.subscriptions, subscribed] }))
    } else {
      this.props.removeSubscription(this.state.editingClient.login, subscribed)
        .then(() => this.setState({ subscriptions: this.state.subscriptions.filter(sub => sub !== subscribed) }))
    }
  }

  changeMAC = (event) => {
    event.preventDefault()
    this.setState({ checkMACStatus: '' }, () => {
      this.props.checkMAC(this.state.newMAC)
        .then(response => {
          if (response.payload.data.status === 'Available.') {
            this.props.updateClient(this.state.client.login, { stb_mac: this.state.newMAC })
              .then(() => this.props.sync())
              .then(() => this.props.history.push(`/clients/${this.state.client.login}`))
          } else {
            this.setState({ checkMACStatus: `Given MAC Address is already in use and will expire on ${format(Date.parse(response.payload.data.expiryDate), 'D MMMM YYYY')}` })
          }
        })
    })
  }

  render () {
    if (!this.state.client) {
      if (this.state.client === false) {
        return (
          <div style={{ height: '100vh' }}>
            <Typography variant='h4'>
                Client with MAC {this.props.match.params.id} was not found
            </Typography>
          </div>
        )
      } else {
        return (
          <div style={{ height: '100vh' }}>
            <Typography variant='h4'>
                Client is loading...
            </Typography>
            <Loading forced />
          </div>
        )
      }
    }

    const {
      authCreditsAvailable,
      authUserType,
      tariffPlans,
      loading,
      colors = defaultColors
    } = this.props

    const fullNameEmpty = this.state.editingClient.fullName === ''
    const phoneInvalid = this.state.editingClient.phone === '' || !isValidPhone(this.state.editingClient.phone)
    const noChanges = isEqual(this.state.editingClient, this.state.client)
    const creditsValidation = validateClientCreditsTransfer(this.state.credits.value, this.state.client.accountBalance, this.state.credits.action, authCreditsAvailable)

    const invalidForm = fullNameEmpty || phoneInvalid || noChanges

    const isClientDeletable = !isValid(new Date(this.state.client.tariff_expired_date)) || isPast(subDays(new Date(this.state.client.tariff_expired_date), 1))

    const tariifPackage = tariffPlans.find(plan => parseInt(plan.external_id) === parseInt(this.state.client.tariff_plan))

    return (
      <Wrapper>
        <TopRowWrapper>
          <Card title={`Edit Client: ${this.state.client.login}`} color={colors.client_profileUpdate}>
            {this.state.editingClient &&
              <Form
                onSubmit={this.updateClient}
                actions={[
                  <Button
                    key='submit'
                    variant='contained'
                    type='submit'
                    color='primary'
                    disabled={(!this.state.newPassword) && (loading || invalidForm || noChanges)}
                  >
                    Update <Icon style={{ marginLeft: 5 }}> save </Icon>
                  </Button>,
                  <Button
                    key='delete'
                    variant='contained'
                    type='submit'
                    color='secondary'
                    disabled={loading}
                    onClick={this.deleteClient}
                  >
                    Delete <Icon style={{ marginLeft: 5 }}> delete </Icon>
                  </Button>
                ]}
              >
                <TextField
                  label='Full Name'
                  type='name'
                  required
                  value={this.state.editingClient.full_name}
                  onChange={(e) => this.handleTextChange('full_name', e.target.value)}
                  fullWidth
                  disabled={loading}
                  error={fullNameEmpty}
                  helperText={fullNameEmpty ? 'Required' : null}
                />
                <InputMask mask='999-999-9999'
                  value={this.state.editingClient.phone}
                  onChange={(e) => this.handleTextChange('phone', e.target.value)}
                >
                  {(inputProps) => (
                    <TextField
                      {...inputProps}
                      label='Phone'
                      required
                      fullWidth
                      disabled={loading}
                      error={phoneInvalid}
                      helperText={phoneInvalid ? 'Invalid Phone' : null}
                    />
                  )}
                </InputMask>
                <TextField
                  label='Comments'
                  type='comment'
                  value={this.state.editingClient.comment ? this.state.editingClient.comment : ''}
                  onChange={(e) => this.handleTextChange('comment', e.target.value)}
                  fullWidth
                  multiline
                  rows={3}
                  rowsMax='3'
                  disabled={loading}
                />
                <TextField
                  label='Change Password'
                  type='password'
                  placeholder='Leave empty to keep the current password unchanged'
                  value={this.state.newPassword}
                  onChange={(e) => this.setState({ newPassword: e.target.value })}
                  fullWidth
                  disabled={loading}
                />
                <FormControlLabel
                  label={`Account Status (${this.state.editingClient.status === 1 ? 'Active' : 'Inactive'})`}
                  control={
                    <Switch
                      checked={this.state.editingClient.status === 1}
                      onChange={(e) => this.handleTextChange('status', e.target.checked ? 1 : 0)}
                      value={this.state.editingClient.status}
                      color='primary'
                      disabled={loading || authUserType === 'reseller'}
                      style={{ color: this.state.editingClient.status === 1 ? 'green' : 'red' }}
                    />
                  }
                />
              </Form>
            }
          </Card>

          <Card title={'Credits'} color={colors.client_credits}>
            {this.state.client && authUserType === 'reseller' &&
              <>
                <Form
                  onSubmit={this.updateCredits}
                  actions={[
                    <Button
                      key='submit'
                      variant='contained'
                      type='submit'
                      color='primary'
                      disabled={loading || !creditsValidation.isValid}
                      onClick={this.updateCredits}
                    >
                      Submit <Icon style={{ marginLeft: 5 }}> save </Icon>
                    </Button>
                  ]}
                >
                  <TextField
                    label='Select Credits'
                    type='number'
                    inputProps={{ min: 1 }}
                    value={this.state.credits.value}
                    onChange={(e) => this.setState({ credits: { ...this.state.credits, value: e.target.value } })}
                    fullWidth
                    disabled={loading}
                    error={!creditsValidation.isValid}
                    helperText={creditsValidation.errorMessage}
                  />
                  <FormControl component='fieldset'>
                    <RadioGroup
                      aria-label='Gender'
                      name='gender1'
                      value={this.state.credits.action}
                      style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center' }}
                      onChange={(e) => this.setState({ credits: { ...this.state.credits, action: e.target.value } })}
                    >
                      <FormControlLabel value='add' control={<Radio />} label='Add' />
                      <FormControlLabel value='recover' control={<Radio />} label='Recover' />
                    </RadioGroup>
                  </FormControl>
                </Form>
                <br />
              </>
            }
            {this.state.client &&
              <CreditsDetails>
                <CreditsDetailsRow>
                  <div> <Typography style={{ fontSize: 15 }} variant='body2'>Credits Available </Typography></div>
                  <div style={{ fontSize: 50 }}> {this.state.client.accountBalance} </div>
                </CreditsDetailsRow>
                <CreditsDetailsRow>
                  <div> <Typography style={{ fontSize: 15 }} variant='body2'>Tariff Expires on </Typography></div>
                  <div style={{ fontSize: 30 }}> {format(Date.parse(this.state.client.tariff_expired_date), 'D MMMM YYYY')} </div>
                </CreditsDetailsRow>
              </CreditsDetails>
            }
          </Card>

          <Card title='Edit Tariff Plan' color={colors.client_tariff}>
            <div style={{ overflowY: 'hidden' }}>
              <TariffHeader>
                <Select
                  label='Tarriff Plan'
                  value={this.state.client.tariff_plan}
                  onChange={(e) => this.props.updateClient(this.state.client.login, { tariff_plan: e.target.value })}
                  inputProps={{ id: 'tariff_plan' }}
                >
                  {tariffPlans.map(plan => <MenuItem key={plan.external_id} value={parseInt(plan.external_id)}> {plan.name} </MenuItem>)
                  }
                </Select>
              </TariffHeader>
              <TariffPackagesHeader>
                <div>Name</div><div style={{ justifySelf: 'center' }}>Optional</div><div style={{ justifySelf: 'center' }}>Subscribed</div>
              </TariffPackagesHeader>
              <TariffPackages style={{ height: 300, overflowY: 'scroll' }}>
                {
                  tariifPackage && tariifPackage.packages.map(tariffPackage => (
                    <TariffPackageRow key={tariffPackage.name}>
                      <div>{tariffPackage.name}</div>
                      <div style={{ justifySelf: 'center' }}>{tariffPackage.optional === '1' ? 'Yes' : 'No'}</div>
                      <Checkbox
                        checked={tariffPackage.optional === '1' ? this.state.subscriptions.includes(tariffPackage.external_id) : true}
                        disabled={tariffPackage.optional !== '1'}
                        style={{ padding: 10, justifySelf: 'center', height: 15 }}
                        onChange={(e) => this.setSubscription(tariffPackage.external_id, e.target.checked)}
                      />
                    </TariffPackageRow>
                  ))
                }
              </TariffPackages>
            </div>
          </Card>
        </TopRowWrapper>

        <Card title='STB Details' color={colors.client_stb} style={{ height: 'fit-content' }}>
          <STBDetailsWrapper>
            <STBDetails>
              <div><Typography variant='subtitle2' style={{ alignSelf: 'center' }}>MAC Address</Typography></div>
              <div><Typography variant='subtitle2'>Receiver Status</Typography></div>
              <div><Typography variant='subtitle2'>IP</Typography></div>
              <div><Typography variant='subtitle2'>STB Type</Typography></div>
              <div><Typography variant='subtitle2'>STB Serial</Typography></div>
              <div><Typography variant='subtitle2'>Last Active</Typography></div>
            </STBDetails>
            <STBDetails>
              <div><Typography variant='body2'>
                <Tooltip title='Change MAC'>
                  <Button aria-label='Change MAC' color='primary' variant='contained' style={{ padding: 9 }} onClick={() => this.setState({ changeMAC: true })}>
                    {this.state.client.stb_mac} <Icon>edit</Icon>
                  </Button>
                </Tooltip>
              </Typography></div>
              <div><Typography variant='body2'>{this.state.client.online === 1 ? 'Online' : 'Offline'}</Typography></div>
              <div><Typography variant='body2'>{this.state.client.ip}</Typography></div>
              <div><Typography variant='body2'>{this.state.client.stb_type}</Typography></div>
              <div><Typography variant='body2'>{this.state.client.serial_number}</Typography></div>
              <div><Typography variant='body2'>{format(Date.parse(this.state.client.last_active), 'D MMMM YYYY @ HH:mm:ss')}</Typography></div>
            </STBDetails>
          </STBDetailsWrapper>
        </Card>

        <Transaction
          username={this.state.client.login}
          type='client'
          embedded
          title='Recent Transactions'
          limit={9999}
          color={colors.client_transactions}
        />

        <Activities
          username={this.state.client.login}
          type='client'
          embedded
          title='Recent Activities'
          limit={9999}
          color={colors.client_activities}
        />

        <Confirmation
          open={this.state.confirmation}
          message={
            this.state.newPassword ? this.state.confirmationMessage
              : this.state.client.accountBalance > 0 ? 'Client has active credits. Please recover them before deleting.'
                : !isClientDeletable ? `Client still has an active tariff plan which expires on ${format(Date.parse(this.state.client.tariff_expired_date), 'D MMMM YYYY')}`
                  : 'Are you sure you want to delete this client ?'
          }
          confirmationProceed={this.confirmationProceed}
          confirmationCancel={this.confirmationCancel}
          disabled={!this.state.newPassword && (this.state.client.accountBalance > 0 || !isClientDeletable)}
        />

        <Dialog
          open={this.state.changeMAC}
          aria-labelledby='form-dialog-title'
          fullWidth
        >
          <form onSubmit={this.changeMAC}>
            <DialogTitle id='form-dialog-title'>
              Changing MAC Address for {this.state.client.login}
            </DialogTitle>
            <DialogContent>
              <Typography variant='body1' style={{ paddingBottom: 20, textAlign: 'center' }}> Current MAC Address: {this.state.client.stb_mac} </Typography>
              <InputMask mask='**:**:**:**:**:**'
                value={this.state.newMAC}
                onChange={(e) => this.setState({ newMAC: e.target.value.toUpperCase() })}
              >
                {(inputProps) => (
                  <TextField
                    {...inputProps}
                    autoFocus
                    label='New MAC Address'
                    disabled={loading}
                    fullWidth
                    error={Boolean(this.state.newMAC) && !isValidMAC(this.state.newMAC)}
                    helperText={this.state.newMAC && !isValidMAC(this.state.newMAC) ? 'Invalid MAC Given' : null}
                  />
                )}
              </InputMask>
              <Typography variant='subtitle1' color='secondary' style={{ paddingTop: 20, textAlign: 'center' }}> {this.state.checkMACStatus} </Typography>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => this.setState({ changeMAC: false })} color='secondary'>
                Cancel
              </Button>
              <Button variant='contained' type='submit' color='primary' disabled={loading || !this.state.newMAC || (Boolean(this.state.newMAC) && !isValidMAC(this.state.newMAC))} onClick={this.changeMAC}>
                Submit
              </Button>
            </DialogActions>
          </form>
          <Loading />
        </Dialog>

      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({
  clients: state.users.clients,
  authUserType: state.auth.userType,
  authCreditsAvailable: state.auth.creditsAvailable,
  tariffPlans: state.config.tariffPlans,
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateClient: (login, client) => dispatch(updateClient(login, client)),
  deleteClient: login => dispatch(deleteClient(login)),
  getUsers: () => dispatch(getUsers()),
  getTariffPlans: () => dispatch(getTariffPlans()),
  updateCredits: (transaction, currentAccountBalance) => dispatch(updateCredits(transaction, currentAccountBalance)),
  getSubscriptions: stb_mac => dispatch(getSubscriptions(stb_mac)),
  addSubscription: (stb_mac, subscribedID) => dispatch(addSubscription(stb_mac, subscribedID)),
  removeSubscription: (stb_mac, subscribedID) => dispatch(removeSubscription(stb_mac, subscribedID)),
  checkMAC: mac => dispatch(checkMAC(mac))
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientEdit)
