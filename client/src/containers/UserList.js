import React, { Component } from 'react'
import { connect } from 'react-redux'
import { startCase } from 'lodash'
import {
  Tabs,
  Tab,
  Icon
} from '@material-ui/core'
import { Table } from 'components'

let rows = [
  { field: 'username', label: 'Username', type: 'string' },
  { field: 'email', label: 'Email', type: 'string' },
  { field: 'phoneNo', label: 'Telephone', type: 'string' },
  { field: 'parentUsername', label: 'Parent', type: 'string' },
  { field: 'creditsAvailable', label: 'Credits Available', type: 'integer' },
  { field: 'createdAt', label: 'Created At', type: 'date' }
]

const getTableData = (users, filter) => {
  let displayData = []
  for (let user of users) {
    let canPush = false
    switch (filter) {
      case 'active':
        canPush = user.accountStatus
        break
      case 'inactive':
        canPush = !user.accountStatus
        break
      default:
        break
    }
    if (canPush) {
      let userData = {}
      for (const row of rows) {
        userData[row.field] = user[row.field]
      }
      displayData.push({ ...userData })
    }
  }
  return displayData
}

const checkPermissionToAdd = (urlPath, authUserType) => {
  if (urlPath === 'admins') return authUserType === 'superAdmin'
  if (urlPath === 'superResellers') return authUserType === 'admin'
  if (urlPath === 'resellers') return authUserType === 'superReseller'
  return false
}
class UserList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      filter: 'active'
    }
  }

  render () {
    const {
      title,
      authUserType,
      orderBy,
      orderByDirection,
      mobileView,
      embedded,
      noPagination = false,
      color
    } = this.props
    const urlPath = embedded ? '' : this.props.match.url.substr(1)
    const users = embedded ? [...this.props.admins, ...this.props.superResellers, ...this.props.resellers] : this.props[urlPath]
    const { filter } = this.state

    return (
      <>
        {!embedded &&
          <Tabs value={filter} onChange={(e, filter) => this.setState({ filter })} >
            <Tab label='Active' value='active' icon={<Icon style={{ color: 'green' }}>thumb_up</Icon>} />
            <Tab label='Inactive' value='inactive' icon={<Icon style={{ color: 'red' }}>thumb_down</Icon>} />
          </Tabs>
        }

        <Table
          title={title || startCase(urlPath)}
          rows={rows}
          data={getTableData(users, filter)}
          orderBy={orderBy || 'username'}
          orderByDirection={orderByDirection || 'desc'}
          mobileView={mobileView}
          gotoLink={(user) => this.props.history.push(`/users/${user.username}`)}
          canAdd={!embedded && checkPermissionToAdd(urlPath, authUserType)}
          addNew={() => this.props.history.push('/users/new')}
          canDownload={authUserType === 'superAdmin'}
          tableHeight={embedded ? '100%' : mobileView ? '75vh' : '85vh'}
          limit={embedded ? 5 : 99999}
          noPagination={noPagination}
          backgroundColor={embedded ? color : ''}
          headingColor={embedded ? 'white' : ''}
        />
      </>
    )
  }
}

const mapStateToProps = state => ({
  admins: state.users.admins,
  superResellers: state.users.superResellers,
  resellers: state.users.resellers,
  mobileView: state.general.mobileView,
  authUserType: state.auth.userType
})

export default connect(mapStateToProps)(UserList)
