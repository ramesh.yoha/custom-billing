import React, { Component } from 'react'
import { connect } from 'react-redux'
import { format, isPast } from 'date-fns'
import styled from 'styled-components'
import InputMask from 'react-input-mask'
import {
  Typography,
  TextField,
  Icon,
  Button
} from '@material-ui/core'
import { Confirmation } from 'components'
import { Card } from '_helpers/components'
import { isValidMAC } from '_helpers/validations'
import { defaultColors } from '_helpers/colors'

import {
  updateClient,
  checkMAC
} from 'actions/clients'
import { logout } from 'actions/auth'

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 20px;
  justify-items: center;
  align:items: center;
`

class SpecialDeleter extends Component {
  constructor (props) {
    super(props)
    this.state = {
      checkMAC: '00-1A-79',
      checkMACResults: '',
      removeConfirmation: false
    }
  }

  removeConfirmationProceed = () => this.setState({ removeConfirmation: false })
  removeConfirmationCancel = () => this.setState({ removeConfirmation: false })

  removeMAC = (event) => {
    event.preventDefault()
    this.removeConfirmationProceed = () => {
      this.setState({ removeConfirmation: false }, () => {
        this.props.updateClient(this.state.checkMACResults.login, { stb_mac: this.state.checkMAC })
          .then(() => this.setState({ checkMAC: '00-1A-79', checkMACResults: '' }))
      })
    }
    this.setState({ removeConfirmation: true })
  }

  checkMac = () => {
    if (isValidMAC(this.state.checkMAC)) {
      this.props.checkMAC(this.state.checkMAC)
        .then(response => {
          if (response.type === 'CHECK_MAC_SUCCESS') {
            this.setState({ checkMACResults: response.payload.data })
          } else {
            this.setState({ checkMACResults: { status: 'Something went wrong while checking MAC availability' } })
          }
        })
    } else {
      if (!this.state.checkMAC.includes('_')) {
        this.setState({ checkMACResults: { status: 'Invalid MAC ID' } })
      } else {
        this.setState({ checkMACResults: '' })
      }
    }
  }

  render () {
    const { colors = defaultColors } = this.props

    const { checkMACResults } = this.state

    return (
      <Wrapper>
        <div style={{ minWidth: 400, paddingTop: '10vh' }}>
          <Card title='Check MAC' color={colors.dashboard_checkMAC} style={{ minHeight: 400 }}>
            <InputMask mask='**:**:**:**:**:**'
              value={this.state.checkMAC}
              onChange={(e) => this.setState({ checkMAC: e.target.value.toUpperCase() }, this.checkMac)}
            >
              {(inputProps) => (
                <TextField
                  {...inputProps}
                  label='MAC Address'
                  fullWidth
                />
              )}
            </InputMask>
            {this.state.checkMACResults &&
              <>
                <div style={{ display: 'grid', gridTemplateColumns: '1fr', paddingTop: 10, alignItems: 'center', justifyItems: 'center' }}>
                  <Typography style={{ textAlign: 'left', color: 'black' }} variant='overline'>
                    {checkMACResults.status === 'Available.' ? <Icon fontSize='large'> thumb_up </Icon> : <Icon fontSize='large'> thumb_down </Icon>}
                  </Typography>
                  <Typography style={{ textAlign: 'left', color: 'black' }} variant='overline'> {checkMACResults.status} </Typography>
                  {checkMACResults.expiryDate &&
                    <Typography style={{ textAlign: 'left', padding: 10, color: 'black' }} variant='overline'> Expiry {format(Date.parse(checkMACResults.expiryDate), 'D MMMM YYYY')} </Typography>
                  }
                  {checkMACResults.expiryDate && isPast(checkMACResults.expiryDate) &&
                    <Button
                      variant='contained'
                      color='secondary'
                      onClick={this.removeMAC}
                    >
                      Delete MAC from client: {checkMACResults.login}
                    </Button>
                  }
                </div>
              </>
            }
          </Card>
          <Button
            variant='contained'
            color='primary'
            onClick={this.props.logout}
            fullWidth
          >
            Logout
          </Button>
        </div>
        <Confirmation
          open={this.state.removeConfirmation}
          message={`Are you sure you want to remove MAC from this client: ${checkMACResults.login}`}
          confirmationProceed={this.removeConfirmationProceed}
          confirmationCancel={this.removeConfirmationCancel}
        />
      </Wrapper>

    )
  }
}

const mapStateToProps = state => ({
  colors: state.config.colors
})

const mapDispatchToProps = dispatch => ({
  updateClient: (login, client) => dispatch(updateClient(login, client)),
  checkMAC: mac => dispatch(checkMAC(mac)),
  logout: () => dispatch(logout())
})

export default connect(mapStateToProps, mapDispatchToProps)(SpecialDeleter)
