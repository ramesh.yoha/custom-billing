import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subDays } from 'date-fns'
import { Table } from 'components'

import { getActivities } from 'actions/general'

const rows = [
  { field: 'createdAt', label: 'Date', type: 'date' },
  { field: 'from', label: 'From', type: 'string' },
  { field: 'action', label: 'Action', type: 'string' },
  { field: 'to', label: 'To', type: 'string' },
  { field: 'description', label: 'Description', type: 'popup' }
]

const getTableData = (activities, username) => {
  let displayData = []
  for (let activity of activities) {
    let activityData = {}
    if (activity.from === username || activity.to === username) {
      for (let row of rows) {
        if (row.field === 'description') {
          activityData[row.field] = activity[row.field] ? JSON.stringify(activity[row.field], null, 4) : false
        } else {
          activityData[row.field] = activity[row.field]
        }
      }
      displayData.push({ ...activityData })
    }
  }
  return displayData
}

class Activities extends Component {
  constructor (props) {
    super(props)
    this.state = {
      daysBehind: 30
    }
  }

  handleFilter = (event, daysBehind) => {
    event.preventDefault()
    this.setState({ daysBehind }, () => {
      const from = subDays(new Date(), this.state.daysBehind)
      const to = new Date()
      this.props.getActivities({ from, to })
    })
  }

  render () {
    const { daysBehind } = this.state
    const {
      mobileView,
      title,
      activities,
      authUserType,
      embedded,
      color,
      limit,
      noPagination = false
    } = this.props

    const username = this.props.username || this.props.authUsername

    return (
      <Table
        title={title || 'Activities'}
        rows={rows}
        data={getTableData(activities, username)}
        orderBy='createdAt'
        orderByDirection='asc'
        mobileView={mobileView}
        viewOnly
        canDownload={authUserType === 'superAdmin'}
        tableHeight={embedded ? '300px' : mobileView ? '75vh' : '85vh'}
        limit={embedded ? limit || 5 : 99999}
        noPagination={noPagination}
        backgroundColor={embedded ? color : ''}
        headingColor={embedded ? 'white' : ''}
        handleFilter={this.handleFilter}
        daysBehind={daysBehind}
      />
    )
  }
}

const mapStateToProps = state => ({
  activities: state.users.activities,
  authUsername: state.auth.username,
  authUserType: state.auth.userType,
  mobileView: state.general.mobileView
})

const mapDispatchToProps = dispatch => ({
  getActivities: args => dispatch(getActivities(args))
})

export default connect(mapStateToProps, mapDispatchToProps)(Activities)
