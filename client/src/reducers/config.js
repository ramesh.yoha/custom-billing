import { defaultColors } from '_helpers/colors'

const initialState = {
  minimumTransferrableCredits: 25,
  UserAnnouncements: '',
  enableSendEventsFor: {},
  nonRefundableCredits: 0,
  tariffPlans: [],
  colors: defaultColors
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CONFIG_SUCCESS':
      if (action.meta.previousAction.special) {
        return state
      } else {
        return {
          ...state,
          minimumTransferrableCredits: action.payload.data.minimumTransferrableCredits,
          UserAnnouncements: action.payload.data.UserAnnouncements,
          enableSendEventsFor: action.payload.data.enableSendEventsFor,
          nonRefundableCredits: action.payload.data.nonRefundableCredits,
          colors: action.payload.data.colors
        }
      }
    case 'SYNC_SUCCESS':
      if (action.payload.data.config) {
        return {
          ...state,
          minimumTransferrableCredits: action.payload.data.config.minimumTransferrableCredits,
          UserAnnouncements: action.payload.data.config.UserAnnouncements,
          enableSendEventsFor: action.payload.data.config.enableSendEventsFor,
          nonRefundableCredits: action.payload.data.config.nonRefundableCredits,
          colors: action.payload.data.config.colors
        }
      } else {
        return state
      }
    case 'GET_TARIFF_PLANS_SUCCESS':
      return {
        ...state,
        loading: false,
        tariffPlans: action.payload.data
      }
    default:
      return state
  }
}
