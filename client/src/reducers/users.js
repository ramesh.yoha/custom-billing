import { uniqBy } from 'lodash'

const initialState = {
  admins: [],
  superResellers: [],
  resellers: [],
  staffs: [],
  clients: [],
  specialDeleters: [],
  transactions: [],
  activities: [],
  stats: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_USERS_SUCCESS':
      return {
        ...state,
        ...action.payload.data
      }
    case 'SYNC_SUCCESS':
      let stats = [...state.stats]
      stats.pop()
      return {
        ...state,
        admins: action.payload.data.admins,
        superResellers: action.payload.data.superResellers,
        resellers: action.payload.data.resellers,
        staffs: action.payload.data.staffs,
        clients: action.payload.data.clients,
        specialDeleters: action.payload.data.specialDeleters,
        transactions: uniqBy([...action.payload.data.transactions, ...state.transactions], '_id'),
        activities: uniqBy([...action.payload.data.activities, ...state.activities], '_id'),
        stats: [...stats, ...action.payload.data.stats]
      }
    case 'GET_STAFFS_SUCCESS':
      return {
        ...state,
        staffs: [...action.payload.data]
      }
    case 'GET_STATS_SUCCESS':
      return {
        ...state,
        stats: action.payload.data
      }
    case 'GET_TRANSACTIONS_SUCCESS':
      return {
        ...state,
        transactions: action.payload.data
      }
    case 'GET_ACTIVITIES_SUCCESS':
      return {
        ...state,
        activities: action.payload.data
      }
    case 'LOGOUT':
      return {
        ...initialState
      }
    default:
      return state
  }
}
