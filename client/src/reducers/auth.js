const initialState = {
  _id: '',
  username: '',
  token: '',
  lastLogin: {},
  loginActivities: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        ...action.payload.data.user,
        token: action.payload.data.token,
        lastLogin: action.payload.data.lastLogin
      }
    case 'LOGIN_FAILED':
      return {
        ...state
      }
    case 'LOGOUT':
      return {
        ...initialState
      }
    case 'SYNC_SUCCESS':
      return {
        ...state,
        ...action.payload.data.auth
      }
    case 'GET_LOGIN_ACTIVITIES_SUCCESS':
      return {
        ...state,
        loginActivities: action.payload.data.loginDetails
      }
    case (action.type.match(/_FAILED$/) || {}).input:
      return action.error.response.status === 401 ? { ...initialState } : state
    default:
      return state
  }
}
