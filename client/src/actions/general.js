import { subDays } from 'date-fns'

export const toggleMobileSideBar = (open = false) => ({
  type: 'TOGGLE_MOBILE_MENU',
  payload: open
})

export const setMobileView = (mobileView) => ({
  type: 'SET_MOBILE_VIEW',
  payload: mobileView
})

export const clearNotification = () => ({
  type: 'CLEAR_NOTIFICATION'
})

export const getTariffPlans = () => ({
  types: ['LOADING', 'GET_TARIFF_PLANS_SUCCESS', 'GET_TARIFF_PLANS_FAILED'],
  payload: {
    request: {
      url: '/ministra/tariffs',
      method: 'GET'
    }
  },
  success: false,
  failure: 'Something went wrong while retrieving tariff plans!'
})

export const showLoading = () => ({
  type: 'LOADING'
})

export const hideLoading = () => ({
  type: 'HIDE_LOADING'
})

export const getActivities = ({ from = subDays(new Date(), 30), to = new Date() } = {}) => ({
  types: ['LOADING', 'GET_ACTIVITIES_SUCCESS', 'GET_ACTIVITIES_FAILED'],
  payload: {
    request: {
      url: `/users/activities/?from=${from}&to=${to}`,
      method: 'GET'
    }
  },
  success: false,
  failure: 'Something went wrong while retrieving activities'
})

export const getStats = ({ from = subDays(new Date(), 30), to = new Date() } = {}) => ({
  types: ['LOADING', 'GET_STATS_SUCCESS', 'GET_STATS_FAILED'],
  payload: {
    request: {
      url: `/users/stats?from=${from}&to=${to}`,
      method: 'GET'
    }
  },
  success: false,
  failure: 'Something went wrong while retrieving stats'
})
