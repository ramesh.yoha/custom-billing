export const sync = (configUpdated = false) => ({
  types: ['NO_LOADING', 'SYNC_SUCCESS', 'SYNC_FAILED'],
  payload: {
    request: {
      url: `/auth/sync?configUpdated=${configUpdated}`,
      method: 'GET'
    }
  },
  success: '',
  failure: 'Something went wrong while syncing !'
})
