import { subDays } from 'date-fns'

export const updateCredits = (transaction, transactionToClientAccountBalance = null) => ({
  types: ['LOADING', 'UPDATE_CREDIT_SUCCESS', 'UPDATE_CREDIT_FAILED'],
  payload: {
    request: {
      url: `/transactions`,
      method: 'POST',
      data: transaction
    }
  },
  success: 'Successfully updated credits',
  failure: 'Something went wrong while adding credits!',
  transactionToClientAccountBalance
})

export const getTransactions = ({ from = subDays(new Date(), 30), to = new Date() } = {}) => ({
  types: ['LOADING', 'GET_TRANSACTIONS_SUCCESS', 'GET_TRANSACTIONS_FAILED'],
  payload: {
    request: {
      url: `/transactions/?from=${from}&to=${to}`,
      method: 'GET'
    }
  },
  success: '',
  failure: 'Something went wrong while retrieving transactions!'
})

export const increaseCredits = (transaction) => ({
  types: ['LOADING', 'INCREASE_CREDITS_SUCCESS', 'INCREASE_CREDITS_FAILED'],
  payload: {
    request: {
      url: `/transactions`,
      method: 'POST',
      data: transaction
    }
  },
  success: 'Successfully increased credits',
  failure: 'Something went wrong while adding credits!'
})
