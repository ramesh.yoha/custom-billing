export const getStaffs = () => ({
  types: ['LOADING', 'GET_STAFFS_SUCCESS', 'GET_STAFFS_FAILED'],
  payload: {
    request: {
      url: '/users/staff',
      method: 'GET'
    }
  },
  success: '',
  failure: 'Something went wrong while retrieving staffs !'
})

export const addStaff = (user) => ({
  types: ['LOADING', 'ADD_STAFF_SUCCESS', 'ADD_STAFF_FAILED'],
  payload: {
    request: {
      url: '/users/staff',
      method: 'POST',
      data: user
    }
  },
  success: 'Successfully added user',
  failure: 'Something went wrong while adding user!'
})

export const updateStaff = (username, user) => ({
  types: ['LOADING', 'UPDATE_USER_SUCCESS', 'UPDATE_USER_FAILED'],
  payload: {
    request: {
      url: `/users/staff/${username}`,
      method: 'PATCH',
      data: user
    }
  },
  success: `Successfully updated user`,
  failure: 'Something went wrong!'
})

export const deleteStaff = username => ({
  types: ['LOADING', 'DELETE_USER_SUCCESS', 'DELETE_USER_FAILED'],
  payload: {
    request: {
      url: `/users/staff/${username}`,
      method: 'DELETE'
    }
  },
  success: `Successfully deleted account`,
  failure: 'Something went wrong!'
})
