import React from 'react'
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton
} from '@material-ui/core'
import {
  Menu as MenuIcon,
  AccountCircle as AccountIcon
} from '@material-ui/icons'
import { LogoImage } from 'assets'

export default ({ username, toggleMobileSideBar, background = '#2D3446' }) => (
  <AppBar position='fixed' style={{ background }}>
    <Toolbar>
      <IconButton
        style={{ margin: '0px 20px 0px -12px' }}
        color='inherit'
        aria-label='Open drawer'
        onClick={() => toggleMobileSideBar(true)}
      >
        <MenuIcon />
      </IconButton>

      <img src={LogoImage} alt='Logo' style={{ maxWidth: 50, paddingRight: '10px' }} />

      <Typography variant='h6' color='inherit' noWrap> G1HD Billing </Typography>

      <div style={{ flexGrow: 1 }} />

      <div style={{ display: 'flex' }}>
        <AccountIcon />
        <Typography variant='subtitle1' color='inherit' noWrap style={{ paddingLeft: '5px' }}>
          {username}
        </Typography>
      </div>
    </Toolbar>
  </AppBar>
)
