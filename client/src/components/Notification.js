import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import {
  IconButton,
  Snackbar,
  SnackbarContent
} from '@material-ui/core'
import {
  Error as ErrorIcon,
  Info as InfoIcon,
  Close as CloseIcon,
  Warning as WarningIcon,
  CheckCircle as CheckCircleIcon
} from '@material-ui/icons'
import {
  green,
  amber
} from '@material-ui/core/colors'

import { clearNotification } from 'actions/general'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon
}

const styles = theme => ({
  success: {
    backgroundColor: green[600],
    color: '#fff'
  },
  error: {
    backgroundColor: theme.palette.error.dark,
    color: '#fff'
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
    color: '#fff'
  },
  warning: {
    backgroundColor: amber[700],
    color: '#fff'
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  }
})

function MySnackbarContent (props) {
  const { classes, className, message, onClose, variant, ...other } = props
  const Icon = variantIcon[variant]

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby='client-snackbar'
      message={
        <span id='client-snackbar' className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key='close'
          aria-label='Close'
          color='inherit'
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>
      ]}
      {...other}
    />
  )
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent)

class CustomizedSnackbars extends React.Component {
  handleClose = (event, reason) => {
    if (reason === 'clickaway') return
    this.props.clearNotification()
  }

  render () {
    const { notificationShow, notificationType, notificationMessage } = this.props

    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        open={notificationShow}
        autoHideDuration={notificationType === 'error' ? null : 3000}
        onClose={this.handleClose}
      >
        <MySnackbarContentWrapper
          onClose={this.handleClose}
          variant={notificationType}
          message={notificationMessage}
        />
      </Snackbar>
    )
  }
}

const mapStateToProps = state => ({
  notificationShow: state.general.notificationShow,
  notificationMessage: state.general.notificationMessage,
  notificationType: state.general.notificationType
})

const mapDispatchToProps = dispatch => ({
  clearNotification: () => dispatch(clearNotification())
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomizedSnackbars)
