import React from 'react'
import { Offline } from 'react-detect-offline'
import { PopupMessage } from 'components'

export default () =>
  <Offline polling={{ interval: 60000, url: '/api/checkStatus' }}>
    <PopupMessage
      title='No Active Internet Connection Detected'
      description='You can continue with your work once the connection is back.'
    />
  </Offline>
