import React from 'react'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core'

export default ({ title, description, image, onClose }) => {
  return (
    <Dialog
      open
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
      onClose={onClose}
    >
      <DialogTitle id='alert-dialog-title'>{title}</DialogTitle>
      <DialogContent style={{ textAlign: 'center' }}>
        <pre>
          <DialogContentText id='alert-dialog-description'>
            {description}
          </DialogContentText>
        </pre>
        {image && <img src={image} width='100%' alt='body' />}
      </DialogContent>
    </Dialog>
  )
}
