import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography
} from '@material-ui/core'

export default ({ open, title, message, disabled, confirmationProceed, confirmationCancel, disableBackdropClick = true, disableEscapeKeyDown = true }) => {
  return (
    <Dialog
      open={open}
      onClose={confirmationCancel}
      aria-labelledby='Confirmation'
      aria-describedby='Confirmation'
      disableBackdropClick={disableBackdropClick}
      disableEscapeKeyDown={disableEscapeKeyDown}
    >
      <DialogTitle id='alert-dialog-title' style={{ backgroundColor: '#ab023e' }}>
        <Typography
          style={{
            textAlign: 'left',
            fontSize: '18px',
            color: 'white',
            letterSpacing: 1
          }}
          variant='overline'
        >
          {title || 'Please Confirm'}

        </Typography>
      </DialogTitle>
      <DialogContent style={{ paddingTop: 20 }}>
        <Typography
          style={{
            textAlign: 'center',
            fontSize: '14px',
            letterSpacing: 1
          }}
          variant='overline'
        >
          {message}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={confirmationCancel} color='secondary'>
          Cancel
        </Button>
        <Button onClick={confirmationProceed} color='primary' variant='contained' autoFocus disabled={disabled}>
          Proceed
        </Button>
      </DialogActions>
    </Dialog>
  )
}
