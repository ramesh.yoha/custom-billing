import React, { Component } from 'react'
import { PopupMessage } from 'components'
import { InactivityImage } from 'assets'

export default class InactivityPopup extends Component {
  constructor (props) {
    super(props)
    this.state = {
      inactivity: 0
    }
  }

  componentDidMount = () => {
    window.onmousemove = this.resetInactiveTimer
    window.onclick = this.resetInactiveTimer
    window.onkeypress = this.resetInactiveTimer
    window.onscroll = this.resetInactiveTimer
    this.idleTimer = setTimeout(this.onIdle, 120000)
    this.refreshTokenTimer = setInterval(this.props.refreshToken, 1800000) // 30 minutes
  }

  onIdle = () => {
    this.timer = setInterval(this.progress, 1000)
  }

  componentWillUnmount = () => {
    clearInterval(this.timer)
    clearTimeout(this.idleTimer)
    clearInterval(this.refreshTokenTimer)
    window.onmousemove = null
    window.onclick = null
    window.onkeypress = null
    window.onscroll = null
  }

  resetInactiveTimer = () => {
    this.setState({ inactivity: 0 }, () => {
      clearTimeout(this.idleTimer)
      clearInterval(this.timer)
      this.idleTimer = setTimeout(this.onIdle, 120000)
    })
  }

  progress = () => {
    const { inactivity } = this.state
    if (inactivity >= 60) {
      clearInterval(this.timer)
      clearTimeout(this.idleTimer)
      clearInterval(this.refreshTokenTimer)
      window.onmousemove = null
      window.onclick = null
      window.onkeypress = null
      window.onscroll = null
      this.props.logout()
    } else {
      this.setState({ inactivity: inactivity + 1 })
    }
  }

  render () {
    if (!this.state.inactivity || this.state.inactivity > 60) return <></>

    return (
      <PopupMessage
        title='Session Idle'
        description={`You will be logged out in ${60 - this.state.inactivity} seconds due to inactivity.`}
        image={InactivityImage}
      />
    )
  }
}
