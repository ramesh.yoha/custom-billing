import React from 'react'
import { FourOhFourImage } from 'assets'

export default () =>
  <div style={{ textAlign: 'center' }}>
    <img src={FourOhFourImage} alt='404' />
  </div>
