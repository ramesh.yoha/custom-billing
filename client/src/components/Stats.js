import React from 'react'
import { ResponsiveLine } from '@nivo/line'
import { format } from 'date-fns'
import { uniqBy } from 'lodash'

const generateData = (data, userType) => {
  const admins = uniqBy(data.map(stats => {
    return {
      x: format(Date.parse(stats.createdAt), 'D-MMM-YY'),
      y: stats.stats.admins
    }
  }), 'x')

  const superResellers = uniqBy(data.map(stats => {
    return {
      x: format(Date.parse(stats.createdAt), 'D-MMM-YY'),
      y: stats.stats.superResellers
    }
  }), 'x')

  const resellers = uniqBy(data.map(stats => {
    return {
      x: format(Date.parse(stats.createdAt), 'D-MMM-YY'),
      y: stats.stats.resellers
    }
  }), 'x')

  const clients = uniqBy(data.map(stats => {
    return {
      x: format(Date.parse(stats.createdAt), 'D-MMM-YY'),
      y: stats.stats.clients
    }
  }), 'x')

  switch (userType) {
    case 'superAdmin':
      return [
        { id: 'Admins', data: admins },
        { id: 'SuperResellers', data: superResellers },
        { id: 'Resellers', data: resellers },
        { id: 'Clients', data: clients }
      ]
    case 'admin':
      return [
        { id: 'SuperResellers', data: superResellers },
        { id: 'Resellers', data: resellers },
        { id: 'Clients', data: clients }
      ]
    case 'superReseller':
      return [
        { id: 'Resellers', data: resellers },
        { id: 'Clients', data: clients }
      ]
    case 'reseller':
      return [
        { id: 'Clients', data: clients }
      ]
    default:
      return []
  }
}

export default ({ data, userType }) => (
  <ResponsiveLine
    margin={{
      top: 50,
      right: 50,
      bottom: 50,
      left: 50
    }}
    data={generateData(data, userType)}
    curve='monotoneX'
    axisBottom={{
      'orient': 'bottom',
      'tickSize': 5,
      'tickPadding': 5,
      'tickRotation': 0,
      'legend': 'Date',
      'legendOffset': 36,
      'legendPosition': 'middle'
    }}
    axisLeft={{
      'orient': 'left',
      'tickSize': 5,
      'tickPadding': 5,
      'tickRotation': 0,
      'legend': 'Count',
      'legendOffset': -40,
      'legendPosition': 'middle'
    }}
    colors='category10'
    dotSize={10}
    dotColor='inherit:darker(0.9)'
    dotBorderWidth={2}
    dotBorderColor='#ffffff'
    enableDotLabel
    dotLabel='y'
    dotLabelYOffset={-12}
    animate
    motionStiffness={90}
    motionDamping={15}
    legends={[
      {
        'anchor': 'top-left',
        'direction': 'column',
        'justify': false,
        'translateX': 10,
        'translateY': 100,
        'itemsSpacing': 0,
        'itemDirection': 'left-to-right',
        'itemWidth': 80,
        'itemHeight': 35,
        'itemOpacity': 0.9,
        'symbolSize': 16,
        'symbolShape': 'circle',
        'symbolBorderColor': 'rgba(0, 0, 0, .5)',
        'effects': [
          {
            'on': 'hover',
            'style': {
              'itemBackground': 'rgba(0, 0, 0, .03)',
              'itemOpacity': 1
            }
          }
        ]
      }
    ]}
  />
)
