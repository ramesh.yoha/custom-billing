import userRepo from '../models/userModel'
import axios from 'axios'
import { winstonLogger } from './logger'

const ministraAPI = process.env.MINISTRA_HOST + 'stalker_portal/api/'
const ministaUser = process.env.MINISTRA_USER
const ministraPW = process.env.MINISTRA_PW
const config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  auth: {
    username: `${ministaUser}`,
    password: `${ministraPW}`
  }
}

export async function checkPermissionRights(reqestedUser, currentUser, ifUsers) {
  winstonLogger.debug("Checking Permission for reqestedUser : " + reqestedUser + " and currentUser : " + currentUser.username)
  if(currentUser.userType == "specialDeleter" && ifUsers == 1) return false
  if (currentUser.userType == "reseller") {
    if (ifUsers == 1) {
      if (reqestedUser.storeAdmin !== currentUser.username && currentUser.username !== reqestedUser.username) return false
    }
    else {
      if (!currentUser.childUsernames.includes(reqestedUser)) return false
    }
  }
  if (currentUser.userType == "superReseller") {
    if (ifUsers == 1) {
      if (currentUser.userType == reqestedUser.userType) {
        if (currentUser.username !== reqestedUser.username) return false
      }
      else {
        if (!currentUser.childUsernames.includes(reqestedUser.username)) return false
      }
    }
    else {
      const resellers = await userRepo.find({ username: { $in: currentUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
      const childUsers = [].concat(...resellers.map(reseller => reseller.childUsernames))
      if (!childUsers.includes(reqestedUser)) return false
    }
  }
  if (currentUser.userType == "admin") {
    const superResellers = await userRepo.find({ username: { $in: currentUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    const childResellers = [].concat(...superResellers.map(superReseller => superReseller.childUsernames))
    if (ifUsers == 1) {
      if (reqestedUser.userType == "specialDeleter") return true
      if (currentUser.userType == reqestedUser.userType) {
        if (currentUser.username !== reqestedUser.username) return false
      }
      else {
        if (reqestedUser.userType == 'superReseller') {
          if (!currentUser.childUsernames.includes(reqestedUser.username)) return false
        }
        else {
          if (!childResellers.includes(reqestedUser.username)) return false
        }
      }
    }
    else {
      const resellers = await userRepo.find({ username: { $in: childResellers } }, null, { sort: { creditsAvailable: 1 } })
      const childUsers = [].concat(...resellers.map(reseller => reseller.childUsernames))
      if (!childUsers.includes(reqestedUser)) return false
    }
  }
  return true
}

async function getLogins(macs){
  winstonLogger.debug("Running getLogins Operation.")
  let ministraClients
  await axios.get(ministraAPI + 'accounts/' + macs.join(","), config)
    .then(response => {
      ministraClients = response.data
    })
    if (ministraClients.status !== 'OK') {
      return []
    }
    else {
      const clients = ministraClients.results.map(element => element.login)
      return clients
    }
}

export async function checkPermissionMinistra(reqestedMacs, currentUser) {
  winstonLogger.debug("Running checkPermissionMinistra Function...")
  var logins = (await getLogins(reqestedMacs))
  if(reqestedMacs.length > logins.length) return false
  if(currentUser.userType == "specialDeleter") return false
  if (currentUser.userType == "reseller") {
    if (Array.isArray(logins)) return logins.every(e => currentUser.childUsernames.includes(e))
    return currentUser.childUsernames.includes(logins)
  }
  if (currentUser.userType == "superReseller") {
    const resellers = await userRepo.find({ username: { $in: currentUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    const childUsers = [].concat(...resellers.map(reseller => reseller.childUsernames))
    if (Array.isArray(logins)) return logins.every(e => childUsers.includes(e))
    return childUsers.includes(logins)
  }
  if (currentUser.userType == "admin") {
    const superResellers = await userRepo.find({ username: { $in: currentUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    const childResellers = [].concat(...superResellers.map(superReseller => superReseller.childUsernames))
    const resellers = await userRepo.find({ username: { $in: childResellers } }, null, { sort: { creditsAvailable: 1 } })
    const childUsers = [].concat(...resellers.map(reseller => reseller.childUsernames))
    if (Array.isArray(logins)) return logins.every(e => childUsers.includes(e))
    return childUsers.includes(logins)
  }
  return true
}

export async function validParent(currentUserType, addingUserType, upgradingUser) {
  winstonLogger.debug("Running validParent Function...")
  if(currentUserType == "specialDeleter") return false
  if(upgradingUser == undefined){
    if (currentUserType == 'superAdmin' && addingUserType == 'specialDeleter') return true
    if (currentUserType == 'admin' && addingUserType == 'specialDeleter') return true
    if (currentUserType == 'superAdmin' && addingUserType == 'admin') return true
    if (currentUserType == 'admin' && addingUserType == 'superReseller') return true
    if (currentUserType == 'superReseller' && addingUserType == 'reseller') return true
    if (currentUserType == 'reseller') return false
    if (addingUserType == 'superAdmin') return false
}
else {
    const upgradingUserType = upgradingUser.userType
    if (addingUserType == "specialDeleter") return false
    if (currentUserType == 'superAdmin' && addingUserType == 'admin' && upgradingUserType == 'superReseller') return true
    if (currentUserType == 'admin' && addingUserType == 'superReseller' && upgradingUserType == 'reseller') return true
    if (currentUserType == 'superReseller') return false
    if (currentUserType == 'reseller') return false
    if (addingUserType == 'superAdmin') return false
}
  return false
}