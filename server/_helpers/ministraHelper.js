import mysql from 'mysql'
import { winstonLogger } from './logger'

var pool = mysql.createPool({
  host: process.env.MINISTRA_DB_HOST,
  user: process.env.MINISTRA_DB_USER,
  password: process.env.MINISTRA_DB_PW,
  database: process.env.MINISTRA_DB_NAME
});

const SQL_CRON_SELECT_FIELDS = '\
  mac AS stb_mac, \
  login, \
  CASE WHEN status=0 THEN 1 ELSE 0 END as status, \
  IFNULL(tariff_expired_date, "2000-01-01 00:00:00") AS tariff_expired_date \
  '


const SQL_SELECT_FIELDS = '\
  login, \
  fname AS full_name, \
  phone, \
  (select external_id from tariff_plan where id = tariff_plan_id) AS tariff_plan, \
  IFNULL(tariff_expired_date, "2000-01-01 00:00:00") AS tariff_expired_date, \
  (select external_id from tariff_plan where id = tariff_id_instead_expired) AS tariff_instead_expired, \
  serial_number AS stb_sn, \
  mac AS stb_mac, \
  stb_type, \
  CASE WHEN status=0 THEN 1 ELSE 0 END as status, \
  now_playing_content, \
  ip, \
  version, \
  comment, \
  ls account_number, \
  keep_alive>=FROM_UNIXTIME(UNIX_TIMESTAMP(NOW())-120) online, \
  last_active \
'


export function getAllClients() {
  winstonLogger.debug("Running getAllClients Function...")
  return new Promise(function (resolve, reject) {
    pool.query(`SELECT ${SQL_SELECT_FIELDS} FROM users`, (err, rows) => {
      if (err) {
        winstonLogger.error("Error querying Ministra DB : " + err)
        return reject(err);
      }
      winstonLogger.debug('Data successfully received from Ministra Db:\n')
      resolve(rows);
    })
  })
}


export function getClients(logins) {
  winstonLogger.debug("Running getClients Function for clients : " + logins)
  return new Promise(function (resolve, reject) {
    pool.query(`SELECT ${SQL_SELECT_FIELDS} FROM users WHERE login IN (${logins.map(x => `'${x}'`)})`, (err, rows) => {
      if (err) {
        winstonLogger.error("Error querying Ministra DB : " + err)
        return reject(err);
      }
      winstonLogger.debug('Data successfully received from Ministra DB:\n')
      resolve(rows);
    })
  })
}

export function getClientsCron(){
  winstonLogger.debug("Running getClientsCron Function")
  return new Promise(function (resolve, reject) {
    pool.query(`SELECT ${SQL_CRON_SELECT_FIELDS} FROM users`, (err, rows) => {
      if (err) {
        winstonLogger.error("Error querying Ministra DB : " + err)
        return reject(err);
      }
      winstonLogger.debug('Data successfully received from Ministra Db:\n')
      resolve(rows);
    })
  })
}