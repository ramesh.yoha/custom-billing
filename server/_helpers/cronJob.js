import axios from 'axios'
import { getClientsCron } from '../_helpers/ministraHelper'
import { ministraAPI, config } from '../controllers/ministraController'
import { mergeArrayObjectsByKey } from '../controllers/userController'
import { generateUserStats } from '../controllers/userController'
import clientRepo from '../models/clientModel'
import configRepo from '../models/configModel'
import { winstonLoggerCron } from './logger'
import userRepo from '../models/userModel'
import dateFns from 'date-fns'
import transactionRepo from '../models/transactionModel'


export async function nightlyCronJob(){
  winstonLoggerCron.info('Started Daily Maintenance Cron Job...')
  winstonLoggerCron.info('Updating runningCron value to true to stop API calls...')
  await configRepo.findOneAndUpdate({ configName : 'runningCron' }, { configValue : true })
  var lastCronDateConfig = await configRepo.findOne({ configName : 'lastCronDate' })
  var lastCronDate = dateFns.format(lastCronDateConfig.configValue, 'MM/DD/YYYY')
  var today = dateFns.format(new Date(), 'MM/DD/YYYY')
  if(today !== lastCronDate){
    try{
      await configRepo.findOneAndUpdate({ configName : 'lastCronDate' }, { configValue : new Date() })
      winstonLoggerCron.info('Delaying 1 Minute till starting activities...')
      await delay(60000).then();
      winstonLoggerCron.info('Completed Delay...')
      const ministraClients = await getClientsCron()
      const mongoClients = await clientRepo.find({})
      const mergedClients = mergeArrayObjectsByKey(mongoClients, ministraClients, 'login', 'login')

      //Actual Logic Starts Here
      await asyncForEach(mergedClients, async (element) => {
        const description = "Cron Transaction"
        const credits = -1
        const transactionFrom = element.parentUsername
        const transactionTo =  element.login
        const cronCheckDate = dateFns.subMonths(element.tariff_expired_date, element.accountBalance)
        const parent = await userRepo.findOne({ username : element.parentUsername })

        if (element.accountBalance > 0 && dateFns.isToday(cronCheckDate)){
          if( parent.creditsAvailable > 0 ){
            await userRepo.findOneAndUpdate({ username : element.parentUsername }, { $inc: { creditsAvailable : -1, creditsOwed : -1 } })
            await clientRepo.findOneAndUpdate({ login : element.login }, { $inc: { accountBalance : -1 } } )
            winstonLoggerCron.info('Successfully transfered 1 credit to ' + element.login + ', from ' + element.parentUsername + '.')
            winstonLoggerCron.info('Reseller ' + element.parentUsername + ' new creditsAvailable is ' + (parent.creditsAvailable-1) + '.')
            winstonLoggerCron.info('Client ' + element.login + ' new accountBalance is ' + (element.accountBalance-1) + '.')
            await transactionRepo.create([{ credits, description, transactionFrom, transactionTo }], { lean: true })
          }
          else {
            winstonLoggerCron.info('Reseller ' + element.parentUsername + ' has no creditsAvailable in his account. Hence the client is getting deactivated...')
            await axios.put(ministraAPI + 'users/' + element.login,
              'status=0', config)
              .then(response => {
                if (response.data.status == 'OK') winstonLoggerCron.info('Updated Client ' + element.login +  ' Status to False. (Deactivated)')
              })
          }
        }
        else if(element.accountBalance > 0 && element.status == 0){
          if( parent.creditsAvailable > 0 ){
            winstonLoggerCron.info('Reseller ' + element.parentUsername + ' has creditsAvailable in his account and client ' + element.login + ' has enough accountBalance. Hence the client is getting reactivated')
            await axios.put(ministraAPI + 'users/' + element.login,
              'status=1', config)
              .then(response => {
                if (response.data.status == 'OK') winstonLoggerCron.info('Updated Client ' + element.login +  ' Status to True. (Reactivated)')
              })
              await userRepo.findOneAndUpdate({ username : element.parentUsername }, { $inc: { creditsAvailable : -1, creditsOwed : -1 } })
              await clientRepo.findOneAndUpdate({ login : element.login }, { accountBalance : (element.accountBalance-1) } )  
              winstonLoggerCron.info('Successfully transfered 1 credit to ' + element.login + ', from ' + element.parentUsername + '.')
              await transactionRepo.create([{ credits, description, transactionFrom, transactionTo }], { lean: true })
          }
        }
        // Generate user statistics for the day
        const users = await userRepo.find({ userType: {$in : [ 'superAdmin', 'admin', 'superReseller', 'reseller' ]} })
        users.forEach(async user => await generateUserStats(user))
      })
    }
    catch(error){
      winstonLoggerCron.info('Cron ran into an error. ', error)
    }
  }
  winstonLoggerCron.info('Updating runningCron value to false to allow back API calls...')
  await configRepo.findOneAndUpdate({ configName : 'runningCron' }, { configValue : false })
  winstonLoggerCron.info('Daily Maintenance Cron Job is Completed Successfully...')
}
  
async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

function delay(ms){
  var ctr, rej, p = new Promise(function (resolve, reject) {
      ctr = setTimeout(resolve, ms);
      rej = reject;
  });
  p.cancel = function(){ clearTimeout(ctr); rej(Error("Cancelled"))};
  return p; 
}

