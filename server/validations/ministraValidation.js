import Joi from 'joi'


export const schemas = {

  idSchema: Joi.object({
    param: Joi.string().min(6).regex(/^\s*\S+\s*$/).error(new Error("Login doesn't meet valid criteria")).required()
  }),

  eventSchema: Joi.object({
    ids: Joi.array().items(Joi.string().regex(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/).error(new Error('ID should be a valid mac Address.'))),
    event: Joi.string().required(),
    msg: Joi.string().allow(''),
    ttl: Joi.number(),
    need_reboot: Joi.number().min(0).max(1),
    channel: Joi.number()
  }),

  msgSchema: Joi.object({
    ids: Joi.array().items(Joi.string().regex(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/).error(new Error('ID should be a valid mac Address.'))),
    msg: Joi.string()
  }),

  accountSubPostSchema: Joi.object({
    subscribed: Joi.array().items(Joi.string()).required()
  }),

  accountSubPutSchema: Joi.object({
    subscribed: Joi.string(),
    unsubscribed: Joi.string()
  })

}
