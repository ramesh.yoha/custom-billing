import Joi from 'joi'
import dateFns from 'date-fns'


export const schemas = {

  idSchema: Joi.object({
    param: Joi.string().min(6).regex(/^\s*\S+\s*$/).required().error(new Error("Login doesn't meet valid criteria"))
  }),

  macSchema: Joi.object({
    param: Joi.string().regex(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/).error(new Error('Should be a valid mac Address.'))
  }),

  addSchema: Joi.object({
    login: Joi.string().lowercase().min(6).regex(/^\s*\S+\s*$/).error(new Error("Login doesn't meet valid criteria")).required(),
    stb_mac: Joi.string().regex(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/).error(new Error('Should be a valid mac Address.')),
    full_name: Joi.string().required(),
    phone: Joi.string().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    status: Joi.number(),
    password: Joi.string().required(),
    tariff_plan: Joi.number(),
    // 1 Day free trial by settinge expiry for tomorrow.
    tariff_expired_date: Joi.string().default(dateFns.format(dateFns.startOfTomorrow(), 'YYYY-MM-DD')).regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/),
    // Change default option once the ministra basic tariff plan is finalized.
    tariff_instead_expired: Joi.number().default(1),
    comment: Joi.string().allow("").default('')
  }),

  updateSchema: Joi.object({
    full_name: Joi.string(),
    phone: Joi.string().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    status: Joi.number(),
    password: Joi.string(),
    stb_mac: Joi.string().regex(/^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/).error(new Error('Should be a valid mac Address.')),
    tariff_plan: Joi.number(),
    comment: Joi.string().allow("")
  })

}
