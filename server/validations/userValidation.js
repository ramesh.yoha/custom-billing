import Joi from 'joi'


export const schemas = {

  usernameSchema: Joi.object({
    param: Joi.string().required()
  }),

  addSchema: Joi.object({
    username: Joi.string().lowercase().min(8).regex(/^\s*\S+\s*$/).error(new Error("Username doesn't meet valid criteria")).required(),
    email: Joi.string().email().allow('').error(new Error('Email Address should be a valid Email Address.')),
    password: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    phoneNo: Joi.string().required().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    userType: Joi.string().required().valid(['superAdmin', 'admin', 'superReseller', 'reseller', 'specialDeleter']),
    accountStatus: Joi.boolean().required()
  }),

  addStaffSchema: Joi.object({
    username: Joi.string().lowercase().min(8).regex(/^\s*\S+\s*$/).error(new Error("Username doesn't meet valid criteria")).required(),
    email: Joi.string().email().allow('').error(new Error('Email Address should be a valid Email Address.')),
    password: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    phoneNo: Joi.string().required().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    accountStatus: Joi.boolean().required()
  }),

  updateStaffSchema: Joi.object({
    email: Joi.string().allow("").email().error(new Error('Email Address should be a valid Email Address.')),
    password: Joi.string(),
    firstName: Joi.string(),
    lastName: Joi.string(),
    phoneNo: Joi.string().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    accountStatus: Joi.boolean()
  }),

  updateSchema: Joi.object({
    email: Joi.string().allow("").email().error(new Error('Email Address should be a valid Email Address.')),
    password: Joi.string(),
    firstName: Joi.string(),
    lastName: Joi.string(),
    phoneNo: Joi.string().regex(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/),
    accountStatus: Joi.boolean()
  }),

  userLoginSchema: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required()
  }),

  upgradeSchema: Joi.object({
    username: Joi.string().min(8).required().regex(/^\s*\S+\s*$/).error(new Error("Username doesn't meet valid criteria")),
    userType: Joi.string().required(),
    password: Joi.string()
  })

}
