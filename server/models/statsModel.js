import mongoose, { Schema } from 'mongoose'
import { endOfToday } from 'date-fns'


const statsSchema = new Schema({
  createdAt: { 
    type: Date, 
    default: endOfToday() 
  },
  username: {
    type: String,
    required: true
  },
  stats: {
    type: {},
    default: {}
  },
})


export default mongoose.model('Stats', statsSchema)