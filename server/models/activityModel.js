import mongoose, { Schema } from 'mongoose'


const activitySchema = new Schema({
  createdAt: { 
    type: Date, 
    default: Date.now 
  },
  from: {
    type: String,
    required: true
  },
  to: {
    type: {}, // could be multiple (eg) sending events
    required: true
  },
  action: {
    type: String,
    required: true
  },
  description: {
    type: {},
    default: ''
  },
})


export default mongoose.model('Activity', activitySchema)