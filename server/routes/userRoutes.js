import passport from '../_helpers/passport'
import { validateParam, validateBody } from '../validations'
import { schemas } from '../validations/userValidation'
import { 
  getAllUsers, 
  addUser, 
  validateUsername, 
  getUser, 
  updateUser, 
  deleteUser, 
  upgradeUserRole,
  getAllUsernames,
  getUserActivities,
  getUserStats,
  staffPermission,
  getAllStaff,
  addStaff,
  deleteStaff,
  getDeletedUsers,
  getCompleteDeletedUsers
} from '../controllers/userController'


const passportJWT = passport.authenticate('jwt', { session: false })
const router = require('express-promise-router')()

router.route('/')
  .all(
    passportJWT
  )
  .get(
    getAllUsers
  )
  .post(
    validateBody(schemas.addSchema),
    addUser
  )

  router.route('/staff')
  .all(
    passportJWT,
    staffPermission
  )
  .get(
    getAllStaff
  )
  .post(
    validateBody(schemas.addStaffSchema),
    addStaff
  )

  router.route('/staff/:username')
  .all(
    passportJWT,
    validateParam(schemas.usernameSchema, 'username'),
    validateUsername
  )
  .get(
    getUser
  )
  .patch(
    validateBody(schemas.updateStaffSchema),
    updateUser
  )
  .delete(
    staffPermission,
    deleteStaff
  )

router.route('/usernames')
.get(
  passportJWT,
  getAllUsernames
)

router.route('/activities')
.get(
  passportJWT,
  getUserActivities
)

router.route('/stats')
.get(
  passportJWT,
  getUserStats
)

router.route('/deleted')
.get(
  passportJWT,
  getDeletedUsers
)

router.route('/deleted/complete')
.get(
  passportJWT,
  getCompleteDeletedUsers
)

router.route('/:username')
  .all(
    passportJWT,
    validateParam(schemas.usernameSchema, 'username'),
    validateUsername
  )
  .get(
    getUser
  )
  .patch(
    validateBody(schemas.updateSchema),
    updateUser
  )
  .delete(
    deleteUser
  )

router.route('/upgrade/:username')
  .post(
    passportJWT,
    validateParam(schemas.usernameSchema, 'username'),
    validateUsername,
    validateBody(schemas.upgradeSchema),
    upgradeUserRole
  )



export default router