import userRepo from '../models/userModel'
import clientRepo from '../models/clientModel'
import userLoginsRepo from '../models/userLoginsModel'
import activityRepo from '../models/activityModel'
import statsRepo from '../models/statsModel'
import transactionRepo from '../models/transactionModel'
import JWT from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import { getAllClients, getClients } from '../_helpers/ministraHelper'
import { checkPermissionRights, validParent } from '../_helpers/checkPermission'
import { winstonLogger } from '../_helpers/logger'
import { isValid, compareAsc, startOfTomorrow, endOfToday } from 'date-fns'
import { io } from '../index'
import { subSeconds } from 'date-fns'

const tokenExpiryHours = process.env.TOKEN_EXPIRY_HOURS

export async function login(req, res, next) { 
  winstonLogger.debug("Running Operation login...")
  const user = req.user
  const lastLogin = await userLoginsRepo.findOne({ username : user.username }).sort({ loginDate : -1 }).limit(1)
  if (user.accountStatus == false || user.storeAdminAccountStatus == false) return res.status(403).json({ error: `Your account is locked. Please contact your Adminstrator for more information.` })
  const token = getToken(user)
  await postLoginDetails(req) 
  res.status(201).json({ user, token, lastLogin })
}

async function postLoginDetails(req)  {
  winstonLogger.debug("Get login details...")
  const username = req.user.username
  const loginUserAgent = req.get('User-Agent')
  const loginDate = new Date() 
  var loginIp = req.headers['x-forwarded-for'] || 
    req.connection.remoteAddress || req.socket.remoteAddress ||
    (req.connection.socket ? req.connection.socket.remoteAddress : null) || ''
  loginIp = loginIp.replace('::ffff:','')
  await userLoginsRepo.create([{ username, loginUserAgent, loginDate, loginIp }], { lean: true })
}

export async function getLastLogins(req, res, next) {
  winstonLogger.debug("Running getLastLogins operation...")
  const loginDetails = await userLoginsRepo.find({ username : req.user.username }, null, { sort: { loginDate: -1 } })
  return res.status(200).json({loginDetails})
}

const getToken = user => {
  winstonLogger.debug("Running Operation getToken...")
  return JWT.sign({
    iss: 'G1HD',
    sub: user.id,
    iat: new Date().getTime(),
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * tokenExpiryHours) 
  }, process.env.JWT_SECRET)
}

export async function validateUsername(req, res, next) {
  winstonLogger.debug("Running Operation validateUsername...")
  const user = await userRepo.findOne({ username: req.params.username })
  if (!user) return res.status(404).json({ error: `User with username ${req.params.username} was not found in DB` })
  res.locals.user = user
  if (await checkPermissionRights(user, req.user, 1) == false) return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  next()
}

export async function staffPermission(req, res, next) {
  if (req.user.userType !== "reseller") return res.status(403).json({ error: `Only reseller has staff accounts.` })
  if (req.user.storeAdmin !== "me") return res.status(403).json({ error: `You have no rights to perform this action.` })
  next()
}

export async function getAllStaff(req, res, next){
  winstonLogger.debug("Running Operation getAllStaff...")
  const staff = await userRepo.find({ storeAdmin: req.user.username })
  return res.status(200).json(staff)
}

export async function addStaff(req, res, next){
  winstonLogger.debug("Running Operation addStaff...")
  const userType = req.user.userType
  const storeAdmin = req.user.username
  const { username, email, password, firstName, lastName, phoneNo, accountStatus } = req.value.body
  const existingUser = await userRepo.findOne({ username })
  if (existingUser)
    return res.status(422).json({ error: `User already exists with username: ${username}` })
  const createdStaff = await userRepo.create([{ username, email, password, firstName, lastName, phoneNo, userType, accountStatus, storeAdmin }], { lean: true })
  const token = getToken(createdStaff)
  const staff = createdStaff[0] 
  await activityRepo.create({from: req.user.username, to: staff.username, action: 'CREATE', description: staff})
  io.emit('message', { type: "ADD_STAFF_SUCCESS", authUsername: req.user.username })
  return res.status(201).json({ staff, token })
}

export async function deleteStaff(req, res, next) {
  winstonLogger.debug("Running Operation deleteUser...")
  if (res.locals.user.storeAdmin !== req.user.username) return res.status(403).json({ error: `You have no rights to perform this action.` })
  const username = res.locals.user.username
  await res.locals.user.remove()
  res.locals.user._doc.username = "DELETED_"+res.locals.user._doc.username
  const activitiesToUpdate = ["DELETE", "UPDATE", "CREATE", "UPGRADE"]
  await transactionRepo.updateMany({ transactionTo : username }, { transactionTo: res.locals.user._doc.username })
  await transactionRepo.updateMany({ transactionFrom : username }, { transactionFrom: res.locals.user._doc.username })
  await activityRepo.updateMany({ to : username, action: { $in:  activitiesToUpdate } }, { to: res.locals.user._doc.username })
  await activityRepo.updateMany({ from : username, action: { $in:  activitiesToUpdate } }, { from: res.locals.user._doc.username })
  io.emit('message', { type: "DELETE_USER_SUCCESS", authUsername: req.user.username })
  return res.status(200).json(`User with username: ${username} successfully deleted.`)
}

export async function getAllUsers(req, res, next) {
  winstonLogger.debug("Running Operation getAllUsers...")
  if(req.user.userType == "specialDeleter") return res.status(403).json(`You can't perform these actions...`)
  const users = await getAllUserHelper(req.user)
  return res.status(200).json(users)
}

export async function getAllUserHelper(reqUser){
  let admins = []
  let superResellers = []
  let resellers = []
  let clients = []
  let specialDeleters = []
  if (reqUser.userType == "superAdmin") {
    specialDeleters = await userRepo.find({ userType: "specialDeleter"})
    admins = await userRepo.find({ username: { $in: reqUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    superResellers = await getChildren(admins, 0)
    resellers = await getChildren(superResellers, 0)
    clients = (await getChildren(resellers, 2)).sort((a, b) => new Date(a.tariff_expired_date) - new Date(b.tariff_expired_date))
  }
  if (reqUser.userType == "admin") {
    specialDeleters = await userRepo.find({ userType: "specialDeleter"})
    superResellers = await userRepo.find({ username: { $in: reqUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    resellers = await getChildren(superResellers, 0)
    clients = (await getChildren(resellers, 1)).sort((a, b) => new Date(a.tariff_expired_date) - new Date(b.tariff_expired_date))
  }
  if (reqUser.userType == "superReseller") {
    resellers = await userRepo.find({ username: { $in: reqUser.childUsernames } }, null, { sort: { creditsAvailable: 1 } })
    clients = (await getChildren(resellers, 1)).sort((a, b) => new Date(a.tariff_expired_date) - new Date(b.tariff_expired_date))
  }
  if (reqUser.userType == "reseller") {
    clients = (await getChildren(reqUser.childUsernames, 3)).sort((a, b) => new Date(a.tariff_expired_date) - new Date(b.tariff_expired_date))
  }
  const returnElement = { specialDeleters, admins, superResellers, resellers, clients }
  return returnElement
}

export async function addUser(req, res, next) {
  winstonLogger.debug("Running Operation addUser...")
  const { username, email, password, firstName, lastName, phoneNo, userType, accountStatus} = req.value.body
  const parentUsername = req.user.username
  if (await validParent(req.user.userType, userType, res.locals.user) == false) return res.status(403).json({ error: `You have no rights to add this user.` })
  const existingUser = await userRepo.findOne({ username })
  if (existingUser)
    return res.status(422).json({ error: `User already exists with username: ${username}` })
  var createdUser
  if (userType !== "specialDeleter") {
    createdUser = await userRepo.create([{ username, email, password, firstName, lastName, phoneNo, userType, accountStatus, parentUsername }], { lean: true })
  }
  else{
    createdUser = await userRepo.create([{ username, email, password, firstName, lastName, phoneNo, userType, accountStatus }], { lean: true })
  }
  if (!req.user.childUsernames.includes(username) && userType !== "specialDeleter") {
    await req.user.update({ $push: { childUsernames: username } })
  }
  const user = createdUser[0] 
  await activityRepo.create({from: req.user.username, to: user.username, action: 'CREATE', description: user})
  io.emit('message', { type: "ADD_USER_SUCCESS", authUsername: req.user.username })
  return res.status(201).json({ user })
}

export async function getUser(req, res, next) {
  winstonLogger.debug("Running Operation getUser...")
  return res.status(200).json(res.locals.user)
}

export async function updateUser(req, res, next) {
  winstonLogger.debug("Running Operation updateUser...")
  if (req.body.password !== undefined) {
    const salt = await bcrypt.genSalt(10)
    req.body.password = await bcrypt.hash(req.body.password, salt)
  }
  await res.locals.user.update(req.body)
  req.value.body.updatedAt = new Date()
  if (req.body.password == undefined) await activityRepo.create({from: req.user.username, to: res.locals.user.username, action: 'UPDATE', description: req.body })
  io.emit('message', { type: "UPDATE_USER_SUCCESS", authUsername: req.user.username })
  return res.status(200).json({ ...res.locals.user._doc, ...req.value.body })
}

export async function deleteUser(req, res, next) {
  winstonLogger.debug("Running Operation deleteUser...")
  if (req.user.userType == "reseller") return res.status(403).json({ error: `Only your superReseller/admin can delete your account.` })
  if (res.locals.user.childUsernames.length !== 0) return res.status(422).json({ error: `The user ${res.locals.user.username} has childs. Please remove/switch childs before you delete this user.`})
  if (res.locals.user.creditsAvailable > 0 || res.locals.user.creditsOwed > 0) return res.status(422).json({ error: `The user ${res.locals.user.username} has credits available/credits owed. Please handle them before you delete this user.`})
  const username = res.locals.user.username
  await userRepo.findOneAndUpdate({ username: res.locals.user.parentUsername }, { $pull: { childUsernames: username } })
  await res.locals.user.remove()
  await userRepo.deleteMany({ storeAdmin : username })
  res.locals.user._doc.username = "DELETED_"+res.locals.user._doc.username
  const activitiesToUpdate = ["DELETE", "UPDATE", "CREATE", "UPGRADE"]
  await transactionRepo.updateMany({ transactionTo : username }, { transactionTo: res.locals.user._doc.username })
  await transactionRepo.updateMany({ transactionFrom : username }, { transactionFrom: res.locals.user._doc.username })
  await activityRepo.updateMany({ to : username, action: { $in:  activitiesToUpdate } }, { to: res.locals.user._doc.username })
  await activityRepo.updateMany({ from : username, action: { $in:  activitiesToUpdate } }, { from: res.locals.user._doc.username })
  await activityRepo.create({from: req.user.username, to: res.locals.user._doc.username, action: 'DELETE', description: res.locals.user._doc})
  io.emit('message', { type: "DELETE_USER_SUCCESS", authUsername: req.user.username })
  return res.status(200).json(`User with username: ${username} successfully deleted.`)
}

export async function upgradeUserRole(req, res, next) {
  winstonLogger.debug("Running Operation upgradeUserRole...")
  const { username, userType, password } = req.value.body
  const { email, firstName, lastName, phoneNo, accountStatus, upgradedAccount } = res.locals.user
  const parentUsername = req.user.username
  if (await validParent(req.user.userType, userType, res.locals.user) == false) return res.status(403).json({ error: `You have no rights to add this user.` })
  if(upgradedAccount == true) return res.status(422).json({ error: `This user already has been upgraded to ${userType}.` })
  const existingUser = await userRepo.findOne({ username })
  if (existingUser)
    return res.status(422).json({ error: `User already exists with username: ${username}` })
  const newlyUpgradedUser = await userRepo.create([{ username, email, password, firstName, lastName, phoneNo, userType, accountStatus, parentUsername, childUsernames : req.params.username }], { lean: true })
  if (!req.user.childUsernames.includes(username)) {
    await req.user.update({ $push: { childUsernames: username.toLowerCase() }})
  }
  const oldParentUsername = res.locals.user.parentUsername
  await res.locals.user.update({parentUsername : req.value.body.username, upgradedAccount : true})
  var oldParent = await userRepo.findOne({ username: oldParentUsername })
  await oldParent.update({ $pull: { childUsernames: req.params.username } })
  await activityRepo.create({from: req.user.username, to: username, action: 'UPGRADE', description: newlyUpgradedUser})
  io.emit('message', { type: "UPGRADE_USER_SUCCESS", authUsername: req.user.username })
  return res.status(200).json(newlyUpgradedUser)
}

async function getChildren(list, isMinistra) {
  winstonLogger.debug("Running Operation getChildren...")
  // given a list of parentObjects, return all direct childObjects of each parent
  const childUsernames = [].concat(...list.map(parent => parent.childUsernames))
  if (isMinistra == 0) {
    return await userRepo.find({ username: { $in: childUsernames } }, null, { sort: { creditsAvailable: 1 } })
  }
  else if (isMinistra == 1) {
    const logins = await (childUsernames.length == 0 ? [''] : childUsernames)
    const ministraClients = await getClients(logins)
    const mongoClients = await clientRepo.find({ login: { $in: logins } })
    return mergeArrayObjectsByKey(mongoClients, ministraClients, 'login', 'login')
  }
  else if (isMinistra == 2) {
    const ministraClients = await getAllClients()
    const mongoClients = await clientRepo.find({})
    return mergeArrayObjectsByKey(mongoClients, ministraClients, 'login', 'login')
  }
  else {
    const logins = await (list.length == 0 ? ['1'] : list)
    const ministraClients = await getClients(logins)
    const mongoClients = await clientRepo.find({ login: { $in: logins } })
    return mergeArrayObjectsByKey(mongoClients, ministraClients, 'login', 'login')
  }
}

export function mergeArrayObjectsByKey(obj1Array, obj2Array, key1, key2) {
  winstonLogger.debug("Running Operation mergeArrayObjectsByKey...")
  return obj1Array.map(obj1=>({...obj1._doc, ...(obj2Array.find(obj2=>obj2[key2]==obj1[key1]))}))
}

export async function getAllUsernames(req, res, next) {
  const usernames = await userRepo.find({}, 'username -_id')
  return res.status(200).json(usernames.map(user => user.username))
}

export async function getUserActivities(req, res, next) {
  winstonLogger.debug("Running Operation getUserActivities...")
  const {from=subSeconds(new Date(), 60), to=new Date()} = req.query
  if (!from || !to) return res.status(422).json({ error: 'Required Query params "from" and "to".(YYYY-MM-DD)'})
  if (!isValid(new Date(from))) return res.status(422).json({ error: 'Query param "from" must be a valid date (YYYY-MM-DD)'})
  if (!isValid(new Date(to))) return res.status(422).json({ error: 'Query param "to" must be a valid date (YYYY-MM-DD)'})
  if (compareAsc(new Date(from), new Date(to)) !== -1) return res.status(422).json({ error: 'Date "from" must be a past date compared with "to"'})


  const allUsers = await getAllUserHelper(req.user)
  var allUsernames = []
  allUsers.admins.forEach(element => { allUsernames.push(element.username) });
  allUsers.superResellers.forEach(element => { allUsernames.push(element.username) });
  allUsers.resellers.forEach(element => { allUsernames.push(element.username) });
  allUsers.clients.forEach(element => { allUsernames.push(element.login) });
  // For Staff
  (await userRepo.find({ storeAdmin: { $in: allUsernames } }).select("username -_id")).forEach(element => { allUsernames.push(element.username) })
  // Adding current user
  allUsernames.push(req.user.username)
  
  const activities = await activityRepo.find({ 
    $or: [{ to: { $in: allUsernames } }, { from: { $in: allUsernames } }], 
    createdAt: {$gte: new Date(from), $lt: startOfTomorrow(new Date(to))} 
  }, '-__v', { sort: { createdAt: -1 } })

  return res.status(200).json(activities)
  }

export async function getAllNestedChildren(parent, result=[]) {
  let children = []
  if (parent.childUsernames && parent.childUsernames.length > 0) {
    if (parent.userType === 'reseller') {
      children = parent.childUsernames.map(client => ({ login: client }))
    } else {
      children = await userRepo.find({ username: {$in : parent.childUsernames} })
    }
  }
  for (let child of children) {
    result = [...await getAllNestedChildren(child, result)]
  }
  return [...result, ...children]
}

export async function getUserStats(req, res, next) {
  const {from=subSeconds(new Date(), 60), to=new Date()} = req.query
  if (!from || !to) return res.status(422).json({ error: 'Required Query params "from" and "to".(YYYY-MM-DD)'})
  if (!isValid(new Date(from))) return res.status(422).json({ error: 'Query param "from" must be a valid date (YYYY-MM-DD)'})
  if (!isValid(new Date(to))) return res.status(422).json({ error: 'Query param "to" must be a valid date (YYYY-MM-DD)'})
  if (compareAsc(new Date(from), new Date(to)) !== -1) return res.status(422).json({ error: 'Date "from" must be a past date compared with "to"'})

  await generateUserStats(req.user)
  const stats = await statsRepo.find({ createdAt: {$gte: new Date(from), $lt: startOfTomorrow(new Date(to))}, username: req.user.username }, '-__v').sort({ createdAt : 1 })

  return res.status(200).json(stats)
}

export async function getDeletedUsers(req, res, next){
  winstonLogger.debug("Running Operation getDeletedUsers...")
  var deletedUsers = await activityRepo.find( { $or: [ { 'description.parentUsername' : req.user.username, action: "DELETE" }, { 'description.storeAdmin' : req.user.username, action: "DELETE" } ] }).select("description -_id")
  return res.status(200).json(deletedUsers.map(user => user.description))
}

export async function getCompleteDeletedUsers(req, res, next){
  winstonLogger.debug("Running Operation getCompleteDeletedUsers...")
  if (req.user.userType !== "superAdmin") return res.status(403).json({ error: `Only your superAdmin has access to this endpoint.` })
  var deletedUsers = await activityRepo.find({ action: "DELETE" }).select("description -_id")
  return res.status(200).json(deletedUsers.map(user => user.description))
}

export async function generateUserStats(user) {
  const all_nested_children = await getAllNestedChildren(user)

  const superAdmins = all_nested_children.filter(user => user.userType==='superAdmin')
  const admins = all_nested_children.filter(user => user.userType==='admin')
  const superResellers = all_nested_children.filter(user => user.userType==='superReseller')
  const resellers = all_nested_children.filter(user => user.userType==='reseller')
  const clients = all_nested_children.filter(user => user.login)

  const stats = {
    superAdmins: superAdmins.length,
    admins: admins.length,
    superResellers: superResellers.length,
    resellers: resellers.length,
    clients: clients.length
  }
  await statsRepo.findOneAndUpdate({createdAt: endOfToday(), username: user.username}, {stats, username: user.username}, {upsert:true})
}