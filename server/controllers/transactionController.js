import transactionRepo from '../models/transactionModel'
import userRepo from '../models/userModel'
import clientRepo from '../models/clientModel'
import configRepo from '../models/configModel'
import { winstonLogger } from '../_helpers/logger'
import axios from 'axios'
import dateFns from 'date-fns'
import { io } from '../index'
import { getAllUserHelper } from '../controllers/userController'

const ministraAPI = process.env.MINISTRA_HOST + 'stalker_portal/api/'
const ministaUser = process.env.MINISTRA_USER
const ministraPW = process.env.MINISTRA_PW
const config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  auth: {
    username: `${ministaUser}`,
    password: `${ministraPW}`
  }
}

export async function getTransactions(req, res, next) {
  winstonLogger.debug("Running Operation getTransactionsForUser...")
  const {from=dateFns.subSeconds(new Date(), 60), to=new Date()} = req.query
  if (!from || !to) return res.status(422).json({ error: 'Required Query params "from" and "to".(YYYY-MM-DD)'})
  if (!dateFns.isValid(new Date(from))) return res.status(422).json({ error: 'Query param "from" must be a valid date (YYYY-MM-DD)'})
  if (!dateFns.isValid(new Date(to))) return res.status(422).json({ error: 'Query param "to" must be a valid date (YYYY-MM-DD)'})
  if (dateFns.compareAsc(new Date(from), new Date(to)) !== -1) return res.status(422).json({ error: 'Date "from" must be a past date compared with "to"'})

  const allUsers = await getAllUserHelper(req.user)
  var allUsernames = []
  allUsers.admins.forEach(element => { allUsernames.push(element.username) });
  allUsers.superResellers.forEach(element => { allUsernames.push(element.username) });
  allUsers.resellers.forEach(element => { allUsernames.push(element.username) });
  allUsers.clients.forEach(element => { allUsernames.push(element.login) });
  // For Staff
  (await userRepo.find({ storeAdmin: { $in: allUsernames } }).select("username")).forEach(element => { allUsernames.push(element.username) })
  // Adding current user
  allUsernames.push(req.user.username)
  
  const transactions = await transactionRepo.find({ 
    $or: [{ transactionTo: { $in: allUsernames } }, { transactionFrom: { $in: allUsernames } }], 
    createdAt: {$gte: new Date(from), $lt: dateFns.startOfTomorrow(new Date(to))} 
  }, null, { sort: { createdAt: -1 } })

  return res.status(200).json(transactions)
}

export async function addTransaction(req, res, next) {
  winstonLogger.debug("Running Operation addTransaction...")
  const transactionFrom = req.user.username
  const { credits, description, transactionTo } = req.value.body
  if(req.user.userType == "specialDeleter") return res.status(403).json(`You can't perform these actions...`)
  if (!req.user.childUsernames.includes(transactionTo) && req.user.userType !== "superAdmin" ) return res.status(403).json(`You can't add credits to the user ${transactionTo}`)
  if (req.user.userType == "reseller") {
    let currReseller
    if (req.user.storeAdmin == "me") currReseller = req.user
    else currReseller = await userRepo.findOne({ username : req.user.storeAdmin })
    // Find existing expiry Date
    await axios.get(ministraAPI + 'users/' + transactionTo, config)
      .then(response => {
        res.locals.clientExpiryDate = response.data.results.tariff_expired_date
      })
    
    // Mongo transactions
    const client = await clientRepo.findOne({ login: transactionTo })
    if (credits == 0) return res.status(400).json('You cant add/recover 0 credits...')
    if (credits > 0 && currReseller.creditsAvailable == 0  && client.accountBalance == 0) return res.status(400).json(`You have no enough credits to transfer.`)
    if (credits < 0 && client.accountBalance < (-1 * credits)) return res.status(400).json("Not enough balance to recover the credits. Try again with lesser credits.")
    if (client.accountBalance == 0 && credits > 1){
      await currReseller.update({ creditsAvailable: (currReseller.creditsAvailable - 1), creditsOwed: (currReseller.creditsOwed + (credits-1))  })
      await clientRepo.findOneAndUpdate({ login: transactionTo }, { $inc: { accountBalance: credits-1 } })
    }
    if (client.accountBalance == 0 && credits == 1){
      // Make expiry date in date format
      let existingExpiryDate
      if (res.locals.clientExpiryDate == null || res.locals.clientExpiryDate == "0000-00-00 00:00:00") {
        existingExpiryDate = await dateFns.startOfTomorrow()
      }
      else {
        existingExpiryDate = await new Date(res.locals.clientExpiryDate)
      }

      // Check expiry Date Logic to add credits
      if(existingExpiryDate > dateFns.startOfTomorrow()){
        await currReseller.update({ creditsOwed: (currReseller.creditsOwed + 1)  })
        await clientRepo.findOneAndUpdate({ login: transactionTo }, { $inc: { accountBalance: 1 } })
      }
      else {
        await currReseller.update({ creditsAvailable: (currReseller.creditsAvailable - 1)  })
      }
    }
    if (client.accountBalance > 0 && credits > 0){ 
      await currReseller.update({ creditsOwed: (currReseller.creditsOwed + credits)  })
      await clientRepo.findOneAndUpdate({ login: transactionTo }, { $inc: { accountBalance: credits } })
    }
    if ((client.accountBalance + credits) >= 0 && credits < 0){
      await currReseller.update({ creditsOwed: (currReseller.creditsOwed + (credits))  })
      await clientRepo.findOneAndUpdate({ login: transactionTo }, { $inc: { accountBalance: credits } })
    }

    // Find New Expiry Date
    let expiredDate = []
    if (res.locals.clientExpiryDate == null || res.locals.clientExpiryDate == "0000-00-00 00:00:00") {
      expiredDate = await `tariff_expired_date=${expiryDateAfterTransaction(0, credits)}`
    }
    else { 
      expiredDate = await `tariff_expired_date=${expiryDateAfterTransaction(res.locals.clientExpiryDate, credits)}`
  }

    // Ministra Transactions
    await axios.put(ministraAPI + 'users/' + transactionTo,
      expiredDate, config)
      .then(response => {
        res.locals.updatedUser = response.data
      })
  }
  else {
    // Non reseller transactions
    if(req.user.userType == "superAdmin" && (transactionTo !== req.user.username && !req.user.childUsernames.includes(transactionTo))) return res.status(403).json(`You cant add credits to the user ${transactionTo}`)
    if(req.user.userType == "superAdmin" && transactionTo == req.user.username) {
      await userRepo.findOneAndUpdate({ username: transactionTo }, { $inc: { creditsAvailable: credits } })
    }
    else {
      if (credits > 0 && req.user.creditsAvailable < credits) return res.status(400).json(`You have no enough credits to transfer.`)   
      const minAddConfig = await configRepo.findOne({ configName : "minimumTransferrableCredits" })
      const recoverUptoConfig = await configRepo.findOne({ configName : "creditsRecoverableUpto" })
      if (credits > 0 && credits < minAddConfig.configValue) return res.status(400).json(`Minimum Transferrable Credits is ${minAddConfig.configValue}.`)
      const user = await userRepo.findOne({ username: transactionTo })
      if (credits < 0 && user.creditsAvailable < (-1 * credits)) return res.status(400).json("Not enough balance to recover the credits. Try again with lesser credits.")
      if (credits < 0 && req.user.userType == "superReseller" && (-1 * credits) < recoverUptoConfig.configValue) return res.status(400).json(`You can only recover upto ${recoverUptoConfig.configValue}.`)
      if (req.user.creditsAvailable > credits) await req.user.update({ creditsAvailable: (req.user.creditsAvailable - credits) })
      await userRepo.findOneAndUpdate({ username: transactionTo }, { $inc: { creditsAvailable: credits } })
    }
  }
  // Add transaction history to transaction collection
  const createdTransaction = await transactionRepo.create([{ credits, description, transactionFrom, transactionTo }], { lean: true })
  const transaction = createdTransaction[0]
  if(req.user.userType == "superAdmin" && transactionTo == req.user.username)
    io.emit('message', { type: "UPDATE_CREDIT_SUCCESS", authUsername: req.user.username })
  else
    io.emit('message', { type: "INCREASE_CREDITS_SUCCESS", authUsername: req.user.username })
  return res.status(201).json({ transaction })
}

function expiryDateAfterTransaction(date, n) {
  winstonLogger.debug("Running Operation expiryDateAfterTransaction...")
  var resDate
  if (date == 0) {
    resDate = dateFns.startOfTomorrow()
  }
  else {
    var startOfTmrw = dateFns.startOfTomorrow()
    resDate = new Date(date)
    if (resDate < startOfTmrw) resDate = startOfTmrw
  }
  resDate = dateFns.addMonths(resDate, n)
  if (resDate < startOfTmrw) resDate = startOfTmrw
  return dateFns.format(resDate, 'YYYY-MM-DD')
}