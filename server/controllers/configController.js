import configRepo from '../models/configModel'
import activityRepo from '../models/activityModel'
import fs from 'fs'
import path from 'path'
import { winstonLogger } from '../_helpers/logger'
import { io } from '../index'
import { log } from 'winston';

const logDirectory = path.join(__dirname, '/../logs/cron/')

export async function updateConfig(req, res, next) {
  winstonLogger.debug("Running Operation updateConfig...")
  if (req.user.userType !== 'superAdmin') return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  const { configName, configValue } = req.value.body
  var query = { configName },
    update = { configName, configValue },
    options = { upsert: true, new: true, setDefaultsOnInsert: true }
  const configExists = await configRepo.findOne({ configName })
  if (!configExists) return res.status(422).json({ error: 'No such config exists' })
  const config = await configRepo.findOneAndUpdate(query, update, options, function (error, result) {
    if (error) return res.status(404).json(error)
  })
  await activityRepo.create({from: req.user.username, to: req.user.username, action: 'UPDATE_CONFIG', description: update})
  io.emit('message', { type: "UPDATE_CONFIG_SUCCESS", authUsername: req.user.username })
  if (configName === 'colors') config.configValue = config.configValue[req.user.userType]
  return res.status(200).json(config)
}

export async function getConfig(req, res, next) {
  winstonLogger.debug("Running Operation getConfig...")
  const config = await configRepo.find()
  if (!config) return res.status(404).json({ error: "No Configs Found." })
  let result = {}
  config.forEach(config => result[config.configName] = config.configValue) 
  if (req.query.special === 'false') result.colors = result.colors[req.user.userType]  
  return res.status(200).json(result)
}

export async function readLog(req, res, next) {
  winstonLogger.debug("Running Operation readLog...")
  if (req.user.userType !== 'superAdmin') return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  const logFileName = req.params.filename
  await fs.readFile(logDirectory+logFileName, 'utf8', (err, data) => {
    if (err) return res.status(404).json({ error: err})
    return res.status(200).json(data)
  })
}

export async function getLogFiles(req, res, next) {
  winstonLogger.debug("Running Operation getLogFiles...")
  if (req.user.userType !== 'superAdmin') return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  let logFileList = []
  await fs.readdir(logDirectory, (err, files) => {
    if (err) return res.status(404).json({ error: err})
    files.forEach(file => {
      if (file.substring(file.lastIndexOf(".")+1, file.length) == "log")
        logFileList.push(file)
    });
    return res.status(200).json(logFileList.sort().reverse())
  })
}