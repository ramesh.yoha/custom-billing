import { getAllUserHelper } from './userController'
import userRepo from '../models/userModel'
import transactionRepo from '../models/transactionModel'
import activityRepo from '../models/activityModel'
import statsRepo from '../models/statsModel'
import configRepo from '../models/configModel'
import { subSeconds, startOfTomorrow, endOfToday } from 'date-fns'


export async function getLatestChanges(req, res, next) {
  const from = subSeconds(new Date(), 60)
  const to = startOfTomorrow()
  
  const { configUpdated } = req.query
  req.query.special = 'false' // used in getConfig to only get auth userType's theme colors

  const auth = req.user
  const children = auth.userType !== 'specialDeleter' ? await getAllUserHelper(req.user) : []
  const staffs = auth.userType === 'reseller' && auth.storeAdmin === 'me' ? await userRepo.find({ storeAdmin: req.user.username }) : []
  const allUsernames = auth.userType !== 'specialDeleter' ? await getUsernames(auth.username, children) : []
  const transactions = auth.userType !== 'specialDeleter' ? await getTransactions(allUsernames, from, to) : []
  const activities = auth.userType !== 'specialDeleter' ? await getUserActivities(allUsernames, from, to) : []
  const stats = auth.userType !== 'specialDeleter' ? await getUserStats(auth.username, children, from, to) : []
  const config = (configUpdated === 'true' && auth.userType !== 'specialDeleter') ? await getConfig(auth.userType) : null
  res.status(200).json({
    auth,
    ...children,
    staffs,
    transactions,
    activities,
    stats,
    config
  })
}

const getUsernames = async (authUsername, children) => {
  var allInternalUsernames = []
  children.resellers.forEach(element => allInternalUsernames.push(element.username)) // Resllers
  const staffs = await userRepo.find({ storeAdmin: { $in: allInternalUsernames }}, 'username') // Staffs
  staffs.forEach(element => allInternalUsernames.push(element.username))  // Staffs
  children.admins.forEach(element => allInternalUsernames.push(element.username)) // Admins
  children.superResellers.forEach(element => allInternalUsernames.push(element.username)) // SuperResellers
  const allCientUsernames = children.clients.map(element => element.login) // Clients
  allInternalUsernames.push(authUsername) // Authenticated User
  return { allInternalUsernames, allCientUsernames }
}

const getTransactions = async ({ allInternalUsernames, allCientUsernames }, from, to) => { 
  return await transactionRepo.find({ 
    $or: [{ transactionTo: { $in: [...allInternalUsernames, ...allCientUsernames] } }, { transactionFrom: { $in: allInternalUsernames } }], 
    createdAt: {$gte: from, $lt: to} 
  }, null, { sort: { createdAt: -1 } })
}

const getUserActivities = async ({ allInternalUsernames, allCientUsernames }, from, to) => {
  return await activityRepo.find({ 
    $or: [{ to: { $in: [...allInternalUsernames, ...allCientUsernames] } }, { from: { $in: allInternalUsernames } }], 
    createdAt: {$gte: from, $lt: to} 
  }, '-__v', { sort: { createdAt: -1 } })
}

const getUserStats = async (authUsername, children, from, to) => {
  const { superAdmins=[], admins=[], superResellers=[], resellers=[], clients=[] } = children
  const stats = {
    superAdmins: superAdmins.length,
    admins: admins.length,
    superResellers: superResellers.length,
    resellers: resellers.length,
    clients: clients.length
  }
  await statsRepo.findOneAndUpdate({createdAt: endOfToday(), username: authUsername}, {stats, username: authUsername}, {upsert:true})
  return await statsRepo.find({ createdAt: {$gte: from, $lt: to}, username: authUsername }, '-__v', { sort: { createdAt: -1 }})
}

const getConfig = async (userType) => {
  const configs = await configRepo.find()
  if (!configs) return null
  let result = {}
  configs.forEach(config => result[config.configName] = config.configValue) 
  result.colors = result.colors[userType]  
  return result
}