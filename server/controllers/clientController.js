import axios from 'axios'
import querystring from 'querystring'
import { checkPermissionRights } from '../_helpers/checkPermission'
import clientRepo from '../models/clientModel'
import userRepo from '../models/userModel'
import activityRepo from '../models/activityModel'
import { winstonLogger } from '../_helpers/logger'
import { mergeArrayObjectsByKey } from '../controllers/userController'
import { getClients } from '../_helpers/ministraHelper'
import { io } from '../index'
import transactionRepo from '../models/transactionModel'


const ministraAPI = process.env.MINISTRA_HOST + 'stalker_portal/api/'
const ministaUser = process.env.MINISTRA_USER
const ministraPW = process.env.MINISTRA_PW
const config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  auth: {
    username: `${ministaUser}`,
    password: `${ministraPW}`
  }
}

export async function validateLogin(req, res, next) {
  winstonLogger.debug("Running validateLogin Operation.")
  await axios.get(ministraAPI + 'users/' + req.params.id, config)
    .then(response => {
      res.locals.client = response.data
    })
  if (res.locals.client.status !== 'OK') return res.status(422).json({ error: `client with login id ${req.params.id} was not found in the ministra system` })
  const client = await clientRepo.findOne({ login: req.params.id })
  if (!client) return res.status(422).json({ error: `Client with login id ${req.params.id} was not found in mongo DB` })
  res.locals.mongoClient = client
  if (await checkPermissionRights(req.params.id, req.user, 0) == false) return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  next()
}

export async function checkMac(req, res, next) {
  winstonLogger.debug("Running checkMac Operation.")
  await axios.get(ministraAPI + 'accounts/' + req.params.id, config)
    .then(response => {
      if (response.data.status !== 'OK') return res.status(200).json({ mac: req.params.id, status: "Available." })
      // if (new Date(response.data.results[0].tariff_expired_date) < new Date()) return res.status(200).json({ mac: req.params.id, status: "Available." })
      return res.status(200).json({ mac: response.data.results[0].stb_mac, login: response.data.results[0].login, expiryDate: response.data.results[0].tariff_expired_date, status: "Unavailable." })
    })
}

export async function getAllClients(req, res, next){
  winstonLogger.debug("Running getAllClients Operation.")
  await axios.get(ministraAPI + 'accounts/', config)
    .then(response => {
      res.locals.allClients = response.data
    })
    if (res.locals.allClients.status !== 'OK') {
      return res.status(422).json({ error: `Failed to get all clients` })
    }
    else {
      const clients = res.locals.allClients.results.map(({ login, stb_mac }) => ({ login, stb_mac }))
      return res.status(200).json(clients)
    }
}

export async function addClient(req, res, next) {
  winstonLogger.debug("Running addClient Operation.")
  if (req.user.userType !== 'reseller') return res.status(403).json({ error: `Only reseller can add a client.` })
  const { login } = req.value.body
  req.value.body.account_number = login
  const ministraPayLoad = querystring.stringify(req.value.body)
  const existingClient = await clientRepo.findOne({ login })
  if (existingClient) {
    await axios.get(ministraAPI + 'users/' + login, config)
    .then(response => {
      res.locals.existingDeletingClient = response.data
    })
    return res.status(422).json({ error: `Client already exists with login id : ${login}` })
  }
  await axios.post(ministraAPI + 'accounts/',
    ministraPayLoad, config)
    .then(response => {
      res.locals.addedUser = response.data
    })
  if (res.locals.addedUser.status !== 'OK') {
    return res.status(422).json({ error: `Failed to add a client to the system : ${res.locals.addedUser.error}` })
  }
  else {
    await axios.get(ministraAPI + 'users/' + login, config)
    .then(response => {
      res.locals.newClient = response.data
    })
    const newMinistraClient = new Array(res.locals.newClient.results)
    const parentUsername = req.user.storeAdmin == "me" ? req.user.username : req.user.storeAdmin
    const client = await clientRepo.create([{ parentUsername, login }], { lean: true })
    await userRepo.findOneAndUpdate({ username: parentUsername }, { $push: { childUsernames: login } })
    const mergedClient = mergeArrayObjectsByKey(client, newMinistraClient, 'login', 'login')[0]
    await activityRepo.create({from: req.user.username, to: login, action: 'CREATE', description: mergedClient})
    io.emit('message', { type: "ADD_CLIENT_SUCCESS", authUsername: req.user.username })
    await res.status(201).json(mergedClient)
  }
}

export async function updateClient(req, res, next) {
  winstonLogger.debug("Running updateClient Operation.")
  var login = req.params.id
  if(req.user.userType == "specialDeleter"){
    await axios.get(ministraAPI + 'users/' + login, config)
        .then(response => {
          res.locals.updateClientMinistra = response.data
        })  
    if (new Date(res.locals.updateClientMinistra.results.tariff_expired_date) >= new Date()) return res.status(422).json({ error: `Client tariff hasn't expired yet to perform this action.` })
    await axios.put(ministraAPI + 'users/' + login,
        'stb_mac', config)
        .then(response => {
          res.locals.updatedUser = response.data
        })
      if (res.locals.updatedUser.status !== 'OK') {
        return res.status(404).json({ error: `Failed to update the client ${login} to the system : ${res.locals.updatedUser.error}` })
      }
      else {
        await activityRepo.create({from: req.user.username, to: login, action: 'UPDATE', description: `removed stb_mac ${res.locals.updateClientMinistra.results.stb_mac} from client ${login}.`})  
        io.emit('message', { type: "UPDATE_CLIENT_SUCCESS", authUsername: req.user.username })
        return res.status(200).json(login)
      }
  }
  else{
    if (req.value.body.stb_mac !== undefined) {
      var stb_mac = req.value.body.stb_mac
      await axios.get(ministraAPI + 'accounts/' + stb_mac, config)
        .then(response => {
          res.locals.existingMac = response.data
        })
        if (res.locals.existingMac.status == 'OK') {
          return res.status(422).json("Mac Already Exists on the System. Please delete that mac before proceeding")
        }
    }
    await axios.put(ministraAPI + 'users/' + login,
      querystring.stringify(req.value.body), config)
      .then(response => {
        res.locals.updatedUser = response.data
      })
    if (res.locals.updatedUser.status !== 'OK') {
      return res.status(404).json({ error: `Failed to update the client ${login} to the system : ${res.locals.updatedUser.error}` })
    }
    else {
      if (req.body.password == undefined) await activityRepo.create({from: req.user.username, to: login, action: 'UPDATE', description: req.value.body})  
      io.emit('message', { type: "UPDATE_CLIENT_SUCCESS", authUsername: req.user.username })
      return res.status(200).json(login)
    }
  }
}

export async function deleteClient(req, res, next) {
  winstonLogger.debug("Running deleteClient Operation.")
  if(req.user.userType == "specialDeleter") return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  if (res.locals.mongoClient.accountBalance > 0) return res.status(400).json({ error: `The Client has ${res.locals.mongoClient.accountBalance} credits in his account. Please recover the credits from this Client ${req.params.id} before you delete.` })
  const ministraClient = (await getClients([req.params.id]))
  const mergedClient = mergeArrayObjectsByKey(new Array(res.locals.mongoClient), ministraClient, 'login', 'login')[0]
  await axios.delete(ministraAPI + 'users/' + req.params.id, config)
    .then(response => {
      res.locals.deletingClient = response.data
    })
  if (res.locals.deletingClient.status !== 'OK') {
    return res.status(422).json({ error: `failed to delete client ${req.params.id} : ${res.locals.deletingClient.error}` })
  }
  else {
    await userRepo.findOneAndUpdate({ username: res.locals.mongoClient.parentUsername }, { $pull: { childUsernames: res.locals.mongoClient.login } })
    await res.locals.mongoClient.remove()
    mergedClient.login = "DELETED_" + mergedClient.login 
    mergedClient.account_number = mergedClient.login 
    const activitiesToUpdate = ["DELETE", "UPDATE", "CREATE", "UPGRADE"]
    await transactionRepo.updateMany({ transactionTo : req.params.id }, { transactionTo : mergedClient.login })
    await transactionRepo.updateMany({ transactionFrom : req.params.id }, { transactionFrom : mergedClient.login })
    await activityRepo.updateMany({ to : req.params.id, action: { $in:  activitiesToUpdate } }, { to : mergedClient.login  })
    await activityRepo.updateMany({ from : req.params.id, action: { $in:  activitiesToUpdate } }, { from : mergedClient.login  })
    await activityRepo.create({from: req.user.username, to: mergedClient.login , action: 'DELETE', description: mergedClient})
    io.emit('message', { type: "DELETE_CLIENT_SUCCESS", authUsername: req.user.username })
    return res.status(200).json(req.params.id)
  }
}

export async function reActivate(req, res, next) {
  winstonLogger.debug("Running reActivate Operation.")
  if (req.user.userType !== 'reseller') return res.status(403).json({ error: `Only reseller can reActivate his client.` })
  if (await checkPermissionRights(req.params.id, req.user, 0) == false) return res.status(403).json({ error: `You Have No Rights To Perform This Action.` })
  const mongoClient = await clientRepo.findOne({ login: req.params.id })
  await axios.get(ministraAPI + 'users/' + req.params.id, config)
    .then(response => {
      res.locals.reActivateClient = response.data
    })
  if (res.locals.reActivateClient.results.status != 0 || mongoClient.accountBalance == 0){
    return res.status(422).json({ error: `Client is already active or there is no account balance for this client.` })
  }
  await axios.put(ministraAPI + 'users/' + req.params.id,
  'status=1', config)
  .then(response => {
    res.locals.reActivatingClient = response.data
  })
  if (res.locals.reActivatingClient.status !== 'OK') {
    return res.status(422).json({ error: `failed to reactivate client ${req.params.id} : ${res.locals.reActivatingClient.error}` })
  }
  else {
    await mongoClient.update({ $inc: { accountBalance : -1 } } )
    await userRepo.findOneAndUpdate({ username : mongoClient.parentUsername }, { $inc: { creditsAvailable : -1, creditsOwed : -1 } })
    io.emit('message', { type:  "REACTIVATE_CLIENT_SUCCESS", payload:{data: req.params.id} } )
    await activityRepo.create({from: req.user.username, to: req.params.id, action: 'REACTIVATE'})
    return res.status(201).json(req.params.id)
  }
}